namespace JVLib.Scripts.Common.Constant {
    public static class PathConstant {
        public const string AddComponentMenuCameraPath = "JvLib/Camera";

        #region Setting Path Collection

        public const string PopupSettingResourcesPath = "Settings/PopupModule/PopupSetting";
        

        #endregion

        #region Save Game
        public const string GameSettingSaveIdentifier = "game_setting";
        public const string GameImmutableSaveIdentifier = "game_immutable";
        public const string GamePlayDataSaveIdentifier = "game_process";
        #endregion
    }
}