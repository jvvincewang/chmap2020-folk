namespace JVLib.Scripts.Common.Constant {
    /// <summary>
    /// Asset Load type enum
    /// </summary>
    public enum LoadAssetType {
        /// <summary>
        /// Load Asset From Resources folder
        /// </summary>
        Resources,
        
        /// <summary>
        /// Load Asset from asset bundle 
        /// </summary>
        AssetBundle,
        
        /// <summary>
        /// Load Asset from streamingAsset folder
        /// </summary>
        StreamingAsset
    }
}