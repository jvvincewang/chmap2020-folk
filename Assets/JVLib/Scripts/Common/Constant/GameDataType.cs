namespace JVLib.Scripts.Common.Constant {
    /// <summary>
    /// Game Data type
    /// </summary>
    public enum GameDataType {
        None = 0,

        /// <summary>
        /// General Game Setting (volume, music, controller key binding,...)
        /// </summary>
        GameSettingData = 1,

        /// <summary>
        /// Immutable Data such as user login info
        /// </summary>
        GameImmutableData = 2,

        /// <summary>
        /// User in game process data
        /// </summary>
        GamePlayData = 3
    }
}