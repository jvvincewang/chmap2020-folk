using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using JVLib.Scripts.Common.Constant;
using UnityEngine;
using Object = UnityEngine.Object;

namespace JVLib.Scripts.Common.Utils {
    public class ModuleSettingAccessor {
        private class ModuleSettingInfo {
            private string m_moduleName;
            private string m_filePath;
            private LoadAssetType m_loadAssetType;
            private Object m_settingFile;

            public string ModuleName {
                get => m_moduleName;
                set => m_moduleName = value;
            }

            public string FilePath {
                get => m_filePath;
                set => m_filePath = value;
            }

            public LoadAssetType LoadAssetType {
                get => m_loadAssetType;
                set => m_loadAssetType = value;
            }

            public Object SettingFile {
                get => m_settingFile;
                set => m_settingFile = value;
            }
        }
        
        private static readonly List<ModuleSettingInfo> m_cachedFileInfo = new List<ModuleSettingInfo>();
        
        /// <summary>
        /// Get The module setting
        /// </summary>
        /// <param name="moduleName">Name Of Module</param>
        /// <param name="loadType">Module Load Type</param>
        /// <typeparam name="T">Type of Asset</typeparam>
        /// <returns>Loaded asset if can not found then return null</returns>
        public static T GetModuleSetting<T>(string moduleName, LoadAssetType loadType) where T : Object {
            var cachedFound = m_cachedFileInfo.Find(x => x.ModuleName == moduleName);
            if (cachedFound != null) {
                return (T)cachedFound.SettingFile;
            }
            
            var cachedFileInfo = new ModuleSettingInfo();
            T asset = null;
            var path = GenerateSettingPath(moduleName);
            switch (loadType) {
                case LoadAssetType.Resources: 
                    path = GenerateSettingPath(moduleName);
                    asset =  LoadFromResources<T>(path);
                    break;
                case LoadAssetType.AssetBundle:
                    break;
                case LoadAssetType.StreamingAsset:
                    break;
            }

            cachedFileInfo.ModuleName = moduleName;
            cachedFileInfo.LoadAssetType = loadType;
            cachedFileInfo.SettingFile = asset;
            cachedFileInfo.FilePath = path;
            return asset;
        }

        /// <summary>
        /// Clear cached object
        /// </summary>
        public static void ClearModuleSettingCache() {
            foreach (ModuleSettingInfo info in m_cachedFileInfo) {
#if UNITY_EDITOR
                Object.DestroyImmediate(info.SettingFile);
#else
                Object.Destroy(info.SettingFile);
#endif
            }
            m_cachedFileInfo.Clear();
        }

        private static T LoadFromResources<T>(string path) where T : Object {
            var setting = Resources.Load<T>(path);
            return setting;
        }

        /// <summary>
        /// This is the temporary solution, I will rework this after moving to the assetLoader module
        /// TODO Rework when implement the resources loader module
        /// </summary>
        /// <param name="moduleName"></param>
        /// <returns></returns>
        private static string GenerateSettingPath(string moduleName) {
            string[] paths = new[] {"Settings", $"{moduleName}Module", $"{moduleName}Setting"};
            return Path.Combine(paths);
        }
    }
}