using UnityEngine;
using System.IO;
using System.Linq;
using JVLib.Scripts.Common.Constant;

namespace JVLib.Scripts.Common.Utils {
    public static class GameStorageUtil {
        /// <summary>
        /// Get the game storage full file path
        /// </summary>
        /// <param name="path">the path that will store under root path</param>
        /// <param name="gameDataType">Game Data Type</param>
        /// <returns>Storage full file path</returns>
        public static string GetStorageFilePath(string path, GameDataType gameDataType) {
            var id = string.Empty;
            switch (gameDataType) {
                case GameDataType.GameSettingData:
                    id = PathConstant.GameSettingSaveIdentifier;
                    break;
                case GameDataType.GameImmutableData:
                    id = PathConstant.GameImmutableSaveIdentifier;
                    break;
                case GameDataType.GamePlayData:
                    id = PathConstant.GamePlayDataSaveIdentifier;
                    break;
            }
            var p = Path.Combine(Application.persistentDataPath, id);
            p = Path.Combine(p, path);
            return p;
        }

        /// <summary>
        /// Check if file path is exist or not
        /// </summary>
        /// <param name="path">file path</param>
        /// <param name="dataType">Game Data type</param>
        /// <returns>File exist or not</returns>
        public static bool IsExistFilePath(string path, GameDataType dataType) {
            var storageFilePath = GetStorageFilePath(path, dataType);
            var exists =  Directory.Exists(storageFilePath);
            return exists;
        }

        /// <summary>
        /// Get all file info under defined path
        /// </summary>
        /// <param name="path">file path</param>
        /// <param name="dataType">game datatype</param>
        /// <returns>All file info</returns>
        public static FileInfo[] GetFileUnderPath(string path, GameDataType dataType) {
            var dirInfo = new DirectoryInfo(GetStorageFilePath(path, dataType));
            var infos = dirInfo.GetFiles();
            return infos;
        }

        /// <summary>
        /// Is this path empty or not. (If this path not exist, it will consider as empty too)
        /// </summary>
        /// <param name="path">defined path</param>
        /// <param name="dataType">game data type</param>
        /// <returns>Check if this path is empty or not</returns>
        public static bool IsPathEmpty(string path, GameDataType dataType) {
            if (!IsExistFilePath(path, dataType)) {
                return true;
            }

            return Directory.GetFiles(GetStorageFilePath(path, dataType)).Length <= 0;
        }

        /// <summary>
        /// Check if the defined file exist or not
        /// </summary>
        /// <param name="path">path to file</param>
        /// <param name="fileName">file name</param>
        /// <param name="dataType">data type</param>
        /// <returns>Check if this file exist or not</returns>
        public static bool IsExistFile(string path, string fileName, GameDataType dataType) {
            var fileInfos = GetFileUnderPath(path, dataType);
            if (fileInfos == null || fileInfos.Length == 0) {
                return false;
            }

            return fileInfos.Any(info => info.Name.Contains(fileName));
        }
    }
}