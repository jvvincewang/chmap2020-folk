using System;

namespace JVLib.Scripts.Common.Attributes {
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class PrefabSingletonAttribute : Attribute {
        public string Name { get; }

        public string PrefabPath { get; }

        /// <summary>
        /// Decide we to load the prefab from. I design this for load prefab from Resource, AssetBundle,...
        /// The most basic is load from resources. If you want to load from other source than defined it yourself
        /// + FromResource;
        /// </summary>
        public string LoadFrom { get; }

        public PrefabSingletonAttribute(string name, string prefabPath, string loadFrom = "FromResource") {
            Name = name;
            PrefabPath = prefabPath;
            LoadFrom = loadFrom;
        }

        public PrefabSingletonAttribute(string prefabPath, string loadFrom = "FromResource") {
            PrefabPath = prefabPath;
            LoadFrom = loadFrom;
        }
    }
}