using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using Common;
using UnityEditor;

namespace JVLib.Scripts.Common.Utils {
    /// <summary>
    /// Controller that use for execute shell command in UnityEditor.
    /// Currently this is available only on OSX, (In fact, it also available on Windows but I still not test it yet)
    /// </summary>
    public class ExecuteProcessController : SingletonBase<ExecuteProcessController> {
        /// <summary>
        /// Flag that check if this Controller was init.
        /// </summary>
        private bool m_initFlag;

        /// <summary>
        /// List to store all the exe shell wrapper
        /// </summary>
        private readonly List<ProcessRequestWrapper> m_shellRequestWrappers = new List<ProcessRequestWrapper>();

        /// <summary>
        /// Wrapper of info use for the command line process (info use for ProcessStartInfo class)
        /// </summary>
        public class ProcessRequestWrapper {
            public string ReqId;
            public string CmdFileName;
            public string CmdArguments;
            public string WorkingDir;

            public bool IsDone;
            public bool IsError;
            public Action<string> OnExeFinished;
            public Action<string> OnError;

            /// <summary>
            /// This field will be auto generate.
            /// </summary>
            public Process ExeProcess;
        }

        private void OnUpdateEditor() {
            var doneProcesses = m_shellRequestWrappers.Where(x => x.IsDone).ToList();
            foreach (var wrapper in doneProcesses) {
                try {
                    var outPutStringSb = new StringBuilder();
                    if (wrapper.IsError) {
                        using (var stream = wrapper.ExeProcess.StandardError) {
                            outPutStringSb.AppendLine(stream.ReadToEnd());
                        }

                        wrapper.OnError?.Invoke(outPutStringSb.ToString());
                    }
                    else {
                        using (var stream = wrapper.ExeProcess.StandardOutput) {
                            outPutStringSb.AppendLine(stream.ReadToEnd());
                        }

                        wrapper.OnExeFinished?.Invoke(outPutStringSb.ToString());
                    }

                }
                catch (Exception e) {
                    UnityEngine.Debug.LogError($"Error when executing reqID: {wrapper.ReqId} {e}");
                }
            }

            foreach (var process in doneProcesses) {
                m_shellRequestWrappers.Remove(process);
            }
        }

        /// <summary>
        /// Execute command inside process class
        /// </summary>
        /// <param name="wrapper">Info use for the process</param>
        private void ExeRequest(ProcessRequestWrapper wrapper) {
            // Init Controller.
            if (!m_initFlag) {
                EditorApplication.update += OnUpdateEditor;
                m_initFlag = true;
            }

            if (wrapper == null) {
                return;
            }

            ThreadPool.QueueUserWorkItem(delegate {
                var info = new ProcessStartInfo {
                    FileName = wrapper.CmdFileName,
                    Arguments = wrapper.CmdArguments,
                    WorkingDirectory = wrapper.WorkingDir,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    RedirectStandardInput = false
                };

                var p = new Process {
                    StartInfo = info
                };

                wrapper.ExeProcess = p;
                p.Start();
                p.WaitForExit();
                while (p.HasExited) {
                    if (p.ExitCode == 0) {
                        wrapper.IsDone = true;
                    }
                    else {
                        wrapper.IsDone = true;
                        wrapper.IsError = true;
                    }
                    break;
                }
            });
        }

        /// <summary>
        /// Create the process command
        /// </summary>
        /// <param name="requestWrapper">command request wrapper</param>
        /// <param name="isAutoGenerateId">Auto generate id of process request or not, if this flag set to true then the wrapper reqID set before will be override</param>
        public void CreateProcessCommand(ProcessRequestWrapper requestWrapper, bool isAutoGenerateId = false) {
            if (isAutoGenerateId) {
                requestWrapper.ReqId = Guid.NewGuid().ToString().Substring(0, 8);
            }

            m_shellRequestWrappers.Add(requestWrapper);
            ExeRequest(requestWrapper);
        }
    }
}