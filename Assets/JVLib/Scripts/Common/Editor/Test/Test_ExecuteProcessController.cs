using JVLib.Scripts.Common.Utils;
using NUnit.Framework;
using UnityEngine;

namespace JVLib.Scripts.Common.Editor.Test {
    // ReSharper disable once InconsistentNaming
    public class Test_ExecuteProcessController {
        [Test]
        public void TestExecuteProcessPass() {
            var gitWrapper = new ExecuteProcessController.ProcessRequestWrapper {
                ReqId = "TestGitStatus",
                CmdFileName = "git",
                CmdArguments = " status --porcelain",
                WorkingDir = Application.dataPath,
                OnExeFinished = (s) => Debug.Log($"TestGitStatus: {s}"), 
                OnError = Debug.Log
            };
            var pwdWrapper = new ExecuteProcessController.ProcessRequestWrapper{
                ReqId = "TestPwdStatus",
                CmdFileName = "pwd",
                WorkingDir = Application.dataPath,
                OnExeFinished =  (s) => {
                    Debug.Log($"TestPwdStatus: {s}");
                }, 
                OnError = Debug.Log
            };
            var lsWrapper = new ExecuteProcessController.ProcessRequestWrapper{
                ReqId = "ls",
                CmdFileName = "ls",
                CmdArguments = "-la",
                WorkingDir = Application.dataPath,
                OnError = Debug.Log
            };
            lsWrapper.OnExeFinished = s => { Debug.Log($"{lsWrapper.ReqId}: {s}"); };
            
            ExecuteProcessController.instance.CreateProcessCommand(gitWrapper);
            ExecuteProcessController.instance.CreateProcessCommand(pwdWrapper);
            ExecuteProcessController.instance.CreateProcessCommand(lsWrapper, true);
        }

    }
}