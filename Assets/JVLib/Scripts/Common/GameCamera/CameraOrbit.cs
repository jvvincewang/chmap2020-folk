using System;
using DG.Tweening;
using JVLib.Scripts.Common.Camera;
using JVLib.Scripts.Common.Constant;
using UnityEngine;

namespace JVLib.Scripts.Common.GameCamera {
    [AddComponentMenu(PathConstant.AddComponentMenuCameraPath)]
    public class CameraOrbit : MonoBehaviour, ICameraEffect {
        [SerializeField] private GameObject m_target;

        [SerializeField] private Vector3 m_angle;
        [SerializeField] private float m_distance;

        [Header("Pitch")]
        [SerializeField] private float m_minPitch, m_maxPitch;

        [Header("Zoom")]
        [SerializeField] private float m_minDistance, m_maxDistance;

        [Header("Smooth Setting")] [SerializeField]
        private float m_smoothDistance = 10f;

        [SerializeField] private float m_smoothRotate = 10;
        

        private float Pitch {
            get => m_angle.x;
            set => m_angle.x = value;
        }

        private float Roll {
            get => m_angle.z;
            set => m_angle.z = value;
        }

        private float Yaw {
            get => m_angle.y;
            set => m_angle.y = value;
        }

        private float Distance {
            get => m_distance;
            set => m_distance = value;
        }

        private void OnEnable() {
//            AdjustPosition();
        }

        private void AdjustPosition() {
            var direction = (m_target.transform.position - transform.position).normalized;
            transform.forward = direction;
            return;
            float angle = Vector3.Angle(transform.forward, direction);
            Quaternion rot = transform.rotation;
            
        }
        

        private void LateUpdate() {
            AdjustPosition();
        }

        public void Execute() {
            var scrollDelta = Input.mouseScrollDelta;
            if (scrollDelta.y > 0) {
                m_distance = Mathf.Clamp(m_distance, m_minDistance, m_maxDistance);
                float smoothDistance = Vector3.Distance(transform.position, m_target.transform.position);
                smoothDistance = Mathf.Lerp(smoothDistance, m_distance, Time.deltaTime * m_smoothDistance);
                Vector3 MoveCam = transform.position * smoothDistance;
                Debug.Log($"move distance{smoothDistance}  - Move cam pos: {MoveCam}");
            }

        }

        public void ZoomIn() {
            Distance--;
        }

        public void ZoomOut() {
            Distance++;
        }

        public void Reset() {
        }

        private void Test() {
//            float axisX = Input.GetAxis("Horizontal");
//            float axisY = Input.GetAxis("Vertical");
//            Vector3 camDir = transform.forward;
//            Vector3 wCamDir = m_t.TransformDirection(camDir);
//		
//            Debug.DrawRay(m_t.position, wCamDir * 5f, Color.yellow);
//		
//            Vector3 r = new Vector3(wCamDir.z, wCamDir.y, -wCamDir.x);
//            Vector3 moveHorizontal = r  * axisX;
//            Vector3 moveVertical = wCamDir * axisY;
//            Vector3 move = (moveHorizontal + moveVertical).normalized;
//            move = move * m_speed * Time.deltaTime;
//            Debug.Log(move);
//            var position = m_t.position;
//            Debug.DrawRay(position, move*5f, Color.red, 1f);
//            position += move;
//            m_t.position = position;
        }

        /// <summary>
        /// Clamp and adjust angle (Because angle in playmode only go from 0 -> 360 but in unity editor it can be out of this range)
        /// </summary>
        /// <param name="angle">angle</param>
        /// <param name="min">min clamp value</param>
        /// <param name="max">max clamp value</param>
        /// <returns>Clamped angle</returns>
        private static float ClampAngle(float angle, float min, float max) {
            if (angle < -360F) {
                angle += 360F;
            }

            if (angle > 360F) {
                angle -= 360F;
            }

            return Mathf.Clamp(angle, min, max);
        }
    }
}