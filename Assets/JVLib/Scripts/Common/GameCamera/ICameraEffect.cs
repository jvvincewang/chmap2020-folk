namespace JVLib.Scripts.Common.Camera {
    public interface ICameraEffect {
        void Execute();
        void Reset();
    }
}