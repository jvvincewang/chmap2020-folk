using System;
using JVLib.Scripts.PopupModule.Common;
using UnityEngine;
using Debug = System.Diagnostics.Debug;

namespace JVLib.Scripts.PopupModule {
    public class TestCallPopupScript : MonoBehaviour {
        [SerializeField] private string m_popupName;
        [SerializeField] private bool m_isCloseCompletely;
        private string m_id;
        private void OnGUI() {
            var o = GUILayout.Button("Open popup");
            var c = GUILayout.Button("Close popup");
            if (o) {
            }

            if (c) {
            }
        }
    }
}