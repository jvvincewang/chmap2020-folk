using JVLib.Scripts.Common.Constant;
using JVLib.Scripts.Common.Utils;
using JVLib.Scripts.PopupModule.Common;
using UnityEditor;
using UnityEngine;

namespace JVLib.Scripts.PopupModule.Editor {
    /// <summary>
    /// Editor use for popup system
    /// </summary>
    public static class PopupModuleEditor {
        /// <summary>
        /// Create Popup Prefab Variant
        /// </summary>
        [MenuItem("JvLib/PopupModule/Create Popup Prefab")]
        public static void CreatePopupPrefab() {
            var popupSetting = ModuleSettingAccessor.GetModuleSetting<Setting>("Popup", LoadAssetType.Resources);
            GameObject popupTemplate = Resources.Load<GameObject>(Setting.PopupTemplatePath);
            var goSource = (GameObject)PrefabUtility.InstantiatePrefab(popupTemplate);
            var variant = PrefabUtility.SaveAsPrefabAsset(goSource,
                $"{popupSetting.PopupVariantPath}{popupSetting.DefaultPopupVariantName}");
            Selection.activeObject = variant;
            Object.DestroyImmediate(goSource);
        }

        /// <summary>
        /// Create draggable popup prefab variant
        /// </summary>
        [MenuItem("JvLib/PopupModule/Create Draggable Popup Prefab")]
        public static void CreateDraggablePopupPrefab() {
            var popupSetting = ModuleSettingAccessor.GetModuleSetting<Setting>("Popup", LoadAssetType.Resources);
            GameObject popupTemplate = Resources.Load<GameObject>(Setting.DraggablePopupTemplatePath);
            var goSource = (GameObject)PrefabUtility.InstantiatePrefab(popupTemplate);
            var variant = PrefabUtility.SaveAsPrefabAsset(goSource,
                $"{popupSetting.PopupVariantPath}{popupSetting.DefaultDragPopupVariantName}");
            Selection.activeObject = variant;
            Object.DestroyImmediate(goSource);
        }
    }
}