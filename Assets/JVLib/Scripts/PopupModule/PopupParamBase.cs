using UnityEngine.Events;

namespace JVLib.Scripts.PopupModule {
    /// <summary>
    /// Popup param base class
    /// </summary>
    public class PopupParamBase {
        public UnityAction OnClickPositiveCallback;
        public UnityAction OnClickNegativeCallback;
        public UnityAction OnClickMiddleCallback;
    }
}