namespace JVLib.Scripts.PopupModule {
    public interface IPopup {
        void Open();
        void Close(bool isDestroyPopup = true);
        void OnClickedNativeBackButton();

        void SetupPopup<T>(T param) where T : PopupParamBase;
    }
}