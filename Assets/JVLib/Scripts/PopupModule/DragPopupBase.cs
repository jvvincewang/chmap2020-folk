using System;
using System.Collections;
using System.Collections.Generic;
using JVLib.Scripts.Common.Ext;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace JVLib.Scripts.PopupModule {
    public class DragPopupBase : PopupBase, IDragHandler, IBeginDragHandler, IEndDragHandler {
        /// <summary>
        /// The object that receive the ray cast to drag popup
        /// </summary>
        [Space]
        [Header("Drag Popup Setting")]
        [SerializeField]
        private GameObject m_dragLayer;

        /// <summary>
        /// Distance between pointer position and the contentContainer Rect transform position
        /// </summary>
        private Vector3 m_distance = Vector2.zero;

        /// <summary>
        /// Cache rect transform of the contentContainer
        /// </summary>
        private RectTransform m_contentRt;

        /// <summary>
        /// Is Popup can be drag
        /// </summary>
        private bool m_isCanDrag;

        /// <summary>
        /// Is popup being dragged;
        /// </summary>
        private bool m_isDragging;

        /// <summary>
        /// Camera of popup Manager
        /// </summary>
        private Camera m_popupManagerCam;

        /// <summary>
        /// rectTransform of popup manager's canvas
        /// </summary>
        private RectTransform m_popupManagerCanvasRt;

        /// <summary>
        /// Callback call when begin drag
        /// </summary>
        private UnityAction m_onBeginDragCallback;

        /// <summary>
        /// Callback call when dragging
        /// </summary>
        private UnityAction m_onDraggingCallback;

        /// <summary>
        /// Callback call when drop
        /// </summary>
        private UnityAction m_onDropCallback;

        /// <summary>
        /// Check if this popup can drag out of screen size.
        /// This maybe rarely be used. 
        /// </summary>
        [SerializeField]
        private bool m_isCanDragOutScreen;

        private Vector3 m_maxMove;
        private Vector3 m_minMove;

        public bool IsDragging => m_isDragging;

        public bool IsCanDragOutScreen {
            get => m_isCanDragOutScreen;
            set => m_isCanDragOutScreen = value;
        }

        public UnityAction OnBeginDragCallback {
            set => m_onBeginDragCallback = value;
        }

        public UnityAction OnDraggingCallback {
            set => m_onDraggingCallback = value;
        }

        public UnityAction OnDropCallback {
            set => m_onDropCallback = value;
        }

        private void Awake() {
            m_contentRt = ContentContainer.GetComponent<RectTransform>();
            m_popupManagerCam = PopupManager.instance.PopupCanvas.worldCamera;
            m_popupManagerCanvasRt = (RectTransform) PopupManager.instance.PopupCanvas.transform;
        }

        protected override void OnOpened() {
            base.OnOpened();
            CalculateMoveLimitArea();
        }

        /// <summary>
        /// Calculate movable area of popup
        /// </summary>
        private void CalculateMoveLimitArea() {
            //Calculate popup can be dragged area
            var rect = m_contentRt.rect;
            var minMoveX = rect.width / 2;
            var minMoveY = rect.height / 2;
            var maxMoveX = Screen.width - rect.width / 2f;
            var maxMoveY = Screen.height - rect.height / 2f;
            var min = new Vector2(minMoveX, minMoveY);
            var max = new Vector2(maxMoveX, maxMoveY);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(m_popupManagerCanvasRt,
                new Vector2(minMoveX, minMoveY),
                m_popupManagerCam, out m_minMove);
            RectTransformUtility.ScreenPointToWorldPointInRectangle(m_popupManagerCanvasRt,
                new Vector2(maxMoveX, maxMoveY),
                m_popupManagerCam, out m_maxMove);
        }

        #region DragAndDrop Handler
        public void OnDrag(PointerEventData eventData) {
            if (!m_isCanDrag) return;
            if (!GetScreenPointToWorldOfPopup(eventData.position, out var pointerPos)) return;
            var moved = pointerPos - m_distance;

            // Adjust popup position if not allow it drag out screen size
            if (!m_isCanDragOutScreen) {
                moved.x = Mathf.Clamp(moved.x, m_minMove.x, m_maxMove.x);
                moved.y = Mathf.Clamp(moved.y, m_minMove.y, m_maxMove.y);
            }

            m_contentRt.transform.SetPositionAndRotation(moved, Quaternion.identity);
            m_onDraggingCallback?.Invoke();
            m_isDragging = true;
        }

        public void OnBeginDrag(PointerEventData eventData) {
            var current = eventData.pointerCurrentRaycast;
            var isCastDragObj = current.gameObject == m_dragLayer;
            if (!isCastDragObj) return;
            if (!GetScreenPointToWorldOfPopup(eventData.position, out var nowPos)) return;
            m_isCanDrag = true;
            m_distance = nowPos - m_contentRt.transform.position;
            m_onBeginDragCallback?.Invoke();
        }

        public void OnEndDrag(PointerEventData eventData) {
            if (!m_isDragging) return;
            m_isCanDrag = false;
            m_isDragging = false;
            m_distance = Vector2.zero;
            m_onDropCallback?.Invoke();
        }
        #endregion

        /// <summary>
        /// Get world position of screen point
        /// </summary>
        /// <param name="pointerPos">Pointer screen point position</param>
        /// <param name="worldPos">output world position</param>
        private bool GetScreenPointToWorldOfPopup(Vector2 pointerPos, out Vector3 worldPos) {
            var result = RectTransformUtility.ScreenPointToWorldPointInRectangle(m_popupManagerCanvasRt, pointerPos,
                m_popupManagerCam, out worldPos);
            return result;
        }
    }
}