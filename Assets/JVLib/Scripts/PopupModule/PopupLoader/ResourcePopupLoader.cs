using UnityEngine;
using UnityEngine.Events;

namespace JVLib.Scripts.PopupModule.PopupLoader {
    public class ResourcePopupLoader : IPopupLoader{
        public void LoadPopupAsync(string popupPath, UnityAction<GameObject> onLoadFinished) {
            var pref = Resources.Load<GameObject>(popupPath);
            var obj = Object.Instantiate(pref);
            onLoadFinished?.Invoke(pref);
        }

        public void DestroyPopup(GameObject popup) {
            Object.Destroy(popup);
        }
    }
}