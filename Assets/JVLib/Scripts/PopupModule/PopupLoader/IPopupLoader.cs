using UnityEngine;
using UnityEngine.Events;

namespace JVLib.Scripts.PopupModule.PopupLoader {
    public interface IPopupLoader {
        void LoadPopupAsync(string popupPath, UnityAction<GameObject> onLoadFinished);
        void DestroyPopup(GameObject popup);
    }
}