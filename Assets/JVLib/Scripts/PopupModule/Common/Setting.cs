using UnityEngine;

namespace JVLib.Scripts.PopupModule.Common {

#if JVLIB_DEV
    [CreateAssetMenu(menuName = "JvLib/PopupModule/Create Popup Setting", fileName = "PopupSetting")]
#endif
    public class Setting : ScriptableObject {
        [Tooltip("The path that popup variant will be create")]
        [SerializeField] private string m_popupVariantPath = "Assets/JVLib/Resources/Prefabs/Popup/";
        
        [Tooltip("The popup variant default name")]
        [SerializeField] private string m_defaultPopupVariantName = "Variant Popup.prefab";
        
        [Tooltip("The draggable popup variant default name")]
        [SerializeField] private string m_defaultDragPopupVariantName = "Draggable Variant Popup.prefab";
        
        public static string PopupTemplatePath = "Prefabs/Popup/Core/PopupTemplate";
        public static string DraggablePopupTemplatePath = "Prefabs/Popup/Core/DragPopupTemplate";

        public string PopupVariantPath => m_popupVariantPath;

        public string DefaultPopupVariantName => m_defaultPopupVariantName;

        public string DefaultDragPopupVariantName => m_defaultDragPopupVariantName;
    }
}