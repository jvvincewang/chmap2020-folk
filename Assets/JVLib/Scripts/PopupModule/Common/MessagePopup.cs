using UnityEngine;

namespace JVLib.Scripts.PopupModule.Common {
    public class MessagePopup : PopupBase {
        [SerializeField] private GameObject m_messageTxt;
        [SerializeField] private GameObject m_messageTittleTxt;
        [SerializeField] private GameObject m_messageBtnOk;

        public override void SetupPopup<T>(T popupParam) {
            base.SetupPopup(popupParam);
            Debug.Log("Has been inited");
        }
    }

    public class MessagePopupParam : PopupParamBase {
        
    }
}