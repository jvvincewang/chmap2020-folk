namespace JVLib.Scripts.PopupModule.Common {
    public enum PopupObjectLoadType {
        RESOURCE,
        ADDRESSABLE,
        DEFAULT = RESOURCE,
    }
}