using System.Collections.Generic;
using JVLib.Scripts.Common.Attributes;
using JVLib.Scripts.PopupModule.Common;
using JVLib.Scripts.PopupModule.PopupLoader;
using UnityEngine;
using UnityEngine.Events;

namespace JVLib.Scripts.PopupModule {
    [PrefabSingleton("Assets/JVLib/AddressableResources/Popup/PopupManager.prefab", loadFrom:"Addressable")]
    public class PopupManager : MonoBehaviourSingletonBase<PopupManager> {
        [SerializeField] private Canvas m_popupCanvas;

        [SerializeField]
        protected PopupObjectLoadType m_loadType = PopupObjectLoadType.DEFAULT;
        private bool m_initialized;

        public Canvas PopupCanvas {
            get => m_popupCanvas;
        }


        private readonly Dictionary<string, GameObject> m_popupPrefabDict = new Dictionary<string, GameObject>();
        private const string PrefabLoadPath = "Prefabs/Popup";
        private Dictionary<string, PopupBase> m_activePopupDict = new Dictionary<string, PopupBase>();
        private const string MESSAGE_POPUP_NAME = "Assets/GameContent/Prefabs/popups/message_popup.prefab";

        public bool Initialized => m_initialized;

        public IPopupLoader PopupLoader { get; private set; }

        private void Awake() {
            InitPopupManager();
        }

        /// <summary>
        /// Init PopupManager, This is method should be call after the popupManager instance has been completely instantiated 
        /// </summary>
        public void InitPopupManager() {
            if (m_initialized) {
                return;
            }
            
            // init popup loader
            InitPopupLoader(m_loadType);

            // Do init if needed here
            m_initialized = true;
        }

        private void InitPopupLoader(PopupObjectLoadType loadType = PopupObjectLoadType.DEFAULT) {
            m_loadType = loadType;
            switch (m_loadType) {
                case PopupObjectLoadType.RESOURCE:
                    PopupLoader = new ResourcePopupLoader();
                    break;
                default:
                    PopupLoader = new ResourcePopupLoader();
                    break;
            }
        }

        /// <summary>
        /// Get the activated popup.
        /// </summary>
        /// <param name="popupId">Id of the popup</param>
        /// <returns>The popup or null if the popup not exist or removed</returns>
        public PopupBase GetActivePopup(string popupId) {
            return m_activePopupDict[popupId];
        }

        /// <summary>
        /// Open popup from prefab object
        /// </summary>
        /// <param name="popupObj">Popup Prefab Object</param>
        /// <param name="popupParam">Popup Use Param</param>
        /// <param name="isClearAll">Is Close all activating popup</param>
        /// <returns>Popup Id</returns>
        public string OpenPopup(GameObject popupObj, PopupParamBase popupParam, bool isClearAll = false) {
            if (popupObj == null || popupObj.GetComponent<PopupBase>() == null) {
                Debug.LogError("Popup Object is null now, please recheck your process");
                return string.Empty;
            }

            var popupObjName = popupObj.name;
            // remove "(Clone)" if contain in popupObj
            popupObjName = ReformatPopupName(popupObjName);
            if (!m_popupPrefabDict.ContainsKey(popupObjName)) {
                m_popupPrefabDict.Add(popupObjName, popupObj);
            }
            else {
                m_popupPrefabDict[popupObjName] = popupObj;
            }

            return ExeOpenPopup(popupObj, popupParam, isClearAll);
        }

        /// <summary>
        /// Load And Open Popup from path Prefabs/Popup/ under resources folder
        /// </summary>
        /// <param name="popupName">Name of the popup that will be open</param>
        /// <param name="popupParam">Param use for init that popup</param>
        /// <param name="isClearAll">Is Clear All Activating popup</param>
        /// <returns>Popup Id</returns>
        public string OpenPopup(string popupName, PopupParamBase popupParam, bool isClearAll = false) {
            if (m_popupPrefabDict.ContainsKey(popupName) && m_popupPrefabDict[popupName] != null) {
                return ExeOpenPopup(m_popupPrefabDict[popupName], popupParam, isClearAll);
            }

            var prefabPath = $"{PrefabLoadPath}/{popupName}";
            var popupPrefab = Resources.Load<GameObject>(prefabPath);
            popupPrefab = Instantiate(popupPrefab);
            if (popupPrefab == null) {
                Debug.LogError($"Cannot Load popup from path [{PrefabLoadPath}], please recheck");
                return string.Empty;
            }

            var popupBase = popupPrefab.GetComponent<PopupBase>();
            if (popupBase == null) {
                Debug.LogError("Loaded prefab is not a popup please recheck if this prefab have popupBase Component");
                return string.Empty;
            }

            if (m_popupPrefabDict.ContainsKey(popupName)) {
                m_popupPrefabDict[popupName] = popupPrefab;
            }
            else {
                m_popupPrefabDict.Add(ReformatPopupName(popupName), popupPrefab);
            }

            return ExeOpenPopup(popupPrefab, popupParam, isClearAll);
        }

        public void OpenMessagePopupAsync(PopupParamBase popupParam,
            UnityAction<string> callback = null, bool isClearAll = false) {
            OpenPopupAsync(MESSAGE_POPUP_NAME, popupParam, callback, isClearAll);
        }

        /// <summary>
        /// Open popup async. Recommend use this to open popup which load from asset.
        /// </summary>
        /// <param name="popupName">popup path, or popup address</param>
        /// <param name="popupParam">param use for popup</param>
        /// <param name="callback">callback when popup done load</param>
        /// <param name="isClearAll">if clear all other popup before open new popup</param>
        public void OpenPopupAsync(string popupName, PopupParamBase popupParam,
            UnityAction<string> callback = null, bool isClearAll = false) {
            if (m_popupPrefabDict.ContainsKey(popupName) && m_popupPrefabDict[popupName] != null) {
                var str = ExeOpenPopup(m_popupPrefabDict[popupName], popupParam, isClearAll);
                callback?.Invoke(str);
                return;
            }

            PopupLoader.LoadPopupAsync(popupName, popupPrefab => {
                if (popupPrefab == null) {
                    Debug.LogError($"Cannot Load popup from path [{PrefabLoadPath}], please recheck");
                    callback?.Invoke(string.Empty);
                    return;
                }

                if (m_popupPrefabDict.ContainsKey(popupName)) {
                    m_popupPrefabDict[popupName] = popupPrefab;
                }
                else {
                    m_popupPrefabDict.Add(ReformatPopupName(popupName), popupPrefab);
                }

                var popupId = ExeOpenPopup(popupPrefab, popupParam, isClearAll);
                callback?.Invoke(popupId);
            });
        }

        
        /// <summary>
        /// DeActive the popup (This mean we can reopen it later). The popup wont be remove, it just be de　active
        /// </summary>
        /// <param name="popupId">The popup Id</param>
        /// <see cref="ReActivePopup"/>
        public bool DeActivePopup(string popupId) {
            if (!m_activePopupDict.ContainsKey(popupId)) return false;
            var deActivePopup = m_activePopupDict[popupId];
            if (deActivePopup != null) {
                deActivePopup.Close(false);
                return true;
            }
            else {
                Debug.LogWarning($"Popup with Id[{popupId}] was null or has been close before. We cannot close this");
                return true;
            }
        }

        /// <summary>
        /// Close the popup with the Id
        /// </summary>
        /// <param name="popupId">The popup Id that you want to close</param>
        /// <returns>Close result</returns>
        public bool ClosePopup(string popupId) {
            if (m_activePopupDict.ContainsKey(popupId)) {
                var closePopup = m_activePopupDict[popupId];
                if (closePopup != null) {
                    closePopup.Close();
                    m_activePopupDict.Remove(popupId);
                    return true;
                }

                Debug.LogWarning($"Popup with Id[{popupId}] was null or has been close before. We cannot close this");
                return true;
            }

            return false;
        }

        /// <summary>
        /// Reactive the un activating popup. This method use with the DeActivePopup method.
        /// </summary>
        /// <param name="popupId">the reactive popupId</param>
        /// <returns>Can reactive popup or not</returns>
        /// <see cref="DeActivePopup"/>
        public bool ReActivePopup(string popupId) {
            if (m_activePopupDict.ContainsKey(popupId)) {
                var reactive = m_activePopupDict[popupId];
                if (reactive != null) {
                    reactive.Open();
                    return true;
                }
                Debug.LogWarning( $"Popup With ID [{popupId}] not exist or has been remove, we can not reopen it");
            }
            return false;
        }
        
        /// <summary>
        /// Close All activating popup
        /// </summary>
        public void CloseAllActivePopup() {
            foreach (KeyValuePair<string, PopupBase> popupBase in m_activePopupDict) {
                popupBase.Value.Close();
            }

            m_activePopupDict.Clear();
        }

        private string ExeOpenPopup(GameObject popup, PopupParamBase popupParam, bool isClearAll = false) {
            if (isClearAll) {
                CloseAllActivePopup();
            }

            var popupBase = popup.GetComponent<PopupBase>();
            SetPopupParent(popup);
            popupBase.InitializePopup();
            popupBase.SetupPopup(popupParam);
            popupBase.Open();
            m_activePopupDict.Add(popupBase.PopupId, popupBase);
            return popupBase.PopupId;
        }

        /// <summary>
        /// Reformat the popup game object name
        /// </summary>
        /// <param name="popupName">Popup Name</param>
        private string ReformatPopupName(string popupName) {
            return popupName.Replace("(Clone)", "");
        }

        /// <summary>
        /// Set parent for popup
        /// </summary>
        /// <param name="popup">Popup Object</param>
        private void SetPopupParent(GameObject popup) {
            popup.transform.SetParent(m_popupCanvas.transform, false);
            popup.transform.SetAsLastSibling();
        }
    }
}