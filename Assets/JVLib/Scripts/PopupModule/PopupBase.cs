using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace JVLib.Scripts.PopupModule {
    public class PopupBase : MonoBehaviour, IPopup {
        /// <summary>
        /// If can interact with thing behind this popup
        /// </summary>
        [Header("Popup Base Setting")]
        [SerializeField]
        private bool m_isCanTouchBack;

        [SerializeField]
        private Image m_touchBackLayer;

        /// <summary>
        /// The container of the popup
        /// </summary>
        [SerializeField]
        private GameObject m_contentContainer;

        [SerializeField]
        private bool m_isPlayPopupAnimation;

        private const float PopupAnimationDuration = 0.2f;
        protected PopupParamBase PopupParam;
        private UnityAction m_onOpenedCallback;
        private UnityAction m_onOpenCallback;
        private UnityAction m_onCloseCallback;
        private UnityAction m_onClosedCallback;

        protected T GetParam<T>() where T : PopupParamBase => (T) PopupParam;

        public UnityAction OnOpenedCallback {
            set => m_onOpenedCallback = value;
        }

        public UnityAction OnOpenCallback {
            set => m_onOpenCallback = value;
        }

        public UnityAction OnCloseCallback {
            set => m_onCloseCallback = value;
        }

        public UnityAction OnClosedCallback {
            set => m_onClosedCallback = value;
        }

        /// <summary>
        /// Popup Id, Will be create when initiatePopup
        /// </summary>
        private string m_popupId;

        public string PopupId => m_popupId;

        public GameObject ContentContainer => m_contentContainer;

        /// <summary>
        /// Very first init process of the popup
        /// </summary>
        public void InitializePopup() {
            GeneratePopupId();
        }

        /// <summary>
        /// This is the main method to set data or behaviour to the popup.
        /// Every callback must set from the popup param.
        /// </summary>
        /// <param name="popupParam"></param>
        public virtual void SetupPopup<T>(T popupParam) where T : PopupParamBase {
            PopupParam = popupParam;
        }

        /// <summary>
        /// Make the button click can be setup from outside of class;
        /// </summary>
        /// <param name="positiveClick">positive button click callback</param>
        /// <param name="negativeClick">negative button click callback</param>
        /// <param name="middleClick">middle button click collback</param>
        public void SetUpButtonClick(UnityAction positiveClick, UnityAction negativeClick, UnityAction middleClick) {
            var buttons = GetComponentInChildren<PopupBasicButtons>();
            middleClick = middleClick ?? (() => PopupManager.instance.ClosePopup(m_popupId));
            positiveClick = positiveClick ?? (() => PopupManager.instance.ClosePopup(m_popupId));
            negativeClick = negativeClick ?? (() => PopupManager.instance.ClosePopup(m_popupId));
            buttons.OnClickMiddleCallback = middleClick;
            buttons.OnClickPositiveCallback = positiveClick;
            buttons.OnClickNegativeCallback = negativeClick;
        }

        /// <summary>
        /// Set up button click using the callback from the popup parameter
        /// </summary>
        public void SetUpButtonClick() {
            SetUpButtonClick(
                PopupParam.OnClickPositiveCallback,
                PopupParam.OnClickNegativeCallback,
                PopupParam.OnClickMiddleCallback
            );
        }

        /// <summary>
        /// Generate the popup Id
        /// </summary>
        private void GeneratePopupId() {
            var guid = Guid.NewGuid();
            m_popupId = guid.ToString().Substring(0, 5);
        }

        /// <summary>
        /// Call when the popup being open
        /// </summary>
        protected virtual void OnOpen() {
            m_onOpenCallback?.Invoke();
        }

        /// <summary>
        /// Call when the popup completely opened (after play the animation effect)
        /// </summary>
        protected virtual void OnOpened() {
            m_onOpenedCallback?.Invoke();
        }

        /// <summary>
        /// Call when the popup being close
        /// </summary>
        protected virtual void OnClose() {
            m_onCloseCallback?.Invoke();
        }

        /// <summary>
        /// Call when the popup completely closed (after play the close animation effect)
        /// </summary>
        protected virtual void OnClosed() {
            m_onClosedCallback?.Invoke();
        }

        private void Reset() {
            var touchBackLayer = transform.Find("PreventTouchBackLayer");
            m_touchBackLayer = touchBackLayer.GetComponent<Image>();

            var popupContainer = transform.Find("PopupContainer");
            m_contentContainer = popupContainer.gameObject;

            m_isPlayPopupAnimation = true;
        }

        #region implement IPopup interface
        /// <summary>
        /// Open Popup
        /// </summary>
        public virtual void Open() {
            OnOpen();
            if (m_isCanTouchBack) {
                m_touchBackLayer.raycastTarget = false;
            }

            if (m_isPlayPopupAnimation) {
                m_contentContainer.transform.localScale = Vector3.zero;
                m_contentContainer.transform.DOScale(1, PopupAnimationDuration).SetEase(Ease.InBack)
                    .OnComplete(OnOpened);
            } else {
                OnOpened();
            }

            gameObject.SetActive(true);
        }

        /// <summary>
        /// Close Popup.
        /// </summary>
        /// <param name="isDestroyPopup">Is Destroy this popup after close or just set popup un active</param>
        public virtual void Close(bool isDestroyPopup = true) {
            OnClose();

            if (m_isPlayPopupAnimation) {
                m_contentContainer.transform.DOScale(0, PopupAnimationDuration).SetEase(Ease.InBack)
                    .OnComplete(() => {
                        OnClosed();
                        if (isDestroyPopup) {
                            PopupManager.instance.PopupLoader.DestroyPopup(gameObject);
                        } else {
                            gameObject.SetActive(false);
                        }
                    });
            } else {
                OnClosed();
                if (isDestroyPopup) {
                    PopupManager.instance.PopupLoader.DestroyPopup(gameObject);
                } else {
                    gameObject.SetActive(false);
                }
            }
        }

        /// <summary>
        /// On Android device, it have the back key button, when that button click, this method will be call.
        /// If not be inherit than this will be close popup button
        /// </summary>
        public virtual void OnClickedNativeBackButton() {
            Close();
        }
        #endregion
    }
}