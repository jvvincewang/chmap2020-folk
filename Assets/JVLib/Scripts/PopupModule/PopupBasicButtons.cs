using UnityEngine;
using UnityEngine.Events;

namespace JVLib.Scripts.PopupModule {
    public class PopupBasicButtons : MonoBehaviour {
        [SerializeField]
        private GameObject m_positiveButton;

        [SerializeField]
        private GameObject m_negativeButton;

        [SerializeField]
        private GameObject m_middleButton;
        
        public UnityAction OnClickPositiveCallback;
        public UnityAction OnClickNegativeCallback;
        public UnityAction OnClickMiddleCallback;

        public GameObject PositiveButton => m_positiveButton;

        public GameObject NegativeButton => m_negativeButton;

        public GameObject MiddleButton => m_middleButton;

        public void OnClickPositiveButton() {
            OnClickPositiveCallback?.Invoke();
        }

        public void OnClickNegativeButton() {
            OnClickNegativeCallback?.Invoke();   
        }

        public void OnClickMiddleButton() {
            OnClickMiddleCallback?.Invoke();
        }
    }
}