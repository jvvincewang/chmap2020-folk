﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class FindAutocomplete : MonoBehaviour
{
    [SerializeField] InputField ipfAddress;
    [SerializeField] RectTransform suggestParent;
    [SerializeField] GameObject suggestButton;
    [SerializeField] int limitSuggest = 2;
    string _finalResult = "";
    private void Start()
    {
        //Remove old child
        for (int i = 0; i < limitSuggest; i++)
        {
            RectTransform rect = Instantiate(suggestButton, suggestParent).GetComponent<RectTransform>();
            rect.anchoredPosition = new Vector2(0, 65 * i + 1);
            rect.gameObject.SetActive(false);
            rect.GetComponent<AutoCompleleAddress>().SetInputAddress(ipfAddress, this);
        }
        suggestParent.sizeDelta = new Vector2(800, 65 * limitSuggest + limitSuggest * 2);
        suggestParent.gameObject.SetActive(false);

        //Add event
        ipfAddress.onValueChanged.AddListener(OnTextChange);
    }

    public static bool IsPointerOverUI(string tag)
    {
        PointerEventData eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        List<RaycastResult> raysastResults = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, raysastResults);
        foreach (RaycastResult raysastResult in raysastResults)
        {
            if (raysastResult.gameObject.name.Contains(tag))
            {
                return true;
            }
        }
        return false;
    }

    void Update()
    {
       if(Input.GetMouseButtonDown(0))
        {
            if (!IsPointerOverUI("Address"))
            {
                suggestParent.gameObject.SetActive(false);
            }
        }    
    }

    public void OnTextChange(string text)
    {
        if (string.IsNullOrEmpty(text) || text == _finalResult)
        {
            suggestParent.gameObject.SetActive(false);
            return;
        }    
        MapController.instance.PlacesAutocomplete(text, OnComplete);
    }

    private void OnComplete(string response)
    {
        // Trying to get an array of results.
        OnlineMapsGooglePlacesAutocompleteResult[] results = OnlineMapsGooglePlacesAutocomplete.GetResults(response);

        // If there is no result
        if (results == null)
        {
            Debug.Log("Error");
            Debug.Log(response);
            return;
        }

        for (int i = 0; i < limitSuggest; i++)
        {
            suggestParent.GetChild(i).gameObject.SetActive(false);
        }
        int index = 0;
        foreach (OnlineMapsGooglePlacesAutocompleteResult result in results)
        {
            Debug.Log(result.description);
            suggestParent.GetChild(index).GetComponentInChildren<Text>().text = result.description;
            suggestParent.GetChild(index).gameObject.SetActive(true);
            index++;
            if(index >= limitSuggest)
            {
                break;
            }
        }
        if(index != 0)
        {
            suggestParent.gameObject.SetActive(true);
            if(index < limitSuggest)
            {
                suggestParent.sizeDelta = new Vector2(800, 65 * index + index * 2);
            }
            else
            {
                suggestParent.sizeDelta = new Vector2(800, 65 * index + index * 2);
            }
        }
    }

    public void CompleteAddress(string result)
    {
        _finalResult = result;
        ipfAddress.text = result;
        suggestParent.gameObject.SetActive(false);
        MapController.instance.UpdateLocationFromAddress(result);
    }
}
