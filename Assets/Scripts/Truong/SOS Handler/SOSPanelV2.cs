﻿using API;
using API.Request.SosMap.Generated;
using App.Services;
using App.UI;
using Gravitons.UI.Modal;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SOSPanelV2 : BaseUIPanel
{
    [SerializeField] InputField ipfOtherProblem;
    [SerializeField] InputField ipfUserName;
    [SerializeField] InputField ipfPhoneNumber;
    [SerializeField] InputField ipfAddress;
    [SerializeField] InputField ipfNote;
    [Space]
    [SerializeField] Transform toggleParent;
    [SerializeField] ToggleGroup toggleGroup;
    [Space]
    [Header("Panel")]
    [SerializeField] GameObject sosButtonPanel;
    [SerializeField] GameObject problemPanel;
    [SerializeField] GameObject infoPanel;
    [SerializeField] GameObject findingPanel;
    [SerializeField] GameObject resultPanel;
    [SerializeField] GameObject helperInfoPanel;
    [Space]
    [SerializeField] Text locationSelecting;

    [Space]
    [Header("Finding Result Option")]
    [SerializeField] float timeOut;
    [SerializeField] Image findingResultImage;
    [SerializeField] Sprite findingSuccessSprite;
    [SerializeField] Sprite findingFaileSprite;
    [SerializeField] Sprite findingCancelSprite;
    [SerializeField] Sprite findingCancel3TimesSprite;
    [SerializeField] Color findingSuccesColor = new Color(35, 252, 255);
    [Space]
    [SerializeField] Text findingResultTitle;
    [SerializeField] Text findingResultDescription;
    [SerializeField] Text findingResultButtonLable;
    [SerializeField] GameObject findingResultButtonCancel;
    [SerializeField] GameObject heplerInfo;
    [Space]
    [Header("Info helper")]
    [SerializeField] Text helperName;
    [SerializeField] Text helperPhone;
    [SerializeField] Text helperNote;
    [Space]
    [SerializeField] Animator findingAnim;

    string _userName;
    string _phoneNumber;
    string _address;
    string _note;
    string _problem = "";

    PhoneCall _mobilePhone = new PhoneCall("0");

    int _totalCancel = 0;
    SOSSTATUS _sosStatus = SOSSTATUS.NONE;
    DestinationSosChannel _channel;
    bool _isMine = true;

    private void Start()
    {
        findingResultButtonCancel.SetActive(false);
        OnlineMapsControlBase.instance.OnMapRelease += ChangeLocation;
    } 

    public void CreateSOSClick()
    {
        sosButtonPanel.SetActive(false);
        problemPanel.SetActive(true);
    } 
    
    public void OnSOSProblemClick()
    {
        Toggle[] toggles = toggleParent.GetComponentsInChildren<Toggle>();
        foreach(Toggle toggle in toggles)
        {
            if(toggle.isOn)
            {
                _problem += toggle.GetComponentInChildren<Text>().text + ";";
            }    
        }
        //Get location for next popup
        ChangeLocation();
        ipfAddress.text = _address;

        problemPanel.SetActive(false);
        infoPanel.SetActive(true);
    }    
    public void OnOkButtonInfoClick()
    {
        //if (string.IsNullOrEmpty(ipfUserName.text))
        //{
        //    Text placeHolder = ipfUserName.placeholder.GetComponent<Text>();
        //    placeHolder.color = Color.red;
        //    placeHolder.text = "Vui lòng nhập tên của bạn!...";
        //    return;
        //}

        if (string.IsNullOrEmpty(ipfPhoneNumber.text))
        {
            Text placeHolder = ipfPhoneNumber.placeholder.GetComponent<Text>();
            placeHolder.color = Color.red;
            placeHolder.text = "Vui lòng nhập số điện thoại!...";
            return;
        }
        if(string.IsNullOrEmpty(ipfAddress.text))
        {
            Text placeHolder = ipfAddress.placeholder.GetComponent<Text>();
            placeHolder.color = Color.red;
            placeHolder.text = "Vui lòng nhập địa chỉ!...";
            return;
        }

        //Setup value
        _userName = ipfUserName.text;
        _phoneNumber = ipfPhoneNumber.text;
        _address = ipfAddress.text;
        _note = ipfNote.text;

        //Create New SOS
        CreateNewSos();
    }

    private void ChangeLocation()
    {
        Location selectLocaiton = MapController.instance.GetLocationAt(new Vector2(Screen.width / 2, Screen.height / 2));
        MapController.instance.GetAddressByLocation(selectLocaiton.ToVector2(), (s) =>
        {
            OnlineMapsGooglePlacesResult[] results = OnlineMapsGooglePlaces.GetResults(s);

            // If there is no result
            if (results == null)
            {
                Debug.Log("Error");
                Debug.Log(s);
                return;
            }
            string locate = results[0].formatted_address;
            float distance = Vector2.Distance(selectLocaiton.ToVector2(), results[0].location);

            //MyLocation.instance.SetPosition(results[0].location.x, results[0].location.y);
            //foreach (OnlineMapsGooglePlacesResult result in results)
            //{
            //    float tempDistance = Vector2.Distance(selectLocaiton.ToVector2(), result.location);
            //    if (tempDistance < distance)
            //    {
            //        locate = result.formatted_address;
            //        _address = result.formatted_address;
            //        distance = tempDistance;
            //    }
            //}
            _address = locate;
            locationSelecting.text = locate;
        });
    }    

    public void OnSelectLocationConfirm()
    {
        ipfAddress.text = locationSelecting.text;
    }    

    public void OnCancelFindSOSClick()
    {
        StopCoroutine(FindingSOS());
        findingPanel.SetActive(false);
        resultPanel.SetActive(true);
        heplerInfo.SetActive(false);

        //Update UI
        findingResultImage.sprite = findingCancelSprite;
        findingResultTitle.text = "Bạn có muốn hủy SOS?";
        findingResultDescription.text = "Hệ thông cảnh báo, bạn không được tiếp tục nếu hủy tìm kiếm liên tục 3 lần.";
        findingResultButtonLable.text = "Tìm tiếp";

        findingResultButtonCancel.SetActive(true);

        _totalCancel += 1;
        //Show warning when canceling 3 times
        if (_totalCancel >= 3)
        {
            findingResultImage.sprite = findingCancel3TimesSprite;
            findingResultTitle.text = "Bạn có chắc chắn mình cần giúp đỡ?";
            findingResultDescription.text = "Hệ thông cảnh báo, bạn không được tiếp tục nếu hủy tìm kiếm liên tục 3 lần.";
        }

        _sosStatus = SOSSTATUS.CANCEL;
    }

    public void OnResultButtonClick()
    {
        switch(_sosStatus)
        {
            case SOSSTATUS.NONE:
            case SOSSTATUS.CANCEL:
            case SOSSTATUS.FAIL:
                CreateNewSos();
                break;
            case SOSSTATUS.SUCCESS:
                {
                    resultPanel.SetActive(false);
                    helperInfoPanel.SetActive(true);
                }
                break; // Show handle on success here
        }    
    }    
    void CreateNewSos()
    {
        _isMine = true;
        //Reset UI
        findingResultButtonCancel.SetActive(false);
        findingResultImage.color = Color.red;
        findingResultImage.transform.parent.GetComponent<Image>().color = Color.red;

#if UNITY_EDITOR
        var currentLocation = MapController.instance.GetLocationAt(new Vector2(Screen.width / 2, Screen.height / 2));
#else
        var currentLocation = MyLocation.instance.lastLocation;
#endif
        Debug.LogFormat("CreateNewSos: {0},{1}", _phoneNumber, currentLocation);

        //AppAPIManager.instance.

        var data = new PostSosesRequest.PostObject
        {
            name = string.IsNullOrEmpty(_userName) ? "Guest":_userName,
            approach = "hi",
            description = string.IsNullOrEmpty(_note) ? "Chúng tôi cần giúp đỡ, Cảm ơn!" : _note,
            phone = _phoneNumber,
            type = 1,
            issue = string.IsNullOrEmpty(_problem) ? "Unknown" : _problem,
            location = new LocationObject()
            {
                type = "Point",
                coordinates = new float[2]
                    {
                        (float) currentLocation.longitude, (float) currentLocation.latitude
                    }
            },
        };

        AppAPIManager.instance.PostSos(data, (res) =>
        {
            Debug.Log("PostSos.DONE");
            string chanelId = res.Data.Channel.ChannelId;
            _channel = SosChannel.CreateChannel<DestinationSosChannel>(chanelId);
            _channel.onConnected = OnConnectedSOS;
            _channel.onDisconected = OnDisconnectedSOS;
            _channel.onMeet = OnMeetSOS;
            _channel.OnRefuseSOS = OnHelperRefuseSOS;

            _channel.onOtherDeviceConnected = () => { _sosStatus = SOSSTATUS.SUCCESS; };

            _channel.location = currentLocation;

            infoPanel.SetActive(false);
            findingPanel.SetActive(true);
            resultPanel.SetActive(false);
            heplerInfo.SetActive(false);
            StartFinding();
        }, ()=>
        {
            findingResultImage.sprite = findingFaileSprite;
            findingResultTitle.text = "Lỗi máy chủ!";
            findingResultDescription.text = "Không thể khởi tạo sos, bạn vui lòng thử lại sau ?";
            findingResultButtonLable.text = "TÌM LẠI";

            infoPanel.SetActive(false);
            findingPanel.SetActive(false);
            resultPanel.SetActive(true);
            heplerInfo.SetActive(false);

        });

    }

    private void StartFinding()
    {
        StartCoroutine(FindingSOS());
    }    

    IEnumerator FindingSOS()
    {
        float startTime = 0f;
        while(startTime <= timeOut && _sosStatus != SOSSTATUS.SUCCESS)
        {
            yield return new WaitForSeconds(0.5f);
            startTime += 0.5f;
        }
        if(_sosStatus == SOSSTATUS.SUCCESS)
        {
            onFindingSOSSuccess();
        }    
        else
        {
            onFindingSOSFail();
        }    
    }    

    private void onFindingSOSSuccess()
    {
        //findingResultImage.sprite = findingSuccessSprite;
        //findingResultImage.color = findingSuccesColor;
        //findingResultImage.transform.parent.GetComponent<Image>().color = findingSuccesColor;

        //findingResultTitle.text = "Tìm kiếm thành công!";
        //findingResultDescription.text = "Đã tìm thấy ở gần bạn.\nNgười giúp đỡ bạn sẽ đến trong ít phút nữa.";
        //findingResultButtonLable.text = "OK";

        //findingPanel.SetActive(false);
        //resultPanel.SetActive(true);
        //heplerInfo.SetActive(true); //hide until it has function

        UpdateHelperInfo();

        findingPanel.SetActive(false);
        helperInfoPanel.SetActive(true);
    }

    private void onFindingSOSFail()
    {
        findingResultImage.sprite = findingFaileSprite;
        findingResultTitle.text = "Không tìm thấy!";
        findingResultDescription.text = "Hệ thống hiện tại không tìm thấy cứu hộ ở gần bạn, bạn có muốn tiếp tục tìm kiếm ?";
        findingResultButtonLable.text = "TÌM LẠI";

        heplerInfo.SetActive(false);
        findingPanel.SetActive(false);
        resultPanel.SetActive(true);
    }

    private void OnConnectedSOS()
    {
        if(!_isMine)
        {
            _sosStatus = SOSSTATUS.SUCCESS;
            //infoPanel.SetActive(false);
            //resultPanel.SetActive(false);
            //findingPanel.SetActive(true);
        }
        else
        {
            _isMine = false;
        }    
        Debug.Log("channel SOS connected");
    }    

    private void OnDisconnectedSOS()
    {
        _sosStatus = SOSSTATUS.FAIL;
        Debug.Log("channel SOS disconnected");
    }    

    public void OnOtherProblemChange()
    {
        toggleGroup.SetAllTogglesOff();
    }    

    private void OnMeetSOS()
    {
        helperInfoPanel.SetActive(false);
        sosButtonPanel.SetActive(true);
        ModalManager.Show("Thông báo", "Đội cứu trợ đã đến", "Ok", null);
    }

    private void OnHelperRefuseSOS()
    {
        resultPanel.SetActive(false);
        helperInfoPanel.SetActive(false);
        sosButtonPanel.SetActive(true);
        ModalManager.Show("Thông báo", "Đội cứu trợ đã đã hủy giúp đỡ!", "Ok", null);
    }    

    //helper info
    public void UpdateHelperInfo()
    {
        API.APIData.CommonUserExtendInfo info = _channel.GetHelperInfo();
        helperName.text = info.fullName;
        helperPhone.text = info.phone;
        helperNote.text = "Lời nhắn: " + (string.IsNullOrEmpty(info.Note) ? "Tôi tới ngay!" : info.Note) ;
    }    

    public void OnRefuseSOSClick()
    {
        _channel?.Refuse();
        helperInfoPanel.SetActive(false);
        sosButtonPanel.SetActive(true);
    }    

    public void OnCallButtonClick()
    {
        _mobilePhone.CallTo(helperPhone.text);
        //Application.OpenURL($"tel:{helperPhone.text}");
    }    

    //end
    protected override void OnClose()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnOpen()
    {
        sosButtonPanel.SetActive(true);
        problemPanel.SetActive(false);
        infoPanel.SetActive(false);
        findingPanel.SetActive(false);
        resultPanel.SetActive(false);
        //Update user location before get address
        MyLocation.instance.OnClickShowCurrentLocation();
        ChangeLocation();
    }


}
enum SOSSTATUS
{
    NONE,
    CANCEL,
    FAIL,
    SUCCESS
}