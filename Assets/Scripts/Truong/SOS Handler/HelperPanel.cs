﻿using API;
using API.Request.SosMap.Generated;
using App.Services;
using App.UI;
using Gravitons.UI.Modal;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HelperPanel : BaseUIPanel
{
    [SerializeField] InputField ipfUserName;
    [SerializeField] InputField ipfPhoneNumber;
    [SerializeField] InputField ipfNote;
    [Space]
    [SerializeField] GameObject panelInfo;
    [SerializeField] GameObject panelHelper;
    [SerializeField] SOSPanelV2 sosPanel;
    [Space]
    [Header("Info User")]
    [SerializeField] Text userName;
    [SerializeField] Text phoneNumber;
    [SerializeField] Text userNote;

    PhoneCall _mobilePhone = new PhoneCall("0");

    UnityAction _acceptCallback;
    UnityAction _cancelCallback;
    SosObject _sosData;
    HelperSosChannel _channel;  

    protected override void OnClose()
    {
        //throw new System.NotImplementedException();
    }

    protected override void OnOpen()
    {
        //Init panel
        panelInfo.SetActive(true);
        panelHelper.SetActive(false);
    }

    public void AddListener(UnityAction acceptCallback, UnityAction cancelCallback)
    {
        _acceptCallback = acceptCallback;
        _cancelCallback = cancelCallback;
    }    

    public void UpdateInfo(SosObject sosData)
    {
        _sosData = sosData;
        userName.text = sosData.Name;
        phoneNumber.text = string.IsNullOrEmpty(sosData.Phone) ? "123456789" : sosData.Phone;
        userNote.text = "Lời nhắn: " + sosData.Description;
    }    

    public void OnConfirmInforClick()
    {
        if(string.IsNullOrEmpty(ipfUserName.text))
        {
            Text placeHolder = ipfUserName.placeholder.GetComponent<Text>();
            placeHolder.color = Color.red;
            placeHolder.text = "Vui lòng tên của bạn...";
            return;
        }
        if (string.IsNullOrEmpty(ipfPhoneNumber.text))
        {
            Text placeHolder = ipfPhoneNumber.placeholder.GetComponent<Text>();
            placeHolder.color = Color.red;
            placeHolder.text = "Vui lòng số điện thoại...";
            return;
        }
        _acceptCallback?.Invoke();
        OnAcceptRequest();
        panelInfo.SetActive(false);
        panelHelper.SetActive(true);
    }

    public void OnCallClick()
    {
        _mobilePhone.CallTo(_sosData.Phone);
    }    

    public void OnConductorClick()
    {
        Location me = MyLocation.instance.lastLocation;
        Location target = new Location((double)_sosData.Location.coordinates[0], (double)_sosData.Location.coordinates[1]);
        //Debug.Log("Conductor : " + System.Environment.NewLine + "Me : " + me.latitude + "," + me.latitude + "\ntarget :" + target.latitude + "," + target.longitude);
        string url = "https://www.google.com/maps/dir/?api=1&origin="+ me.latitude+ ","+ me.longitude +"&destination="+ target.latitude +"," + target.longitude;
        Application.OpenURL(url);
    }

    void OnAcceptRequest()
    {
        //TODO: "soses/{0}/help"
        Debug.Log($"notiSosData.Id: {_sosData.Id}");
        var req = AppAPIManager.instance.CreateRequest<PostSosHelpRequest>(UnityWebRequest.kHttpVerbPOST, (object)_sosData.Id);
        req.Send(res =>
        {
            Debug.Log("PostSosHelpRequest Success");
            Debug.Log("Channel socket PN :" + res.Data.Channel.ChannelId);
            string chanelId = res.Data.Channel.ChannelId;
            _channel = SosChannel.CreateChannel<HelperSosChannel>(chanelId);
            _channel.InitInfo(ipfUserName.text, ipfPhoneNumber.text, ipfNote.text);
            _channel.OnRefuseSOS = OnRefuseSOS;
            _channel.onMeet = OnMeetSOS;
            _channel.sosData = _sosData;
        }, (arg0, err) =>
        {
            Debug.LogError($"<color=red>Error PostSosHelpRequest: {err}</color>");
        });
    }

    public void OnCancelSOSClick()
    {
        _channel.CancelSOS();
        _cancelCallback?.Invoke();
        this.Close();
    }    

    private void OnRefuseSOS()
    {
        _cancelCallback?.Invoke();
        this.Close();
        ModalManager.Show("Hủy SOS", "Người dùng đã hủy SOS!", "OK", null);
    }    

    private void OnMeetSOS()
    {
        _cancelCallback?.Invoke();
        this.Close();
        ModalButton[] buttons = new ModalButton[] { new ModalButton { Text = "OK", Callback = null } };
        ModalManager.Show("Hoàn thành", "Bạn đã tới nơi!", buttons);
    }    

    public void AcceptSOSClick()
    {
        TestTarget();
        //_acceptCallback?.Invoke();
    }


    OnlineMapsMarker targetMarker = null;
    private void TestTarget()
    {
        Location me = MyLocation.instance.lastLocation;
        Location target = MapController.instance.GetLocationAt(new Vector2(Screen.width / 2, Screen.height / 3));

        if (me == null || target == null)
        {
            Debug.LogError("Location cannot be null");
        }
        else
        {
            if (targetMarker == null)
            {
                targetMarker = OnlineMapsMarkerManager.CreateItem(target.longitude, target.latitude, Database.DataAssets.Image.place.icon.texture, "");
            }
            else
            {
                targetMarker.SetPosition(target.longitude, target.latitude);
            }
        }
    }    

    public void CancelSOSClick()
    {
        panelInfo.SetActive(false);
        panelHelper.SetActive(false);
        this.Close();
        sosPanel.Open();
        _cancelCallback?.Invoke();
        _channel?.Close();
    }    

}
