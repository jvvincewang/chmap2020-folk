﻿using UnityEngine;
using UnityEngine.UI;

public class ToggleProblemHandler : MonoBehaviour
{
    Toggle _Toggle;
    [SerializeField] Image imgBackground;
    [SerializeField] Image imgCheckMark;
    [SerializeField] Text txtLabel;
    [SerializeField] Color colorOn;
    [SerializeField] Color colorOff;
    private void Start()
    {
        _Toggle = GetComponent<Toggle>();
    }
    public void OnToggleChange()
    {
        if(_Toggle.isOn)
        {
            imgBackground.color = colorOn;
            imgCheckMark.color = colorOn;
            txtLabel.color = colorOn;
        }
        else
        {
            imgBackground.color = colorOff;
            imgCheckMark.color = colorOff;
            txtLabel.color = colorOff;
        }
    }
}
