﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoCompleleAddress : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] Text addressSugesst;
    private InputField _inputAddress;
    private FindAutocomplete _autoCompleteAddress;

    public void OnSuggestAddressClick()
    {
        _autoCompleteAddress.CompleteAddress(addressSugesst.text);
    }

    public void SetInputAddress(InputField input, FindAutocomplete findAutocomplete)
    {
        _inputAddress = input;
        _autoCompleteAddress = findAutocomplete;
    }
}
