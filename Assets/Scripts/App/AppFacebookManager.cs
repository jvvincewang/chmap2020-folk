using System;
using App.Interface;
using Facebook.Unity;
using UnityEngine;

namespace App {
    public class AppFacebookManager : MonoBehaviourSingletonBase<AppFacebookManager>, IOnAppLaunch {
        private void Start() {
            AppManager.instance.AddOnAppLaunch(this);
        }

        public void OnAppLaunch() {
            if (!FB.IsInitialized) {
                FB.Init(OnInitComplete);
            } else {
                FB.ActivateApp();
            }
        }

        private void OnInitComplete() {
            if (FB.IsInitialized) {
                FB.ActivateApp();
            } else {
                Debug.LogError("[FacebookAppManager] Init facebook app failed");
            }
        }
    }
}