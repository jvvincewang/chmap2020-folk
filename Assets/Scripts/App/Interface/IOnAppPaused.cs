namespace App.Interface {
    /// <summary>
    /// Interface that will be call when application paused or resumed.
    /// <seealso cref="AppManager.AddOnAppPaused"/>
    /// </summary>
    public interface IOnAppPaused {
        void OnAppPaused(bool isPaused);
    }
}