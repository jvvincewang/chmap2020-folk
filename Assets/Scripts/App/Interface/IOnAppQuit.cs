namespace App.Interface {
    /// <summary>
    /// Interface that will be call before application completely quit. 
    /// <seealso cref="AppManager.AddOnAppQuit"/>
    /// </summary>
    public interface IOnAppQuit {
        void OnAppQuit();
    }
}