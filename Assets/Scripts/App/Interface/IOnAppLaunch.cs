namespace App.Interface {
    /// <summary>
    /// Interface that will be call when application launched.
    /// THIS INTERFACE SHOULD BE USE WITH THE SINGLETON OBJECT ONLY. THIS INTERFACE WILL BE CALL ONLY ONE TIME SINCE APP START
    /// (Best fit for the init process of singleton object)
    /// <seealso cref="AppManager.AddOnAppLaunch"/>
    /// </summary>
    public interface IOnAppLaunch {
        void OnAppLaunch();
    }
}