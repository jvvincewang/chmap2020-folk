using System;
using App.NavigationBar.Interfaces;
using UniRx;
using UniRx.Triggers;
using UnityEngine;

namespace App.NavigationBar.View {
    public class TabBaseView : MonoBehaviour, INavigationBarView {
        [SerializeField]
        private string _viewId;
        public string ViewId => _viewId;
        
        public IObservable<Unit> ReloadedView() {
            return gameObject.OnEnableAsObservable();
        }
        public void EnableView(bool isEnable) {
            gameObject.SetActive(isEnable);
        }
    }
}