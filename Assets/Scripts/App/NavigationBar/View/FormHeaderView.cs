using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace App.NavigationBar.View {
    public class FormHeaderView : MonoBehaviour {
        [SerializeField]
        private Text _formTitle;

        [SerializeField]
        private Button _backButton;

        public IObservable<Unit> OnBackButtonClicked() {
            return _backButton.OnClickAsObservable();
        }

        public void SetFormTitleText(string title) {
            _formTitle.text = title;
        }
    }
}