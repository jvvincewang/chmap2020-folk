using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace App.NavigationBar.View.AccountTabView {
    public class AccountView : TabBaseView {
        [Space]
        [Header("Account View Setting")]
        [SerializeField]
        private Text _userNameText;

        [SerializeField]
        private Image _avatarImage;

        [SerializeField]
        private Image _coverImage;

        [SerializeField]
        private Button _accountSettingButton;

        [SerializeField]
        private Button _logoutButton;

        [SerializeField]
        private Button _linkWithFacebookBtn;

        public IObservable<Unit> OnAccountButtonClicked() {
            return _accountSettingButton.OnClickAsObservable();
        }

        public IObservable<Unit> OnAccountLogoutButtonClicked() {
            return _logoutButton.OnClickAsObservable();
        }

        public IObservable<Unit> OnClickLinkGuestWithFacebook() {
            return _linkWithFacebookBtn.OnClickAsObservable();
        }

        public void SetUserAvatar(Sprite avatarImg) {
            _avatarImage.sprite = avatarImg;
        }

        public void SetUserName(string userName) {
            _userNameText.text = userName;
        }

        public void SetLinkWithFacebookBtnInteractable(bool isInteractable) {
            _linkWithFacebookBtn.interactable = isInteractable;
        }
    }
}