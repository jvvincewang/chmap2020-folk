using System;
using UniRx;
using UnityEngine;

namespace App.NavigationBar.View.AccountTabView {
    public class UserPersonalInfoView : MonoBehaviour {
        [SerializeField]
        private FormHeaderView _headerView;

        [SerializeField]
        private FormSlideAnimation _slideAnimation;

        public FormSlideAnimation SlideAnimation => _slideAnimation;

        public IObservable<Unit> OnClickHeaderBackButton() {
            return _headerView.OnBackButtonClicked();
        }
    }
}