using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace App.NavigationBar.View {
    public class NavigationBarView : MonoBehaviour {
        [SerializeField]
        private Button _homeTabBtn;

        [SerializeField]
        private Button _accountTabBtn;

        public IObservable<Unit> HomeOnClickAsObservable() {
            return _homeTabBtn.OnClickAsObservable();
        }

        public IObservable<Unit> AccountOnClickAsObservable() {
            return _accountTabBtn.OnClickAsObservable();
        }
    }
}