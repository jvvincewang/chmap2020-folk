using System;
using System.Collections;
using BayatGames.SaveGameFree;
using UniRx;

namespace App.NavigationBar.Model {
    [Serializable]
    public class AccountModel {
        private ReactiveProperty<string> _userName = new ReactiveProperty<string>();
        private ReactiveProperty<string> _avatarImgPath = new ReactiveProperty<string>();
        private ReactiveProperty<bool> _loggedInWithFacebook = new ReactiveProperty<bool>();

        public IReadOnlyReactiveProperty<string> UserName => _userName;

        public IReadOnlyReactiveProperty<string> AvatarImgPath => _avatarImgPath;

        public IReadOnlyReactiveProperty<bool> LoggedInWithFacebook => _loggedInWithFacebook;

        public void LoadOrUpdateUserInfo() {
            _userName.Value = AppRuntimeData.Data.userName;
            _avatarImgPath.Value = AppRuntimeData.Data.userImage;
            _loggedInWithFacebook.Value = !string.IsNullOrEmpty(AppSaveData.Data.userInfo.fbToken);
        }
    }
}