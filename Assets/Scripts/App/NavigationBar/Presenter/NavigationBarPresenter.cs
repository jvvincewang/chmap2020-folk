using App.NavigationBar.View;
using App.NavigationBar.View.AccountTabView;
using UniRx;
using UnityEngine;

namespace App.NavigationBar.Presenter {
    public class NavigationBarPresenter : MonoBehaviour {
        [Header("Navigation Bar View Setting")]
        [SerializeField]
        private NavigationBarView _navigationBarView;

        [Header("Tab View Setting")]
        [SerializeField]
        private HomeView _homeView;

        [SerializeField]
        private AccountView _accountView;

        [SerializeField]
        private string _defaultView = "home";

        private TabBaseView[] _barViews;

        private void Start() {
            _barViews = new[] {
                (TabBaseView) _homeView,
                _accountView
            };
            Initialize();
        }

        private void SetUpButtonClick() {
            _navigationBarView.HomeOnClickAsObservable().Subscribe(_ => {
                ShowTabView("home");
            }).AddTo(gameObject);
            _navigationBarView.AccountOnClickAsObservable().Subscribe(_ => {
                ShowTabView("account");
            }).AddTo(gameObject);
        }

        private void ShowTabView(string viewId) {
            foreach (var view in _barViews) {
                view.EnableView(view.ViewId == viewId);
            }
        }

        public void Initialize() {
            SetUpButtonClick();
            ShowTabView(_defaultView);
        }
    }
}