using System.Collections.Generic;
using API;
using App.NavigationBar.Model;
using App.NavigationBar.View.AccountTabView;
using Facebook.Unity;
using UnityEngine;
using UniRx;
using UnityEngine.SceneManagement;

namespace App.NavigationBar.Presenter {
    public class AccountPresenter : MonoBehaviour {
        [SerializeField]
        private AccountView _accountView;

        [Header("Account Tab Sub View")]
        [SerializeField]
        private UserPersonalInfoView _userPersonalInfoView;

        private AccountModel _accountModel;

        private void Start() {
            InitAccount();
        }

        public void InitAccount() {
            _accountModel = new AccountModel();
            _accountModel.LoadOrUpdateUserInfo();
            _accountView.ReloadedView()
                .Subscribe(_ => { })
                .AddTo(gameObject);

            _accountModel.LoggedInWithFacebook
                .Subscribe(_ => { _accountView.SetLinkWithFacebookBtnInteractable(!_); })
                .AddTo(gameObject);
            _accountModel.UserName
                .Subscribe(_ => { _accountView.SetUserName(_); })
                .AddTo(gameObject);

            // Button click observable
            _accountView.OnAccountButtonClicked().Subscribe(_ => {
                Debug.Log("Clicked account view");
                _userPersonalInfoView.SlideAnimation.SlideIn();
                _userPersonalInfoView.OnClickHeaderBackButton().Subscribe(back => {
                    _userPersonalInfoView.SlideAnimation.SlideOut();
                    _accountView.ReloadedView();
                });
            }).AddTo(gameObject);

            _accountView.OnAccountLogoutButtonClicked()
                .Subscribe(_ => { LogoutUser(); })
                .AddTo(gameObject);

            _accountView.OnClickLinkGuestWithFacebook()
                .Subscribe(_ => { LinkGuestWithFacebook(); })
                .AddTo(gameObject);
        }

        private void LinkGuestWithFacebook() {
            var perms = new List<string> {"public_profile", "email"};
            FB.LogInWithReadPermissions(perms, FbAuthCallback);
        }

        private void FbAuthCallback(ILoginResult result) {
            if (FB.IsLoggedIn) {
                // AccessToken class will have session details
                var aToken = AccessToken.CurrentAccessToken;
                
                // Print current access token's User ID
                Debug.Log(aToken.UserId);
                Debug.Log($"User token:{aToken.TokenString}");

                // Print current access token's granted permissions
                foreach (var perm in aToken.Permissions) {
                    Debug.Log(perm);
                }

                AppAPIManager.instance.LinkGuestUserWithFacebook(aToken.TokenString,
                    () => {
                        _accountModel.LoadOrUpdateUserInfo(); 
                    });
            } else {
                Debug.Log("User cancelled login");
            }
        }

        private void LogoutUser() {
            Debug.Log("Logout user");
            // clear all executing request
            AppAPIManager.instance.ClearAllRequest();

            // Clear user login session
            AppRuntimeData.instance.ClearUserLoginSession();

            // Clear user data
            AppSaveData.Data.ClearUserMainInfo();
            AppSaveData.Save();

            // Unload all active scene then reload the login scene
            var loadedSceneCnt = SceneManager.sceneCount;
            for (int i = 0; i < loadedSceneCnt; i++) {
                var scene = SceneManager.GetSceneAt(i);
                if (scene.name == "Main") {
                    continue;
                }

                SceneManager.UnloadSceneAsync(scene);
            }

            SceneManager.LoadSceneAsync(Config.AppSceneConfig.LoginScene);
        }
    }
}