using System;
using UniRx;

namespace App.NavigationBar.Interfaces {
    public interface INavigationBarView {
        string ViewId { get; }
        void EnableView(bool isEnable);
        IObservable<Unit> ReloadedView();
    }
}