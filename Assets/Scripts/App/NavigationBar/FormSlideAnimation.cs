using System;
using DG.Tweening;
using UnityEngine;

namespace App.NavigationBar {
    public class FormSlideAnimation : MonoBehaviour {
        [SerializeField]
        private float _animDuration = .2f;

        private Vector2 _initPos;

        private void Start() {
            var trans = transform;
            var localPos = trans.localPosition;
            var rectTrans = gameObject.GetComponent<RectTransform>();
            Debug.Log($"size delta: {rectTrans.rect.width}");
            localPos.x += rectTrans.rect.width;
            trans.localPosition = localPos;
            _initPos = localPos;
        }

        public void SlideIn() {
            transform.DOLocalMoveX(0, _animDuration)
                .OnStart(() => {
                    gameObject.SetActive(true);
                });
        }

        public void SlideOut() {
            transform.DOLocalMoveX(_initPos.x, _animDuration)
                .OnComplete(() => {
                    gameObject.SetActive(false);
                });
        }
    }
}