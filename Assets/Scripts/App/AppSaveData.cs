using System.Text;
using App.AppData;
using App.Interface;
using BayatGames.SaveGameFree;
using BayatGames.SaveGameFree.Serializers;

namespace App {
    public class AppSaveData : MonoBehaviourSingletonBase<AppSaveData>, IOnAppLaunch {
        private InAppData _inAppData = new InAppData();
        private ISaveGameSerializer _serializer = new SaveGameBinarySerializer();
        private SaveGamePath _saveGamePath = SaveGamePath.PersistentDataPath;
        private bool _hasFinishedInit;

        public static InAppData Data => instance._inAppData;

        public static bool HasFinishedInit => instance._hasFinishedInit;

        private void Start() {
            AppManager.instance.AddOnAppLaunch(this);
        }

        public void OnAppLaunch() {
#if UNITY_EDITOR
            _serializer = new SaveGameJsonSerializer();
            _saveGamePath = SaveGamePath.DataPath;
#endif
            Load();
            _hasFinishedInit = true;
        }

        public static void Save() {
            SaveGame.Save(Config.SaveGameConfig.UserDataIdentifier, Data, true,
                Config.SaveGameConfig.SaveGamePassword, instance._serializer, SaveGame.Encoder, Encoding.UTF8,
                SaveGamePath.PersistentDataPath);
        }

        private static void Load() {
            instance._inAppData = SaveGame.Load(Config.SaveGameConfig.UserDataIdentifier, new InAppData(), true,
                Config.SaveGameConfig.SaveGamePassword, instance._serializer, SaveGame.Encoder, Encoding.UTF8,
                SaveGamePath.PersistentDataPath);
        }
    }
}