using System;
using System.Collections;
using App.Firebase;
using App.Interface;
using API;
using API.Request.SosMap.Generated;
using Cysharp.Threading.Tasks;
using Firebase;
using Firebase.Messaging;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;

namespace App
{
	public class AppFirebaseCloudMessManager : MonoBehaviourSingletonBase<AppFirebaseCloudMessManager>, IOnAppLaunch
	{
		private FirebaseApp _firebaseApp;
		private ReactiveProperty<bool> _hasInit = new ReactiveProperty<bool>();
		private bool _isActiveMess;

		public IReadOnlyReactiveProperty<bool> HasInit => _hasInit;
		public static Action<object, MessageReceivedEventArgs> MessageReceived;
		public static Action<object, TokenReceivedEventArgs> TokenReceived;

		private bool _isOpenFromNotificationWithData = false;
		private string _dataOpenFromNotification;

		public bool ShowSOSAtLaunch
        {
            get
            {
				return _isOpenFromNotificationWithData;
            }
			set
            {
				_isOpenFromNotificationWithData = value;

			}
        }			
		public string SOSData
        {
			get
            {
				return _dataOpenFromNotification;
            }
        }			

		public void Start()
		{
			AppManager.instance.AddOnAppLaunch(this);
		}

		private void FirebaseMessagingOnMessageReceived(object sender, MessageReceivedEventArgs e)
		{
			Debug.Log($"<color=lime>[FCM Manager]</color>Received a new message from: {e.Message.From} \n{e.Message.MessageId}");

			//Check data open from notification
			string msg = "";
			var data = e.Message.Data;
			if (data.Count > 0)
			{
				foreach (var item in data)
				{
					msg += $"[{item.Key}]: " + item.Value + System.Environment.NewLine;
				}
			}

			Debug.Log("<color=lime>[FCM Manager]</color>Received a new message data:" + System.Environment.NewLine + msg);

			if (data.ContainsKey("data"))
			{
				_dataOpenFromNotification = data["data"];
				_isOpenFromNotificationWithData = true;
			}
			else
            {
				_isOpenFromNotificationWithData = false;
            }				
			//end check

			MessageReceived?.Invoke(sender, e);
		}

		private void FirebaseMessagingOnTokenReceived(object sender, TokenReceivedEventArgs token)
		{
			Debug.Log($"<color=lime>[FCM Manager]</color>Received a new Token: {token.Token}");
			TokenReceived?.Invoke(sender, token);
			StartCoroutine(UpdateFcmTokenToServer(token.Token, true));
		}

		public void OnAppLaunch()
		{
			AppFirebaseCore.instance.FinishedInit.Subscribe(init =>
			{
				if (init)
				{
					_firebaseApp = FirebaseApp.DefaultInstance;
					InitializeFirebaseCloudMess();
				}
			}).AddTo(gameObject);
		}

		private void InitializeFirebaseCloudMess()
		{
			FirebaseMessaging.TokenReceived += FirebaseMessagingOnTokenReceived;
			FirebaseMessaging.MessageReceived += FirebaseMessagingOnMessageReceived;
			Debug.Log("<color=lime>[FCM Manager]</color> Finished FCM Initialize");
			// This will display the prompt to request permission to receive
			// notifications if the prompt has not already been displayed before. (If
			// the user already responded to the prompt, thier decision is cached by
			// the OS and can be changed in the OS settings).
			FirebaseMessaging.RequestPermissionAsync().AsUniTask()
				.ContinueWith(() =>
				{
					Debug.Log("<color=lime>[FCM Manager]</color> Finished ask for permission");
				});
			_hasInit.Value = true;
		}

		private IEnumerator UpdateFcmTokenToServer(string fcmToken, bool isActive)
		{
			// wait for user login
			yield return new WaitUntil(() => !string.IsNullOrEmpty(AppRuntimeData.Data.token));
			Debug.Log("<color=lime>[FCM Manager]</color>Call the update notification to server");
			var req = AppAPIManager.instance.CreateRequest<PutNotificationSettingRequest>(UnityWebRequest.kHttpVerbPUT);
			var data = new PutNotificationSettingRequest.PostObject
			{
				isActive = isActive,
					firebaseToken = fcmToken
			};
			req.SetPostData(data);
			req.Send(res =>
			{
				Debug.Log("<color=lime>[FCM Manager]</color>Finished update notification token");
			}, (arg0, s) =>
			{
				Debug.LogError($"<color=lime>[FCM Manager]</color>Failed to update token: {s}");
			});
		}
	}
}