using App.Interface;
using Firebase;
using Firebase.Extensions;
using UniRx;
using UnityEngine;

namespace App.Firebase {
    /// <summary>
    /// This class should ONLY call from other firebase SINGLETON class
    /// </summary>
    public class AppFirebaseCore : MonoBehaviourSingletonBase<AppFirebaseCore>, IOnAppLaunch {
        private FirebaseApp _appFirebase;
        private ReactiveProperty<bool> _finishedInit = new ReactiveProperty<bool>();

        public IReadOnlyReactiveProperty<bool> FinishedInit => _finishedInit;
        public FirebaseApp AppFirebase => _appFirebase;

        private void Start() {
            AppManager.instance.AddOnAppLaunch(this);
        }

        public void OnAppLaunch() {
            FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task => {
                var status = task.Result;
                if (status == DependencyStatus.Available) {
                    _appFirebase = FirebaseApp.DefaultInstance;
                    _finishedInit.Value = true;
                } else {
                    Debug.LogError($"<color=lime>[FCM Manager]</color>Could not resolve all Firebase dependencies: {status}");
                }
            });
        }
    }
}