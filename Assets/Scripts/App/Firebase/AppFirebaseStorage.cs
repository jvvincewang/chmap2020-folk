using System;
using System.Threading.Tasks;
using App.Interface;
using Common;
using Cysharp.Threading.Tasks;
using Cysharp.Threading.Tasks.Linq;
using Firebase;
using Firebase.Extensions;
using Firebase.Storage;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace App.Firebase {
    public class AppFirebaseStorage : MonoBehaviourSingletonBase<AppFirebaseStorage>, IOnAppLaunch {
        private FirebaseStorage _storage;
        private StorageReference _storageRootRef;
        private StorageReference _userFileRootRef;

        /// <summary>
        /// Default time out of unity web request task. (in seconds)
        /// </summary>
        private const int DefaultTimeOut = 60;

        public const string DefaultCommonAvatar = "G.png";

        private bool _isFinishedInit;

        public bool IsFinishedInit => _isFinishedInit;

        public void Start() {
            AppManager.instance.AddOnAppLaunch(this);
        }

        public void OnAppLaunch() {
            AppFirebaseCore.instance.FinishedInit.Subscribe(init => {
                if (init) {
                    _storage = FirebaseStorage.DefaultInstance;
                    _storageRootRef = _storage.GetReferenceFromUrl(Config.UploadFileToFirebaseConfig.RootPath);
                    _userFileRootRef = _storageRootRef.Child(Config.UploadFileToFirebaseConfig.UserFileStorageRootPath);
                    _isFinishedInit = true;    
                }
            }).AddTo(gameObject);
        }

        /// <summary>
        /// Download user avatar.
        /// </summary>
        /// <param name="downloadFinishedCallback">callback after download</param>
        /// <param name="isCommonAvatar">Is download common avatar</param>
        /// <param name="isSaveToLocal">Is save downloaded avatar to local</param>
        public void DownloadUserAvatar(UnityAction<(Texture2D, string)> downloadFinishedCallback, bool isCommonAvatar = true, bool isSaveToLocal = false) {
            var commonAvatar = _userFileRootRef.Child(Config.UploadFileToFirebaseConfig.UserAvatarCommonPath);
            var uploadedAvatar = _userFileRootRef.Child(Config.UploadFileToFirebaseConfig.UserAvatarUploadedPath);
            var dlRef = isCommonAvatar ? commonAvatar : uploadedAvatar;
            dlRef = dlRef.Child(DefaultCommonAvatar);
            Debug.Log($"<color=cyan>[AppFirebaseDl]</color> Start download avatar: {dlRef.Path}");
            var dlRefTask = dlRef.GetDownloadUrlAsync();
            dlRefTask.ContinueWithOnMainThread(t => {
                if (t.IsCanceled || t.IsFaulted) {
                    Debug.LogError($"<color=cyan>[AppFirebaseDl]</color>Download avatar failed: {t.Exception}");
                } else {
                    var uwr = new UnityWebRequest {
                        uri = t.Result,
                        downloadHandler = new DownloadHandlerTexture()
                    };
                    var dlTask = uwr.SendWebRequest().ToUniTask();
                    dlTask.Timeout(TimeSpan.FromSeconds(DefaultTimeOut))
                        .ContinueWith(dlReq => {
                            var handler = (DownloadHandlerTexture) dlReq.downloadHandler;
                            var texture = handler.texture;
                            Debug.Log($"<color=cyan>[AppFirebaseDl]</color>Finished download avatar {handler.data.Length}");
                            if (isSaveToLocal) {
                                var data = texture.EncodeToPNG();
                                StorageUtil.SaveFileToBytes("avatar.png", data);
                            }
                            downloadFinishedCallback?.Invoke((texture, dlRef.Path));
                        });
                }
            });
        }

        public void UploadUserAvatar(UnityAction<bool> onFinishedCallback, Texture2D image) {
            var uploadedAvatar = _userFileRootRef.Child(Config.UploadFileToFirebaseConfig.UserAvatarUploadedPath);
            var texture = (Texture2D) AdjustImageToUploadSetting(image);
            var imageByte = texture.GetRawTextureData();
            uploadedAvatar.PutBytesAsync(imageByte)
                .ContinueWithOnMainThread(task => {
                    if (task.IsCanceled || task.IsFaulted) {
                        Debug.LogError(task.Exception);
                        onFinishedCallback.Invoke(false);
                    } else {
                        onFinishedCallback.Invoke(true);
                        var metadata = task.Result;
                        var download_url = metadata.Path;
                        Debug.Log("Finished uploading avatar...");
                        Debug.Log("download url = " + download_url);
                    }
                });
        }

        /// <summary>
        /// Adjust image to upload setting
        /// </summary>
        /// <param name="inputImage"></param>
        /// <returns></returns>
        private Texture AdjustImageToUploadSetting(Texture inputImage) {
            var converted = inputImage;
            return converted;
        }
    }
}