namespace App
{
	public static class Config
	{
		public static class SaveGameConfig
		{
			public const string UserDataIdentifier = "userData";
			public const string SaveGamePassword = "eF48NAf6e0";
		}

		public static class UploadFileToFirebaseConfig
		{
			public const string RootPath = "gs://sosmapnet.appspot.com";
			public const string UserFileStorageRootPath = "end-user-data";
			public const string UserAvatarUploadedPath = "avatar/upload";
			public const string UserAvatarCommonPath = "avatar/common";
		}

		public static class StorageConfig
		{
			public const string LocalFileRoot = "user-local-file";
		}

		public static class AppSceneConfig
		{
			public const string MainScene = "Scenes/Main";
			public const string MapScene = "Scenes/chmtmap_Truong";
			public const string LoginScene = "Scenes/LoginScene";
		}

		public static class Security
		{

		}
	}
}