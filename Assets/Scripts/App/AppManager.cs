using System.Collections;
using System.Collections.Generic;
using System.Linq;
using API;
using API.Request.SosMap.Generated;
using App.Firebase;
using App.Interface;
using UniRx;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace App {
    public class AppManager : MonoBehaviourSingletonBase<AppManager> {
        [SerializeField]
        private float _defaultTimeScale = 1f;

        [Range(0, 10)]
        [SerializeField]
        private float _timeScale = 1f;

        [SerializeField]
        private string _defaultScenePath;

        private List<IOnAppLaunch> _onAppLaunchLst = new List<IOnAppLaunch>();
        private List<IOnAppQuit> _onAppQuitLst = new List<IOnAppQuit>();
        private List<IOnAppPaused> _onAppPauseLst = new List<IOnAppPaused>();

        private void Start() {
            // Load the default scene
            StartCoroutine(ExecuteLauncherCallback(() => {
                //if (AppSaveData.HasFinishedInit) {
                //    if (string.IsNullOrEmpty(AppSaveData.Data.userInfo.userInAppId)) {
                //        SceneManager.LoadSceneAsync(Config.AppSceneConfig.LoginScene);
                //    } else {
                //        if (string.IsNullOrEmpty(AppSaveData.Data.userInfo.fbToken)) {
                //            AppAPIManager.instance.Login(() => {
                //                AppAPIManager.instance.FetchNotificationSetting(null);
                //                SceneManager.LoadSceneAsync(_defaultScenePath);
                //            });    
                //        } else {
                //            AppAPIManager.instance.SignUpOrLoginAsFacebookUser(AppSaveData.Data.userInfo.fbToken, () => {
                //                AppAPIManager.instance.FetchNotificationSetting(null);
                //                SceneManager.LoadSceneAsync(_defaultScenePath);
                //            });
                //        }
                        
                //    }
                //}

                // Turn off Login scene base on David Pham request. It will not include Facebook Login
                if(AppSaveData.HasFinishedInit)
                {
                    if(string.IsNullOrEmpty(AppSaveData.Data.userInfo.userInAppId))
                    {
                        SignUpAsGuest();
                    }   
                    else
                    {
                        AppAPIManager.instance.Login(() => {
                            AppAPIManager.instance.FetchNotificationSetting(null);
                            SceneManager.LoadSceneAsync(_defaultScenePath);
                        }, SignUpAsGuest);
                    }   
                }   

            }));
        }

        private void SignUpAsGuest()
        {
            AppAPIManager.instance.SignUpAsGuest("Guest", () =>
            {
                MainThreadDispatcher.SendStartCoroutine(DownloadAvatar());
                Debug.Log($"<color=lime>login success</color>");
                // Change scene
                SceneManager.LoadSceneAsync(Config.AppSceneConfig.MapScene);
            });
        }    
        private static IEnumerator DownloadAvatar()
        {
            yield return new WaitUntil(() => AppFirebaseStorage.instance.IsFinishedInit);
            AppFirebaseStorage.instance.DownloadUserAvatar(t =>
            {
                // Save file path to local 
                AppSaveData.Data.userInfo.userAvatarFilePath = t.Item2;
                AppSaveData.Save();

                // Update user avatar path to server
                var req = AppAPIManager.instance.CreateRequest<PutUserRequest>(UnityWebRequest.kHttpVerbPUT);
                var data = new PutUserRequest.PostObject
                {
                    image = AppSaveData.Data.userInfo.userAvatarFilePath,
                    fullName = AppRuntimeData.Data.userName,
                    email = AppRuntimeData.Data.userEmail,
                    phone = AppRuntimeData.Data.userPhone,
                    location = new LocationObject()
                    {
                        type = "Point",
                        coordinates = new float[2] { 0, 0, }
                    },
                };
                req.SetPostData(data);
                req.Send(res => { }, (arg0, s) => { });
            }, true, true);
        }

        private IEnumerator ExecuteLauncherCallback(UnityAction finishedCallback) {
            yield return new WaitForEndOfFrame();
            foreach (var launcher in _onAppLaunchLst) {
                if (launcher != null) {
                    launcher.OnAppLaunch();
                }
            }

            // Clean up null object
            _onAppLaunchLst = _onAppLaunchLst.Where(x => x != null).ToList();
            if (finishedCallback != null) {
                finishedCallback.Invoke();
            }
        }

        private void Update() {
            if (!Mathf.Approximately(Time.timeScale, _timeScale)) {
                Time.timeScale = _timeScale;
            }
        }

        private void OnApplicationQuit() {
            foreach (var quit in _onAppQuitLst) {
                if (quit != null) {
                    quit.OnAppQuit();
                }
            }

            // Clean up quit list
            _onAppQuitLst = _onAppQuitLst.Where(x => x != null).ToList();
        }

        private void OnApplicationPause(bool pauseStatus) {
            foreach (var callback in _onAppPauseLst) {
                if (callback != null) {
                    callback.OnAppPaused(pauseStatus);
                }
            }

            // Clean up the null object
            _onAppLaunchLst = _onAppLaunchLst.Where(x => x != null).ToList();
        }

        #region Application public process
        /// <summary>
        /// Setting App time scale
        /// </summary>
        /// <param name="timeScale">Time scale value</param>
        public void SetUpAppTimeScale(float timeScale) {
            _timeScale = timeScale;
        }

        /// <summary>
        /// Reset app timescale
        /// </summary>
        public void ResetTimeScale() {
            Time.timeScale = _defaultTimeScale;
        }

        /// <summary>
        /// Add callback which will called when application finished load first frame
        /// </summary>
        public void AddOnAppLaunch(IOnAppLaunch onAppLaunch) {
            _onAppLaunchLst.Add(onAppLaunch);
        }

        /// <summary>
        /// Add callback which will called when application paused or resumed
        /// </summary>
        public void AddOnAppPaused(IOnAppPaused onAppPaused) {
            _onAppPauseLst.Add(onAppPaused);
        }

        /// <summary>
        /// Add callback which will called when application quited
        /// </summary>
        public void AddOnAppQuit(IOnAppQuit onAppQuit) {
            _onAppQuitLst.Add(onAppQuit);
        }
        #endregion
    }
}