using App.AppData;
using App.Interface;

namespace App {
    public class AppRuntimeData : MonoBehaviourSingletonBase<AppRuntimeData>, IOnAppLaunch {
        private RuntimeData _runtimeData = new RuntimeData();
        private bool _hasFinishedInit;

        public static RuntimeData Data => instance._runtimeData;

        public static bool HasFinishedInit => instance._hasFinishedInit;

        private void Start() {
            AppManager.instance.AddOnAppLaunch(this);
        }

        public void OnAppLaunch() {
            _hasFinishedInit = true;
        }

        /// <summary>
        /// Clear user session data, use when user logout
        /// </summary>
        public void ClearUserLoginSession() {
            Data.token = string.Empty;
        }
    }
}