using System.Collections;
using System.Collections.Generic;
using App.Firebase;
using App.Login.View;
using API;
using API.Request.SosMap.Generated;
using Facebook.Unity;
using UniRx;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace App.Login.Presenter
{
	public class LoginSceneMainPresenter : MonoBehaviour
	{
		[SerializeField]
		private LoginSceneMainView _loginMainView;

		[SerializeField]
		private GuestLoginPanelView _guestLoginPanelView;

		private readonly ReactiveProperty<bool> _isProcessingLogin = new ReactiveProperty<bool>();

		private void Start()
		{
			Init();
		}

		private void Init()
		{
			_loginMainView.OnClickedLoginGuest.Subscribe(_ => { _guestLoginPanelView.SlideAnimation.SlideIn(); })
				.AddTo(gameObject);
			_loginMainView.OnClickedConnectWithFacebook.Subscribe(_ => { SignupFacebook(); }).AddTo(gameObject);

			// guest login panel view
			_guestLoginPanelView.OnClickedLoginButton.Subscribe(_ => { SignupGuest(); }).AddTo(gameObject);

			_guestLoginPanelView.OnClickedBackButton.Subscribe(_ =>
			{
				_guestLoginPanelView.SlideAnimation.SlideOut();
				_isProcessingLogin.Value = false;
			}).AddTo(gameObject);

			// freeze login ui when processing login;
			_isProcessingLogin.Subscribe(_ => { _guestLoginPanelView.SetFreezeInteractive(_isProcessingLogin.Value); })
				.AddTo(gameObject);
		}

		private void SignupGuest()
		{
			_isProcessingLogin.Value = true;
			AppAPIManager.instance.SignUpAsGuest(_guestLoginPanelView.GetGuestFullName, () =>
			{
				MainThreadDispatcher.SendStartCoroutine(DownloadAvatar());
				Debug.Log($"<color=lime>login success</color>");
				_isProcessingLogin.Value = false;
				// Change scene
				SceneManager.LoadSceneAsync(Config.AppSceneConfig.MapScene);
			});
		}

		private void SignupFacebook()
		{
			_isProcessingLogin.Value = true;
			var perms = new List<string> { "public_profile", "email" };
			FB.LogInWithReadPermissions(perms, FbAuthCallback);
		}

		/// <summary>
		/// Facebook authentication callback
		/// </summary>
		/// <param name="result"></param>
		private void FbAuthCallback(ILoginResult result)
		{
			if (FB.IsLoggedIn)
			{
				// AccessToken class will have session details
				var aToken = AccessToken.CurrentAccessToken;
				// Print current access token's User ID
				Debug.Log(aToken.UserId);
				Debug.Log($"User token:{aToken.TokenString}");

				// Print current access token's granted permissions
				foreach (var perm in aToken.Permissions)
				{
					Debug.Log(perm);
				}

				AppAPIManager.instance.SignUpOrLoginAsFacebookUser(aToken.TokenString, () =>
				{
					MainThreadDispatcher.SendStartCoroutine(DownloadAvatar());
					Debug.Log($"<color=lime>login success</color>");
					_isProcessingLogin.Value = false;
					// Change scene
					SceneManager.LoadSceneAsync(Config.AppSceneConfig.MapScene);
				});
			}
			else
			{
				Debug.Log("User cancelled login");
			}
		}

		private static IEnumerator DownloadAvatar()
		{
			yield return new WaitUntil(() => AppFirebaseStorage.instance.IsFinishedInit);
			AppFirebaseStorage.instance.DownloadUserAvatar(t =>
			{
				// Save file path to local 
				AppSaveData.Data.userInfo.userAvatarFilePath = t.Item2;
				AppSaveData.Save();

				// Update user avatar path to server
				var req = AppAPIManager.instance.CreateRequest<PutUserRequest>(UnityWebRequest.kHttpVerbPUT);
				var data = new PutUserRequest.PostObject
				{
					image = AppSaveData.Data.userInfo.userAvatarFilePath,
						fullName = AppRuntimeData.Data.userName,
						email = AppRuntimeData.Data.userEmail,
						phone = AppRuntimeData.Data.userPhone,
						location = new LocationObject()
						{
							type = "Point", coordinates = new float[2] { 0, 0, }
						},
				};
				req.SetPostData(data);
				req.Send(res => { }, (arg0, s) => { });
			}, true, true);
		}
	}
}