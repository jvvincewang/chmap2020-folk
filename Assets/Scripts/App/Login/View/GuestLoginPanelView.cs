using UnityEngine;
using UnityEngine.UI;
using System;
using App.NavigationBar;
using UniRx;

namespace App.Login.View {
    public class GuestLoginPanelView : MonoBehaviour {
        [SerializeField]
        private InputField _fullName;

        [SerializeField]
        private Button _loginBtn;

        [SerializeField]
        private Button _backBtn;

        private FormSlideAnimation _slideAnimation;

        public FormSlideAnimation SlideAnimation {
            get {
                if (_slideAnimation == null) {
                    _slideAnimation = GetComponent<FormSlideAnimation>();
                }

                return _slideAnimation;
            }
        }
        public IObservable<Unit> OnClickedLoginButton => _loginBtn.OnClickAsObservable();
        public IObservable<Unit> OnClickedBackButton => _backBtn.OnClickAsObservable();
        public string GetGuestFullName => _fullName.text;

        private void Start() {
            // can not login when name is null
            _fullName.OnValueChangedAsObservable().Subscribe(_ => {
                var isNullString = string.IsNullOrEmpty(_);
                _loginBtn.interactable = !isNullString;
            });
        }

        /// <summary>
        /// Freeze interactive ui component 
        /// </summary>
        /// <param name="isFreeze"></param>
        public void SetFreezeInteractive(bool isFreeze) {
            Debug.Log("Is Freeze: " + isFreeze);
            _loginBtn.interactable = !isFreeze;
            _backBtn.interactable = !isFreeze;
            _fullName.interactable = !isFreeze;
        }
    }
}