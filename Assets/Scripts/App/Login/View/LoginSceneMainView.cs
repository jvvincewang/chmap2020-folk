using System;
using UniRx;
using UnityEngine;
using UnityEngine.UI;

namespace App.Login.View {
    public class LoginSceneMainView : MonoBehaviour {
        [SerializeField]
        private Button _loginAsGuestBtn;

        [SerializeField]
        private Button _connectWithFacebookBtn;

        public IObservable<Unit> OnClickedLoginGuest => _loginAsGuestBtn.OnClickAsObservable();

        public IObservable<Unit> OnClickedConnectWithFacebook => _connectWithFacebookBtn.OnClickAsObservable();
    }
}