using System;

namespace App.AppData {
    [Serializable]
    public class RuntimeData {
        public string token;
        public string userName;
        public string userEmail;
        public string userPhone;
        public string userImage;
        public UserNotificationSetting notificationSetting = new UserNotificationSetting();

        [Serializable]
        public class UserNotificationSetting {
            public bool isActiveNotification;    
        }
    }
}