using System;

namespace App.AppData {
    [Serializable]
    public class InAppData {
        public UserMainInfo userInfo = new UserMainInfo();

        public void ClearUserMainInfo() {
            userInfo = new UserMainInfo();
        }
        
        [Serializable]
        public class UserMainInfo {
            /// <summary>
            /// This user Id is public and use to share with other users
            /// </summary>
            public string userId = "";

            /// <summary>
            /// This user id is use for login
            /// </summary>
            public string userInAppId = "";

            /// <summary>
            /// Facebook token (for the user linked with the facebook account)
            /// </summary>
            public string fbToken = "";

            /// <summary>
            /// User avatar file path
            /// </summary>
            public string userAvatarFilePath = "";
        }
    }
}