using System;
using System.Collections.Generic;
using API.Attributes;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbPOST)]
    [MethodType(UnityWebRequest.kHttpVerbGET)]
    [MethodType(UnityWebRequest.kHttpVerbPUT)]
    [MethodType(UnityWebRequest.kHttpVerbDELETE)]
    [EndpointPath("support")]
    [Serializable]
    public class SupportRequest : RequestBase<SupportRequest.PostObject, SupportRequest.ResponseObject> {
        [Serializable]
        public class PostObject : APIData.PostObject { }

        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public List<Support> data = new List<Support>();
        }

        [Serializable]
        public class Support {
            public string _id;
            public string name;
            public string phone;
            public string area;
            [JsonProperty("long")]
            public string longtitude;
            public string lat;
            public string vehicle;
            public string provide;
            public string type;
            public string status;
            public string createdAt;
        }

        protected override ResponseObject ParseResponseObject() {
            var res = _uwr.downloadHandler.text;
            Debug.Log(res);
            _responseData = JsonConvert.DeserializeObject<ResponseObject>(res);
            return _responseData;
        }
    }
}