using System;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbPOST)]
    [EndpointPath("soses/{0}/help")]
    [Serializable]
    public class SosHelpRequest : RequestBase {
        
    }
}