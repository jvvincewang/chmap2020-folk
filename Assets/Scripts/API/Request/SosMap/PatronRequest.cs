using API.APIData;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbGET)]
    [EndpointPath("patron")]
    public class PatronRequest : RequestBase { }
}