using System;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap
{
	[MethodType(UnityWebRequest.kHttpVerbGET)]
	[EndpointPath("soses/{0}")]
	[Serializable]
	public class SosesParamsRequest : RequestBase { }
}