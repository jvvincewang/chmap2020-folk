using System;

namespace API.Request.SosMap.Generated {
    public partial class PostUserLinkFacebookRequest {
        public override bool IsAuthorize => true;

        public partial class PostObject : PostUserLoginWithFacebookRequest.PostObject { }

        public partial class ResponseObject {
            public FacebookLinkedUserData data;
        }

        [Serializable]
        public class FacebookLinkedUserData : PostUserGuestRequest.GuestLoginData {
            
        }
    }
}