using System;
using API.APIData;

// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated {
    public partial class PostUserGuestRequest {
        public partial class PostObject {
            public string fullName;
        }
        
        public partial class ResponseObject {
            public GuestLoginData data;
            public string status;
            public string message;
        }
        
        [Serializable]
        public class GuestLoginData {
            public string _id;
            public string ID;
            public string fullName;
            public string phone;
            public string image;
            public string email;
            public string createAt;
            public Location location;
        }
    }
}