using System.Collections.Generic;
using API.APIData;

// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated {
    public partial class GetSosesRequest {
        public partial class PostObject { }

        public partial class ResponseObject {
            public string createdUserId;
            public string name;
            public Location location = new Location();
            public string provinceId;
            public string approach;
            public int status;
            public List<string> images = new List<string>();
            public string description;
            public int type;
            public string issue;
            public string deletedBy;
            public string _id;
            public string updatedAt;
            public string createdAt;
        }
    }
}