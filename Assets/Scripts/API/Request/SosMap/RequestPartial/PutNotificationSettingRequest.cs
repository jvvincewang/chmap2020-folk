using System;

namespace API.Request.SosMap.Generated
{
	public partial class PutNotificationSettingRequest
	{

		public override bool IsAuthorize => true;

		public partial class PostObject
		{
			public bool isActive;
			public string firebaseToken;
		}

		public partial class ResponseObject
		{
			public PutNotificationData data;
		}

		[Serializable]
		public class PutNotificationData : GetNotificationSettingRequest.NotificationData { }
	}
}