using API.APIData;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated
{

	public partial class PostSosHelpRequest
	{
		public override bool IsAuthorize => true;

		public partial class PostObject
		{

		}

		public partial class ResponseObject
		{
			[JsonProperty("data")]
			public Data Data { get; set; }

			[JsonProperty("status")]
			public string Status { get; set; }
		}

		// public partial class Data
		// {
		// 	[JsonProperty("sos")]
		// 	public SosObject Sos { get; set; }

		// 	[JsonProperty("channel")]
		// 	public ChannelObject Channel { get; set; }
		// }

	}
}