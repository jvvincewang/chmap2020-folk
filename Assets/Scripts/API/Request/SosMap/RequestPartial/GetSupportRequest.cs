using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace API.Request.SosMap.Generated {
    public partial class GetSupportRequest {
        public partial class PostObject : APIData.PostObject { }

        public partial class ResponseObject : APIData.ResponseObject {
            public List<SupportRequest.Support> data = new List<SupportRequest.Support>();
        }
        
        protected override ResponseObject ParseResponseObject() {
            var res = _uwr.downloadHandler.text;
            _responseData = JsonConvert.DeserializeObject<ResponseObject>(res);
            return _responseData;
        }
    }
}