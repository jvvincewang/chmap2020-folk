using System;
using API.APIData;
using Newtonsoft.Json;

namespace API.Request.SosMap.Generated {
    public partial class PostUserLoginRequest {
        public partial class PostObject {
            public string userId;
            public string ID;
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
            public string fbToken;
        }
        
        public partial class ResponseObject {
            public UserMeData data;
        }

        [Serializable]
        public class UserMeData {
            public string token;
            public CommonUser user;
            public string status;
            public string message;
        }
    }
}