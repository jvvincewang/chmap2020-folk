// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated
{
	public partial class PutUserRequest
	{
		public partial class PostObject
		{
			public string fullName;
			public string email;
			public string phone;
			public string image;
			public LocationObject location;
		}

		public partial class ResponseObject
		{

		}
	}
}