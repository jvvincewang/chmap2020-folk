using API.APIData;
using Newtonsoft.Json;
using static API.Request.SosMap.Generated.PostSosesRequest;

// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated
{
	public partial class PostSosesMetRequest
	{
		public override bool IsAuthorize => true;
		public partial class PostObject { }
		public partial class ResponseObject
		{
			[JsonProperty("data")]
			public Data Data { get; set; }

			[JsonProperty("status")]
			public string Status { get; set; }
		}

		public partial class Data
		{
			[JsonProperty("sos")]
			public Sos Sos { get; set; }

			[JsonProperty("channel")]
			public Channel Channel { get; set; }
		}

		public partial class Channel
		{
			[JsonProperty("_id")]
			public string ChannelId { get; set; }

			[JsonProperty("isMet")]
			public bool IsMet { get; set; }

			[JsonProperty("sosUserId")]
			public string SosUserId { get; set; }

			[JsonProperty("sosId")]
			public string SosId { get; set; }

			[JsonProperty("supporterUserId")]
			public string SupporterUserId { get; set; }

			[JsonProperty("ID")]
			public string Id { get; set; }

			[JsonProperty("updatedAt")]
			public string UpdatedAt { get; set; }

			[JsonProperty("createdAt")]
			public string CreatedAt { get; set; }

			[JsonProperty("__v")]
			public long V { get; set; }
		}

		public partial class Sos
		{
			[JsonProperty("_id")]
			public string Id { get; set; }

			[JsonProperty("approach")]
			public string Approach { get; set; }

			[JsonProperty("status")]
			public long Status { get; set; }

			[JsonProperty("images")]
			public object[] Images { get; set; }

			[JsonProperty("description")]
			public string Description { get; set; }

			[JsonProperty("type")]
			public long Type { get; set; }

			[JsonProperty("issue")]
			public string Issue { get; set; }

			[JsonProperty("name")]
			public string Name { get; set; }

			[JsonProperty("location")]
			public LocationObject Location { get; set; }

			[JsonProperty("createdUserId")]
			public string CreatedUserId { get; set; }

			[JsonProperty("updatedAt")]
			public string UpdatedAt { get; set; }

			[JsonProperty("createdAt")]
			public string CreatedAt { get; set; }

			[JsonProperty("__v")]
			public long V { get; set; }
		}
	}

}