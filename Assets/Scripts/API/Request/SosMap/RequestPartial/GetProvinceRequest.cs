// ReSharper disable once CheckNamespace

using System.Collections.Generic;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated {
    public partial class GetProvinceRequest {
        public partial class PostObject {
        }
        
        public partial class ResponseObject {
            public List<ProvinceRequest.Province> data = new List<ProvinceRequest.Province>();
        }
        protected override ResponseObject ParseResponseObject() {
            var res = _uwr.downloadHandler.text;
            _responseData = JsonConvert.DeserializeObject<ResponseObject>(res);
            return _responseData;
        }
    }
}