// ReSharper disable once CheckNamespace

using System.Collections.Generic;
using Newtonsoft.Json;

namespace API.Request.SosMap.Generated {
    public partial class GetDestinationRequest {
        public partial class ResponseObject {
            public List<DestinationRequest.Destination> data = new List<DestinationRequest.Destination>();
        }
        
        protected override ResponseObject ParseResponseObject() {
            var res = _uwr.downloadHandler.text;
            _responseData = JsonConvert.DeserializeObject<ResponseObject>(res);
            return _responseData;
        }
    }
}