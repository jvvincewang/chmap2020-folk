using System;
using System.Collections.Generic;
using API.APIData;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated
{
	[System.Serializable]
	public class LocationObject
	{
		public string type;
		// public Coordinates coordinates;
		public float[] coordinates;
	}

	[System.Serializable]
	public class Data
	{
		[JsonProperty("sos")]
		public SosObject Sos { get; set; }

		[JsonProperty("channel")]
		public ChannelObject Channel { get; set; }
	}

	[System.Serializable]
	public class ChannelObject
	{
		[JsonProperty("isMet")]
		public bool IsMet { get; set; }

		[JsonProperty("_id")]
		public string ChannelId { get; set; }

		[JsonProperty("sosUserId")]
		public string SosUserId { get; set; }

		[JsonProperty("sosId")]
		public string SosId { get; set; }

		[JsonProperty("supporterUserId")]
		public string SupporterUserId { get; set; }

		[JsonProperty("ID")]
		public string Id { get; set; }

		[JsonProperty("updatedAt")]
		public string UpdatedAt { get; set; }

		[JsonProperty("createdAt")]
		public string CreatedAt { get; set; }

		[JsonProperty("__v")]
		public long V { get; set; }

		// [JsonProperty("id")]
		// public string PurpleId { get; set; }
	}

	[System.Serializable]
	public class SosObject
	{
		[JsonProperty("approach")]
		public string Approach { get; set; }

		[JsonProperty("status")]
		public int Status { get; set; }

		// [JsonProperty("images")]
		// public object[] Images { get; set; }

		[JsonProperty("description")]
		public string Description { get; set; }

		[JsonProperty("phone")]
		public string Phone { get; set; }

		[JsonProperty("type")]
		public long Type { get; set; }

		[JsonProperty("issue")]
		public string Issue { get; set; }

		[JsonProperty("_id")]
		public string Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("location")]
		public LocationObject Location { get; set; }

		[JsonProperty("createdUserId")]
		public string CreatedUserId { get; set; }

		[JsonProperty("updatedAt")]
		public string UpdatedAt { get; set; }

		[JsonProperty("createdAt")]
		public string CreatedAt { get; set; }

		[JsonProperty("__v")]
		public long V { get; set; }

		[JsonProperty("id")]
		public string SosId { get; set; }
	}

	public partial class PostSosesRequest
	{
		public override bool IsAuthorize => true;

		public partial class PostObject
		{
			public string name;
			//public string provinceId;
			public string approach;
			//public List<string> images = new List<string>();
			public string description;
			public string phone;
			public int type;
			public string issue;
			public LocationObject location = new LocationObject();
		}

		public partial class ResponseObject
		{
			[JsonProperty("data")]
			public Data Data { get; set; }

			[JsonProperty("status")]
			public string Status { get; set; }
		}

		// public partial class Location
		// {
		// 	[JsonProperty("coordinates")]
		// 	public double[] Coordinates { get; set; }

		// 	[JsonProperty("_id")]
		// 	public string Id { get; set; }

		// 	[JsonProperty("type")]
		// 	public string Type { get; set; }
		// }
	}
}