﻿using System;
using API.APIData;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated {
    public partial class PostCreateUserRequest {
        public partial class PostObject {
            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
            public string phone;

            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
            public string fullName;

            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
            public string email;

            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
            public string image;

            [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
            public Location location;
        }

        public partial class ResponseObject {
            public CreateUserData data;
        }

        [Serializable]
        public class CreateUserData {
            public CommonUser user;
        }
    }
}