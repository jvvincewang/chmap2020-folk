using System;
using API.APIData;
using Newtonsoft.Json;

// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated {
    public partial class GetUserMeRequest {
        public override bool IsAuthorize => true;

        public partial class PostObject { }

        public partial class ResponseObject {
            public UserMeData data;
        }

        [Serializable]
        public class UserMeData {
            public CommonUser user;
            public string status;
        }
    }
}