using System;

namespace API.Request.SosMap.Generated
{
	public partial class PostDestinationHelpRequest
	{
		public override bool IsAuthorize => true;

		public partial class PostObject
		{
			public string destinationId;
		}

		public partial class ResponseObject
		{
			public ChannelData data;
			public string status;
			public string message;
		}

		[Serializable]
		public class ChannelData
		{
			public Channel channel;
		}

		[Serializable]
		public class Channel
		{
			public string _id;
			public string isMet;
			public string destinationId;
			public string userId;
			public string supporterId;
			public string ID;
			public string createdAt;
			public string updatedAt;
		}
	}
}