// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated
{
	public partial class PutLocationRequest
	{
		public override bool IsAuthorize => true;
		public partial class PostObject
		{
			public LocationObject location;
		}

		public partial class ResponseObject
		{

		}
	}
}