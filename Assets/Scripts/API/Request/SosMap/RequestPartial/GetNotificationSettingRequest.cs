using System;

// ReSharper disable once CheckNamespace
namespace API.Request.SosMap.Generated {
    public partial class GetNotificationSettingRequest {
        public override bool IsAuthorize => true;

        public partial class ResponseObject {
            public NotificationData data;
            public string status;
            public string message;
        }
        
        [Serializable]
        public class NotificationData {
            public string _id;
            public bool isActive;
        }
        
    }
}