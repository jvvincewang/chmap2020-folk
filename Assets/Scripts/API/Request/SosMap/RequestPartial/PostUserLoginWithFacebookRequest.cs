using System;
using API.APIData;
using Newtonsoft.Json;

namespace API.Request.SosMap.Generated {
    public partial class PostUserLoginWithFacebookRequest {
        public partial class PostObject {
            public string fbToken;
        }
        
        public partial class ResponseObject {
            public UserLoginWithFbData data;
        }
        
        [Serializable]
        public class UserLoginWithFbData {
            public string token;
            public CommonUser user;
            public string status;
        }
    }
}