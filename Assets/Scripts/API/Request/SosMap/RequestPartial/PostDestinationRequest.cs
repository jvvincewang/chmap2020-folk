﻿using System;

namespace API.Request.SosMap.Generated
{
	public partial class PostDestinationRequest
	{
		public override bool IsAuthorize => true;

		public partial class PostObject
		{
			public string guide { get; set; }
			public string approach { get; set; }
			public string status { get; set; }
			public string[] images = new string[] { };
			public string description { get; set; }
			public int level = 1;
			public string issue { get; set; }
			public bool verify = false;
			public string name { get; set; }
			public string lat { get; set; }
			public string @long { get; set; }
			public string provinceId { get; set; }
			public string secretCode { get; set; }
		}

		public partial class ResponseObject
		{
			public DestinationCreateData data;
			public string status;
			public string message;
		}

		[Serializable]
		public class DestinationCreateData
		{
			public string approach { get; set; }
			public string guide { get; set; }
			public string[] images { get; set; }
			public string description { get; set; }
			public string level { get; set; }
			public string issue { get; set; }
			public string verify { get; set; }
			public string _id { get; set; }
			public string name { get; set; }
			public string lat { get; set; }
			public string @long { get; set; }
			public string provinceId { get; set; }
			public string createdAt { get; set; }
			public string updatedAt { get; set; }
			public string status { get; set; }
			public string userId { get; set; }
		}
	}
}