using API.APIData;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbPOST)]
    [EndpointPath("users/linkFacebook")]
    public class UserLinkFacebookRequest : RequestBase { }
}