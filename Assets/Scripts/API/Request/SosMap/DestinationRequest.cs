using System;
using System.Collections.Generic;
using API.Attributes;
using Newtonsoft.Json;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbPOST)]
    [MethodType(UnityWebRequest.kHttpVerbGET)]
    [MethodType(UnityWebRequest.kHttpVerbPUT)]
    [MethodType(UnityWebRequest.kHttpVerbDELETE)]
    [EndpointPath("destination")]
    [Serializable]
    public class DestinationRequest : RequestBase<DestinationRequest.PostObject, DestinationRequest.ResponseObject> {
        [Serializable]
        public class PostObject : APIData.PostObject { }

        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public List<Destination> data = new List<Destination>();
        }
        
        [Serializable]
        public class Destination {
            public string _id;
            public string guide;
            public string approach;
            public string status;
            public string description;
            public int level;
            public string issue;
            public string view;
            public string name;
            public string lat;
            [JsonProperty("long")]
            public string longtitude;
            public Province provinceId;
            public string createdAt;
            public string updatedAt;
        }
        
        [Serializable]
        public class Province {
            public string _id;
            public string name;
            public string code;
            public string lat;
            [JsonProperty("long")]
            public string longtitude;
            public string createdAt;
            public string updatedAt;
        }

        protected override ResponseObject ParseResponseObject() {
            var res = _uwr.downloadHandler.text;
            _responseData = JsonConvert.DeserializeObject<ResponseObject>(res);
            return _responseData;
        }
    }
}