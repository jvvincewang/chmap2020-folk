using System;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbPOST)]
    [EndpointPath("destination/met")]
    [Serializable]
    public class DestinationMetRequest : RequestBase { }
}