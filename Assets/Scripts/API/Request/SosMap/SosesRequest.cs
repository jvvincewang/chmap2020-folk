using System;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap
{
	[MethodType(UnityWebRequest.kHttpVerbPOST)]
	[EndpointPath("soses")]
	[Serializable]
	public class SosesRequest : RequestBase { }
}