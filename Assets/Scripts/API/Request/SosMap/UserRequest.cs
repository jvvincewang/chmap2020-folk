using API.APIData;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap
{
	[MethodType(UnityWebRequest.kHttpVerbPUT)]
	[EndpointPath("users")]
	public class UserRequest : RequestBase { }
}