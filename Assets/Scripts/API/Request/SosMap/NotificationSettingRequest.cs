using API.APIData;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbGET)]
    [MethodType(UnityWebRequest.kHttpVerbPUT)]
    [EndpointPath("notificationSettings")]
    public class NotificationSettingRequest : RequestBase { }
}