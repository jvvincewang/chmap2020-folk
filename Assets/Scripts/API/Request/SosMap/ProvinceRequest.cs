using System;
using System.Collections.Generic;
using API.Attributes;
using Newtonsoft.Json;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbGET)]
    [EndpointPath("province")]
    [Serializable]
    public class ProvinceRequest : RequestBase<ProvinceRequest.PostObject, ProvinceRequest.ResponseObject> {
        [Serializable]
        public class PostObject : APIData.PostObject { }

        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public List<Province> data = new List<Province>();
        }

        [Serializable]
        public class Province {
            public string _id;
            public string name;
            public string code;
            public string lat;

            [JsonProperty("long")]
            public string longtitude;

            public string createdAt;
            public string updatedAt;
            public string __v;
        }

        protected override ResponseObject ParseResponseObject() {
            var res = _uwr.downloadHandler.text;
            _responseData = JsonConvert.DeserializeObject<ResponseObject>(res);
            return _responseData;
        }
    }
}