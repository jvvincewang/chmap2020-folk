using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbGET)]
    [EndpointPath("users/me")]
    public class UserMeRequest : RequestBase { }
}