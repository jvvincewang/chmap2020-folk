/*
This is the auto generated class
DO NOT MODIFY THIS FILE, CREATE (IT NOT EXISTED) AND MODIFY THE PARTIAL FILE INSTEAD
*/
using System;
using API.Attributes;
using API.Interfaces;

namespace API.Request.SosMap.Generated {
    [EndpointPath("users/linkFacebook")]
    [Serializable]
    public partial class PostUserLinkFacebookRequest : RequestBase<PostUserLinkFacebookRequest.PostObject, PostUserLinkFacebookRequest.ResponseObject>, IPostApiRequest {
        [Serializable]         
        public partial class PostObject{ }
        
        [Serializable]         
        public partial class ResponseObject{ }
    }
}