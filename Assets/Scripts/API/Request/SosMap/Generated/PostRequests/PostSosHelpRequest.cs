/*
This is the auto generated class
DO NOT MODIFY THIS FILE, CREATE (IT NOT EXISTED) AND MODIFY THE PARTIAL FILE INSTEAD
*/
using System;
using API.Attributes;
using API.Interfaces;
using Newtonsoft.Json;

namespace API.Request.SosMap.Generated
{
	[EndpointPath("soses/{0}/help")]
	[Serializable]
	public partial class PostSosHelpRequest : RequestBase<PostSosHelpRequest.PostObject, PostSosHelpRequest.ResponseObject>, IPostApiRequest
	{

		[Serializable]
		public partial class PostObject { }

		[Serializable]
		public partial class ResponseObject { }

	}
}