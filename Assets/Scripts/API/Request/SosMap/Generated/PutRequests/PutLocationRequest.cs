/*
This is the auto generated class
DO NOT MODIFY THIS FILE, CREATE (IT NOT EXISTED) AND MODIFY THE PARTIAL FILE INSTEAD
*/
using System;
using API.Attributes;
using API.Interfaces;

namespace API.Request.SosMap.Generated
{
	[EndpointPath("users")]
	[Serializable]
	public partial class PutLocationRequest : RequestBase<PutLocationRequest.PostObject, PutLocationRequest.ResponseObject>, IPutApiRequest
	{
		[Serializable]
		public partial class PostObject { }

		[Serializable]
		public partial class ResponseObject { }
	}
}