using System;
using System.Collections.Generic;
using API.Attributes;
using Newtonsoft.Json;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbPOST)]
    [MethodType(UnityWebRequest.kHttpVerbGET)]
    [MethodType(UnityWebRequest.kHttpVerbPUT)]
    [MethodType(UnityWebRequest.kHttpVerbDELETE)]
    [EndpointPath("supportHistory")]
    [Serializable]
    public class SupportHistoryRequest : RequestBase {
        [Serializable]
        public class PostObject : APIData.PostObject { }

        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public List<SupportHistory> data = new List<SupportHistory>(); 
        }

        [Serializable]
        public class SupportHistory {
            public string _id;
            public string destinationId;
            public string supportId;
            public string description;
            public int status;
            public string createdAt;
        }
    }
}