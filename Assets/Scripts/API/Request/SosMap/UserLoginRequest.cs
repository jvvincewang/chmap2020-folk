using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbPOST)]
    [EndpointPath("users/login")]
    public class UserLoginRequest: RequestBase { }
}