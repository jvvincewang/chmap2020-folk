using System;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbPOST)]
    [EndpointPath("destination/help")]
    [Serializable]
    public class DestinationHelpRequest : RequestBase { }
}