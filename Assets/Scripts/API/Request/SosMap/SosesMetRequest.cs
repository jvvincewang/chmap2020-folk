using System;
using API.Attributes;
using UnityEngine.Networking;

namespace API.Request.SosMap {
    [MethodType(UnityWebRequest.kHttpVerbPOST)]
    [EndpointPath("soses/{0}/met")]
    [Serializable]
    public class SosesMetRequest : RequestBase { }
}