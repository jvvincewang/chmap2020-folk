using System;
using System.Collections.Generic;
using API.APIData.CommonEntities;

namespace API.Request.CuuHoMienTrung {
    [Serializable]
    public class HuyenRequest : RequestBase<HuyenRequest.PostObject, HuyenRequest.ResponseObject> {
        [Serializable]
        public class PostObject : APIData.PostObject { }
        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public int count;
            public string next;
            public string previous;
            public List<Huyen> results = new List<Huyen>();
        }
        
        [Serializable]
        public class Huyen : HuyenCommon{ }
    }
}