﻿using System;
using System.Collections.Generic;
using API.APIData.CommonEntities;

namespace API.Request.CuuHoMienTrung {
    [Serializable]
    public class CuuhoRequest : RequestBase<CuuhoRequest.PostObject, CuuhoRequest.ResponseObject> {
        [Serializable]
        public class PostObject : APIData.PostObject { }

        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public int count;
            public string next;
            public string previous;
            public List<CuuHo> results = new List<CuuHo>();
        }

        [Serializable]
        public class CuuHo : HodanCommon {
            public string tinh_display;
            public string huyen_display;
            public string xa_display;
            public string status_display;
            public string volunteer;
        }
    }
}