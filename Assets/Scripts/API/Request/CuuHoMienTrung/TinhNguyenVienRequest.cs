﻿using System;
using System.Collections.Generic;
using API.APIData.CommonEntities;

namespace API.Request.CuuHoMienTrung {
    [Serializable]
    public class TinhNguyenVienRequest : RequestBase<TinhNguyenVienRequest.PostObject, TinhNguyenVienRequest.ResponseObject> {
        [Serializable]
        public class PostObject : APIData.PostObject { }

        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public int count;
            public string next;
            public string previous;
            public List<TinhNguyenVien> results = new List<TinhNguyenVien>();
        }

        [Serializable]
        public class TinhNguyenVien : HodanCommon {
            public string tinh_display;
            public string huyen_display;
            public string xa_display;
            public string status_display;
            public string volunteer;
        }
    }
}