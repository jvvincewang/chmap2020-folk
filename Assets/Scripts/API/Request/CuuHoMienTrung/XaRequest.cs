using System;
using System.Collections.Generic;
using API.APIData.CommonEntities;

namespace API.Request.CuuHoMienTrung {
    [Serializable]
    public class XaRequest : RequestBase<XaRequest.PostObject, XaRequest.ResponseObject> {
        [Serializable]
        public class PostObject : APIData.PostObject { }
        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public int count;
            public string next;
            public string previous;
            public List<Xa> results = new List<Xa>();
        }
        
        [Serializable]
        public class Xa : XaCommon{ }
    }
}