﻿using System;
using System.Collections.Generic;
using API.APIData.CommonEntities;

namespace API.Request.CuuHoMienTrung {
    [Serializable]
    public class HoDanRequest : RequestBase<HoDanRequest.PostObject, HoDanRequest.ResponseObject> {
        [Serializable]
        public class PostObject : APIData.PostObject { }

        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public int count;
            public string next;
            public string previous;
            public List<HoDan> results = new List<HoDan>();
        }

        [Serializable]
        public class HoDan : HodanCommon {
            public string tinh_display;
            public string huyen_display;
            public string xa_display;
            public string status_display;
            public string volunteer;
        }
    }
}