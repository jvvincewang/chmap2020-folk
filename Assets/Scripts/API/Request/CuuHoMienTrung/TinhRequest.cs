using System;
using System.Collections.Generic;
using API.APIData.CommonEntities;

namespace API.Request.CuuHoMienTrung {
    [Serializable]
    public class TinhRequest : RequestBase<TinhRequest.PostObject, TinhRequest.ResponseObject> {
        [Serializable]
        public class PostObject : APIData.PostObject { }
        [Serializable]
        public class ResponseObject : APIData.ResponseObject {
            public int count;
            public string next;
            public string previous;
            public List<Tinh> results = new List<Tinh>();
        }
        
        [Serializable]
        public class Tinh : TinhCommon{ }
    }
}