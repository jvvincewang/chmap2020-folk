namespace API {
    public enum Environment {
        Development,
        Staging,
        Production
    }
}