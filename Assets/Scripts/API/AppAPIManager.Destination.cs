using App.Interface;
using API.Interfaces;
using API.Request.SosMap.Generated;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace API
{
	public partial class AppAPIManager : MonoBehaviourSingletonBase<AppAPIManager>, IAppApiManagerSendRequest, IOnAppLaunch, IOnAppPaused
	{
		public void PostDestination(PostDestinationRequest.PostObject data, UnityAction<PostDestinationRequest.ResponseObject> finishedCallback = null)
		{
			var req = CreateRequest<PostDestinationRequest>(UnityWebRequest.kHttpVerbPOST);

			req.SetPostData(data);
			req.Send(res =>
			{
				finishedCallback?.Invoke(res);
			}, (arg0, err) =>
			{
				Debug.LogError($"<color=red>Error Post Destination: {err}</color>");
			});
		}

		public void PostDestinationHelp(PostDestinationHelpRequest.PostObject data, UnityAction<PostDestinationHelpRequest.ResponseObject> finishedCallback = null)
		{
			var req = CreateRequest<PostDestinationHelpRequest>(UnityWebRequest.kHttpVerbPOST);

			req.SetPostData(data);
			req.Send(res =>
			{
				finishedCallback?.Invoke(res);
			}, (arg0, err) =>
			{
				Debug.LogError($"<color=red>Error Post Destination Help: {err}</color>");
			});
		}

		public void PostDestinationMet(PostDestinationMetRequest.PostObject data, UnityAction<PostDestinationMetRequest.ResponseObject> finishedCallback = null)
		{
			var req = CreateRequest<PostDestinationMetRequest>(UnityWebRequest.kHttpVerbPOST);

			req.SetPostData(data);
			req.Send(res =>
			{
				finishedCallback?.Invoke(res);
			}, (arg0, err) =>
			{
				Debug.LogError($"<color=red>Error Post Destination Met: {err}</color>");
			});
		}
	}
}