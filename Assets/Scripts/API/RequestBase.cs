﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using API.Interfaces;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace API
{
	[Serializable]
	public class RequestBase<TPost, TRes> : IApiRequest, ISendApi
	where TPost : class, new()
	where TRes : class, new()
	{
		public string Domain { get; private set; }
		public string Endpoint { get; private set; }
		public string Path { get; private set; }
		public int TimeOut { get; set; }
		public long ErrorCode { get; set; }
		public string ResourceId { get; set; }
		public virtual bool IsAuthorize => false;
		public bool IsInterrupted { get; private set; }

		protected UnityWebRequest _uwr;
		protected TPost _postData;
		protected TRes _responseData;
		protected Dictionary<string, string> _headers = new Dictionary<string, string>();
		protected UnityAction<TRes> _successCallback;
		protected UnityAction<IApiRequest, string> _errorCallback;

		public void SetHeaders(Dictionary<string, string> headers)
		{
			foreach (var kv in headers)
			{
				AddHeader(kv.Key, kv.Value);
			}
		}

		/// <summary>
		/// Parse the response json string data into the response object
		/// </summary>
		/// <returns>The response object</returns>
		protected virtual TRes ParseResponseObject()
		{
			var res = _uwr.downloadHandler.text;
			Debug.Log($"[Response]{Path} : {res}");
			_responseData = JsonConvert.DeserializeObject<TRes>(res);
			return _responseData;
		}

		public void AddHeader(string k, string v)
		{
			_uwr.SetRequestHeader(k, v);
		}

		public void InterruptRequest()
		{
			IsInterrupted = true;
		}

		public void SetPostData(TPost postData)
		{
			_postData = postData;
		}

		public IAppApiManagerSendRequest AppApiManagerSendRequest { get; set; }

		/// <summary>
		/// Initialize request
		/// </summary>
		public virtual void InitializeRequest(string domain, string endpoint, string path, int timeOut, string method)
		{
			Domain = domain;
			Endpoint = endpoint;
			Path = path;
			TimeOut = timeOut;

			_uwr = new UnityWebRequest(Path, method)
			{
				downloadHandler = new DownloadHandlerBuffer(),
					timeout = TimeOut,
			};

			foreach (var kv in _headers)
			{
				_uwr.SetRequestHeader(kv.Key, kv.Value);
			}
		}

		/// <summary>
		/// This request will be call after received web response
		/// </summary>
		protected virtual void OnReceivedResponse()
		{
			if (!string.IsNullOrEmpty(_uwr.error))
			{
				ErrorCode = _uwr.responseCode;
				if (_errorCallback != null)
				{
					_errorCallback.Invoke(this, _uwr.error);
				}

				return;
			}

			try
			{
				ParseResponseObject();
				_successCallback.Invoke(_responseData);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message);
			}
		}

		/// <summary>
		/// This method will be call before send request to server
		/// </summary>
		protected virtual void OnBeginSendRequest() { }

		/// <summary>
		/// Send API to the 
		/// </summary>
		/// <param name="success">success callback will return the response object data</param>
		/// <param name="error">error callback, will return the request object, error string </param>
		public void Send(UnityAction<TRes> success, UnityAction<IApiRequest, string> error)
		{
			// add post data
			var data = _postData;
			if (data != null)
			{
				var json = JsonConvert.SerializeObject(data);
				Debug.Log($"[PostData]{Path}: {json}");
				var rawData = Encoding.UTF8.GetBytes(json);
				_uwr.uploadHandler = new UploadHandlerRaw(rawData);
			}

			// Set up callback when call api
			_successCallback = success;
			_errorCallback = error;

			// delegate the send progress to AppAPIManager
			AppApiManagerSendRequest.SendApi(this);
		}

		/// <summary>
		/// This method only be call in the API manager, since we want to wrap all the apis and manage from 1 point
		/// </summary>
		IEnumerator ISendApi.ExeSendApi()
		{
			OnBeginSendRequest();
			var asyncProcess = _uwr.SendWebRequest();
			yield return new WaitUntil(() => asyncProcess.isDone || IsInterrupted);
			if (!IsInterrupted)
			{
				OnReceivedResponse();
			}
			// dispose uwr
			_uwr.Dispose();
			AppApiManagerSendRequest.RemoveApiRequest(this);
		}
	}

	public class RequestBase : RequestBase<object, object> { }
}