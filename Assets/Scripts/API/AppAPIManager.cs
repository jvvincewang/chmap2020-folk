using System.Collections.Generic;
using System.Linq;
using App;
using App.Interface;
using API.Interfaces;
using API.Request.SosMap.Generated;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace API
{
	// ReSharper disable once InconsistentNaming
	public partial class AppAPIManager : MonoBehaviourSingletonBase<AppAPIManager>, IAppApiManagerSendRequest, IOnAppLaunch, IOnAppPaused
	{
		private EndpointConfiguration _nowConfig;
		private bool _initialized;
		private int _timeOut;
		private string _authKey;
		private IApiFactory _apiFactory;
		private List<IApiRequest> _reqList = new List<IApiRequest>();

		private void Start()
		{
			AppManager.instance.AddOnAppLaunch(this);
			AppManager.instance.AddOnAppPaused(this);
		}

		public void Initialize()
		{
			if (_initialized)
			{
				return;
			}

			_timeOut = NetworkingConfig.DefaultTimeOut;
			SetUpApiFactory();
			_initialized = true;
		}

		public void SetAuth(string userAuth)
		{
			_authKey =$"Bearer {userAuth}";
			_apiFactory.AuthKey = _authKey;
		}

		private void SetUpApiFactory()
		{
			_apiFactory = new APIFactory(_nowConfig.Domain, _timeOut, _authKey);
		}

		/// <summary>
		/// Creat Api request
		/// </summary>
		/// <param name="method">method of Api (using UnityWebRequest.kHttpVerbGET,... )</param>
		/// <param name="resourceId">resource id to combine on api url (this only use for the delete, put request)</param>
		public T CreateRequest<T>(string method = UnityWebRequest.kHttpVerbGET, string resourceId = "") where T : IApiRequest, new()
		{
			Initialize();
			var req = _apiFactory.CreateRequest<T>();
			req.AppApiManagerSendRequest = this;
			_reqList.Add(req);
			return req;
		}

		/// <summary>
		/// Creat Api request
		/// </summary>
		/// <param name="method">method of Api (using UnityWebRequest.kHttpVerbGET,... )</param>
		/// <param name="param">the param use for string format</param>
		public T CreateRequest<T>(string method = UnityWebRequest.kHttpVerbGET, params object[] param) where T : IApiRequest, new()
		{
			Initialize();
			var req = _apiFactory.CreateRequestWithParam<T>(param);
			req.AppApiManagerSendRequest = this;
			_reqList.Add(req);
			return req;
		}

		public void SetDefaultEndpointConfig(string path)
		{
			_nowConfig = Resources.Load<EndpointConfiguration>(path);
		}

		public void SendApi(IApiRequest request)
		{
			Debug.Log($"[API Manager] Send API: {request.Path}");
			var sendApi = (ISendApi) request;
			StartCoroutine(sendApi.ExeSendApi());
		}

		public void RemoveApiRequest(IApiRequest request)
		{
			if (_reqList.Contains(request))
			{
				_reqList.Remove(request);
			}
		}

		public void ClearAllRequest()
		{
			foreach (var req in _reqList)
			{
				req.InterruptRequest();
			}
			_reqList.Clear();
		}

		public void OnAppLaunch()
		{
			SetDefaultEndpointConfig(NetworkingConfig.DefaultEndpointResourcePath);
			Initialize();
			Debug.Log("<color=lime>[API Manager]Finished init api class</color>");
		}

		public void OnAppPaused(bool isPaused) { }
	}
}