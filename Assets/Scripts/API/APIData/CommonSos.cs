using System;
using System.Collections.Generic;

namespace API.APIData {
    [Serializable]
    public class CommonSos {
        public string name;
        public string provinceId;
        public string approach;
        public List<string> images = new List<string>();
        public string description;
        public int type;
        public string issue;
        public Location location = new Location();
        public string deletedBy;
        public string _id;
        public string updatedAt;
        public string createdAt;
    }
}