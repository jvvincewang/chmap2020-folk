﻿using System;
using Newtonsoft.Json;

namespace API.APIData {
    [Serializable]
    public class CommonUser {
        public int type;

        public int status;

        public string _id;

        public string phone;

        public string fullName;

        public string email;

        public string image;

        public Location location;

        public string ID;

        public string updatedAt;

        public string createdAt;

        public int __v;

        [JsonProperty("id")]
        public string secretId;
    }

    public class CommonUserExtendInfo:CommonUser
    {
        public string Note;
    }
}