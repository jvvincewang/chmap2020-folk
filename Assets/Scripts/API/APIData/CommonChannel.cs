using System;

namespace API.APIData {
    [Serializable]
    public class CommonChannel {
        public string ID;
        public string sosUserId;
        public string sosId;
        public string supporterUserId;
        public bool isMet;
        public string _id;
        public string updatedAt;
        public string createdAt;
    }
}