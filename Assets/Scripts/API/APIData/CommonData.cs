﻿using System;
using System.Collections.Generic;

namespace API.APIData {
    [Serializable]
    public class LoginUserData {
        public string phone;

        public string fullName;

        public string email;
        public string image;
        public Location location;
    }

    [Serializable]
    public class Location {
        public string _id;
        public string type;
        public List<double> coordinates = new List<double> {0, 0};
    }
}