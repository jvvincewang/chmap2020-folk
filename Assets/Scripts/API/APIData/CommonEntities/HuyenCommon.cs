using System;

namespace API.APIData.CommonEntities {
    [Serializable]
    public class HuyenCommon {
        public int id;
        public string name;
        public int tinh;
    }
}