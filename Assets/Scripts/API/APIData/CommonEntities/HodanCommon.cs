using System;

namespace API.APIData.CommonEntities {
    [Serializable]
    public class HodanCommon {
        public int id;
        public string name;
        public int status;
        public string phone;
        public string note;
        public string location;
        public int tinh;
        public int huyen;
        public int xa;
        public int thon;
        public string geo_longtitude;
        public string geo_latitude;
        public string update_time_timestamp;
        public string update_time;
    }
}