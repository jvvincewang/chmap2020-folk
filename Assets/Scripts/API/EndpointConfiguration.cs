﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace API {
    [CreateAssetMenu(fileName = "XXXConfig", menuName = "Project/Endpoint Configuration", order = 0)]
    public class EndpointConfiguration : ScriptableObject {
        public string Domain;
        public Environment Environment;
        public string Token;
        [SerializeField]
        private List<EndpointConfig> _endpointConfigs = new List<EndpointConfig>();

        public string GetFullApiPath(string endpointKey) {
            var p = "";
            var endpoint = _endpointConfigs.Find(x => x.endpointKey == endpointKey);
            if (endpoint != null) {
                p = $"{Domain}{endpoint.endpoint}";
            } else {
                Debug.LogWarning($"Not found endpoint with this key: {endpointKey}");
            }

            return p;
        }

        public string GetEndpoint(string endpointKey) {
            var endpoint = _endpointConfigs.Find(x => x.endpointKey == endpointKey);
            endpoint = endpoint ?? new EndpointConfig();
            return endpoint.endpoint;
        }
    }

    [Serializable]
    public class EndpointConfig {
        [Tooltip("Endpoint key need to be lower case and no space")]
        public string endpointKey;
        public string endpoint;
    }
}