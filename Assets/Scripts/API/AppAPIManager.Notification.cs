using App;
using App.Interface;
using API.Interfaces;
using API.Request.SosMap.Generated;
using UnityEngine;
using UnityEngine.Events;

namespace API
{
	public partial class AppAPIManager : MonoBehaviourSingletonBase<AppAPIManager>, IAppApiManagerSendRequest, IOnAppLaunch, IOnAppPaused
	{

		public void FetchNotificationSetting(UnityAction finishedCallback)
		{
			var req = CreateRequest<GetNotificationSettingRequest>();
			req.Send(res =>
			{
				AppRuntimeData.Data.notificationSetting.isActiveNotification = res.data.isActive;
				finishedCallback?.Invoke();
			}, (arg0, s) =>
			{
				Debug.LogError($"<color=red>Error Get User Info: {s}</color>");
			});
		}

		public void PutNotificationSetting(UnityAction finishedCallback)
		{
			var req = CreateRequest<PutNotificationSettingRequest>();
			var data = new PutNotificationSettingRequest.PostObject
			{
				isActive = true, firebaseToken = AppRuntimeData.Data.token,
			};

			req.SetPostData(data);
			req.Send(res =>
			{
				AppRuntimeData.Data.notificationSetting.isActiveNotification = res.data.isActive;
				finishedCallback?.Invoke();
			}, (arg0, s) =>
			{
				Debug.LogError($"<color=red>Error Put Notification: {s}</color>");
			});
		}
	}
}