namespace API.Interfaces {
    public interface IAppApiManagerSendRequest {
        void SendApi(IApiRequest request);
        void RemoveApiRequest(IApiRequest request);
    }
}