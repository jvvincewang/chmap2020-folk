using System.Collections.Generic;

namespace API.Interfaces {
    public interface IApiRequest {
        string Domain { get; }
        string Endpoint { get; }
        string Path { get; }
        int TimeOut { get; set; }
        long ErrorCode { get; }
        string ResourceId { get; set;}
        bool IsAuthorize { get; }
        IAppApiManagerSendRequest AppApiManagerSendRequest { get; set; }
        bool IsInterrupted { get; }

        void InitializeRequest(string domain, string endpoint, string path, int timeOut, string method);
        void SetHeaders(Dictionary<string, string> headers);
        void AddHeader(string k, string v);
        void InterruptRequest();
    }
}