namespace API.Interfaces {
    public interface IApiFactory {
        string Domain { get; set; }
        int TimeOut { get; set; }
        string AuthKey { get; set; }
        T CreateRequest<T>() where T : IApiRequest, new();
        T CreateRequestWithParam<T>(params object[] param) where T : IApiRequest, new();
    }
}