using System;
using System.IO;
using System.Linq;
using API.Attributes;
using API.Interfaces;
using UnityEngine;
using UnityEngine.Networking;

namespace API
{
	[Serializable]
	// ReSharper disable once InconsistentNaming
	public class APIFactory : IApiFactory
	{
		public string Domain { get; set; }
		public int TimeOut { get; set; }
		public string AuthKey { get; set; }

		public APIFactory(string domain, int timeout, string authKey)
		{
			Domain = domain;
			TimeOut = timeout;
			AuthKey = authKey;
		}

		public APIFactory() { }

		/// <summary>
		/// Create basic api request
		/// </summary>
		/// <param name="method">Method for the Api</param>
		/// <typeparam name="T">type of IApiRequest</typeparam>
		public T CreateRequest<T>() where T : IApiRequest, new()
		{
			var req = new T();
			var endpointKey = GetEndpoint(req);
			var path = Path.Combine(Domain, endpointKey);
			var method = GetRequestMethod(typeof(T));
			req.InitializeRequest(Domain, endpointKey, path, TimeOut, method);
			CreateBasicHeaders(req);
			Debug.Log($"[APIManager][<color=cyan>{method}</color>] Create API: {req.Path}");
			return req;
		}

		/// <summary>
		/// Create request with the resource id inside Api url
		/// </summary>
		/// <typeparam name="T">type of IApiRequest</typeparam>
		public T CreateRequestWithParam<T>(params object[] param) where T : IApiRequest, new()
		{
			var req = new T();
			var endpointKey = GetEndpoint(req);
			var path = Path.Combine(Domain, endpointKey);
			path = string.Format(path, param);
			var method = GetRequestMethod(typeof(T));
			req.InitializeRequest(Domain, endpointKey, path, TimeOut, method);
			CreateBasicHeaders(req);
			return req;
		}

		private string GetRequestMethod(Type requestType)
		{
			if (requestType.GetInterfaces().Contains(typeof(IDeleteApiRequest)))
			{
				return UnityWebRequest.kHttpVerbDELETE;
			}

			if (requestType.GetInterfaces().Contains(typeof(IPutApiRequest)))
			{
				return UnityWebRequest.kHttpVerbPUT;
			}

			if (requestType.GetInterfaces().Contains(typeof(IPostApiRequest)))
			{
				return UnityWebRequest.kHttpVerbPOST;
			}

			return UnityWebRequest.kHttpVerbGET;
		}

		/// <summary>
		/// Get api endpoint path
		/// </summary>
		/// <param name="requestType">type of the api request</param>
		/// <returns></returns>
		private string GetEndpoint(IApiRequest requestType)
		{
			var t = requestType.GetType();
			var att = t.GetCustomAttributes(typeof(EndpointPathAttribute), true);
			if (att.Length == 0)
			{
				Debug.LogWarning($"[APIFactory] end point of this api was null: {t}");
				return string.Empty;
			}

			var pathAtt = (EndpointPathAttribute) att.First();
			var p = pathAtt.EndpointUrl;
			return p;
		}

		/// <summary>
		/// Create the basic headers of the api request
		/// </summary>
		/// <param name="req">the api request</param>
		private void CreateBasicHeaders(IApiRequest req)
		{
			req.AddHeader("Content-Type", "application/json; charset=UTF-8");
			// req.AddHeader("Authorization", $"{AuthKey}");
			req.AddHeader("secret", $"{NetworkingConfig.SecretCode}");
			if (req.IsAuthorize)
			{
				req.AddHeader("authorization", $"{AuthKey}");
			}
		}
	}
}