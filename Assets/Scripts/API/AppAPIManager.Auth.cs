using System;
using App;
using App.Interface;
using API.Interfaces;
using API.Request.SosMap.Generated;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

namespace API
{
	public partial class AppAPIManager : MonoBehaviourSingletonBase<AppAPIManager>, IAppApiManagerSendRequest,
		IOnAppLaunch, IOnAppPaused
		{
			/// <summary>
			/// Login user as guest user. If login failed, will redirect user to the login or sign up page
			/// </summary>
			/// <param name="loginFinishedCallback">login finished success callback</param>
			public void Login(UnityAction loginFinishedCallback, UnityAction loginFailCallback)
			{
				Debug.Log("userInAppID:" + AppSaveData.Data.userInfo.userInAppId);
				Debug.Log("userID" + AppSaveData.Data.userInfo.userId);
				var req = CreateRequest<PostUserLoginRequest>(UnityWebRequest.kHttpVerbPOST);
				var data = new PostUserLoginRequest.PostObject
				{
					userId = AppSaveData.Data.userInfo.userInAppId,
						ID = AppSaveData.Data.userInfo.userId
				};
				req.SetPostData(data);
				req.Send(res =>
				{
					AppRuntimeData.Data.token = res.data.token;
					// Set user authentication token
					SetAuth(AppRuntimeData.Data.token);
					// Fetch user info
					FetchUserInfo(loginFinishedCallback);
				}, (arg0, s) =>
				{
					//If error then directly send user to login scene
					Debug.LogError($"<color=red>Error Login: {s}</color>");
					loginFailCallback?.Invoke();
					//SceneManager.LoadSceneAsync(Config.AppSceneConfig.LoginScene);
				});
			}

			/// <summary>
			/// Sign up a user account as guest user. 
			/// </summary>
			/// <param name="guestName">name of user</param>
			/// <param name="finishedCallback">finished login success callback</param>
			public void SignUpAsGuest(string guestName, UnityAction finishedCallback)
			{
				// var req = CreateRequest<PostUserGuestRequest>(UnityWebRequest.kHttpVerbPOST);
				// var data = new PostUserGuestRequest.PostObject
				// {
				// 	fullName = guestName
				// };

				var req = CreateRequest<PostCreateUserRequest>(UnityWebRequest.kHttpVerbPOST);
				var data = new PostCreateUserRequest.PostObject
				{
					fullName = guestName
				};

				req.SetPostData(data);
				req.Send(res =>
				{
					AppSaveData.Data.userInfo.userId = res.data.user.ID;
					AppSaveData.Data.userInfo.userInAppId = res.data.user._id;
					AppSaveData.Save();
					// fetch user info to runtime info
					Login(finishedCallback, null);
				}, (arg0, s) => { Debug.LogError($"<color=red>Error Signup guest user: {s}</color>"); });
			}

			internal void PostSos(PostSosesRequest.PostObject data, Action<PostSosesRequest.ResponseObject> finishedCallback, Action errorCallback = null)
			{
				var req = CreateRequest<PostSosesRequest>();
				req.SetPostData(data);
				req.Send(res =>
				{
					finishedCallback?.Invoke(res);
				}, (arg0, s) =>
				{
					errorCallback?.Invoke();
					Debug.LogError($"<color=red>Error PostSos: {s}</color>");
				});
			}

			/// <summary>
			/// Sign up or login as a facebook account. If login failed, will redirect user to the login or sign up page
			/// </summary>
			/// <param name="fbToken">token get from facebook</param>
			/// <param name="finishedCallback">finished login success callback</param>
			public void SignUpOrLoginAsFacebookUser(string fbToken, UnityAction finishedCallback)
			{
				var req = CreateRequest<PostUserLoginWithFacebookRequest>();
				var data = new PostUserLoginWithFacebookRequest.PostObject { fbToken = fbToken };
				req.SetPostData(data);
				req.Send(res =>
				{
					AppSaveData.Data.userInfo.userId = res.data.user.ID;
					AppSaveData.Data.userInfo.userInAppId = res.data.user._id;
					AppRuntimeData.Data.token = res.data.token;
					AppSaveData.Data.userInfo.fbToken = fbToken;
					AppSaveData.Save();

					// Set user authentication token
					SetAuth(AppRuntimeData.Data.token);

					// Fetch user info
					FetchUserInfo(finishedCallback);
				}, (arg0, s) =>
				{
					//If error then directly send user to login scene
					Debug.LogError($"<color=red>Error Facebook Login: {s}</color>");
					SceneManager.LoadSceneAsync(Config.AppSceneConfig.LoginScene);
				});
			}

			public void LinkGuestUserWithFacebook(string fbToken, UnityAction finishedCallback)
			{
				var req = CreateRequest<PostUserLinkFacebookRequest>();
				var data = new PostUserLinkFacebookRequest.PostObject { fbToken = fbToken };
				req.SetPostData(data);
				req.Send(res =>
				{
					AppSaveData.Data.userInfo.userId = res.data.ID;
					AppSaveData.Data.userInfo.userInAppId = res.data._id;
					AppSaveData.Data.userInfo.fbToken = fbToken;
					AppSaveData.Save();

					// Set user authentication token
					SetAuth(AppRuntimeData.Data.token);

					// Fetch user info
					FetchUserInfo(finishedCallback);
				}, (arg0, s) =>
				{
					//If error then directly send user to login scene
					Debug.LogError($"<color=red>Error Facebook Login: {s}</color>");
					SceneManager.LoadSceneAsync(Config.AppSceneConfig.LoginScene);
				});
			}

			/// <summary>
			/// Fetch the user information
			/// </summary>
			/// <param name="finishedCallback">Callback when user finish fetch info</param>
			public void FetchUserInfo(UnityAction finishedCallback)
			{
				var req = CreateRequest<GetUserMeRequest>();
				req.Send(res =>
				{
					AppRuntimeData.Data.userName = res.data.user.fullName;
					AppRuntimeData.Data.userPhone = res.data.user.phone;
					AppRuntimeData.Data.userImage = res.data.user.image;
					finishedCallback?.Invoke();
				}, (arg0, s) => { Debug.LogError($"<color=red>Error Get User Info: {s}</color>"); });
			}

			public void PutLocationInfo(PutLocationRequest.PostObject postData, UnityAction finishedCallback)
			{
				var req = CreateRequest<PutLocationRequest>(UnityWebRequest.kHttpVerbPUT);
				req.SetPostData(postData);
				req.Send(res =>
				{
					finishedCallback?.Invoke();
				}, (arg0, s) => { Debug.LogError($"<color=red>Error Put Location Info: {s}</color>"); });
			}
		}
}