using System;

namespace API.Attributes {
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class MethodTypeAttribute : Attribute {
        private string _methodType;

        public string MethodType => _methodType;

        public MethodTypeAttribute(string methodType) {
            _methodType = methodType;
        }
    }
}