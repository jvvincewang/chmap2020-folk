using System;

namespace API.Attributes {
    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class EndpointPathAttribute : Attribute {
        public string EndpointUrl { get; }

        public EndpointPathAttribute(string endpointUrl) {
            EndpointUrl = endpointUrl;
        }
    }
}