using System.Collections.Generic;

namespace API {
    public class ResponseContext {
        private readonly byte[] _bytes;
        private long _statusCode;
        private Dictionary<string, string> _resHeaders;

        public byte[] Bytes => _bytes;

        public long StatusCode => _statusCode;

        public Dictionary<string, string> ResHeaders => _resHeaders;

        public ResponseContext(byte[] bytes, long statusCode, Dictionary<string, string> resHeaders) {
            _bytes = bytes;
            _statusCode = statusCode;
            _resHeaders = resHeaders;
        }

        public virtual T GetResponseAs<T>() {
            return default;
        }
    }
}