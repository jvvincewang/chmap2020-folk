using System;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace API {
    public interface IAsyncDecorator {
        UniTask<ResponseContext> SendAsync(RequestContext context,
            CancellationToken cancellationToken, Func<RequestContext, CancellationToken, UniTask<ResponseContext>> _);
    }
}