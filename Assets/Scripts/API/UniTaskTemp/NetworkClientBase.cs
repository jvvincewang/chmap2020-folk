using System;
using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace API {
    public class NetworkClientBase : IAsyncDecorator {
        readonly Func<RequestContext, CancellationToken, UniTask<ResponseContext>> next;
        readonly IAsyncDecorator[] decorators;
        readonly TimeSpan timeout;
        readonly IProgress<float> progress;
        readonly string basePath;

        public NetworkClientBase(string basePath, TimeSpan timeout, params IAsyncDecorator[] decorators)
            : this(basePath, timeout, null, decorators) { }

        public NetworkClientBase(string basePath, TimeSpan timeout, IProgress<float> progress,
            params IAsyncDecorator[] decorators) {
            this.next = InvokeRecursive; // setup delegate
            this.basePath = basePath;
            this.timeout = timeout;
            this.progress = progress;
            this.decorators = new IAsyncDecorator[decorators.Length + 1];
            Array.Copy(decorators, this.decorators, decorators.Length);
            this.decorators[this.decorators.Length - 1] = this;
        }

        public async UniTask<R> PostAsync<T, R>(string path, T value, CancellationToken cancellationToken = default) {
            var request = new RequestContext(basePath, path, value, timeout, decorators);
            var response = await InvokeRecursive(request, cancellationToken);
            return response.GetResponseAs<R>();
        }

        public async UniTask<T> GetAsync<T>(string path, T value, CancellationToken cancellationToken = default) {
            var request = new RequestContext(basePath, path, value, timeout, decorators);
            var response = await InvokeRecursive(request, cancellationToken);
            return response.GetResponseAs<T>();
        }

        UniTask<ResponseContext> InvokeRecursive(RequestContext context, CancellationToken cancellationToken) {
            return context.GetNextDecorator.SendAsync(context, cancellationToken, next); // magical recursive:)
        }

        public async UniTask<ResponseContext> SendAsync(RequestContext context,
            CancellationToken cancellationToken, Func<RequestContext, CancellationToken, UniTask<ResponseContext>> _) {
            // This is sample, use only POST
            // If you want to maximize performance, customize uploadHandler, downloadHandler

            // send JSON in body as parameter
            var data = JsonUtility.ToJson(context.PostData);
            var formData = new Dictionary<string, string> {{"body", data}};

            var path = $"{basePath}{context.Endpoint}";
            Debug.Log($"Path: {path}");
            using (var req = new UnityWebRequest(path)) {
                req.method = "GET";
                req.downloadHandler = new DownloadHandlerBuffer();
                var header = context.GetHeaders;
                if (header != null) {
                    foreach (var item in header) {
                        req.SetRequestHeader(item.Key, item.Value);
                    }
                }

                // You can process Timeout by CancellationTokenSource.CancelAfterSlim(extension of UniTask)
                var linkToken = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
                linkToken.CancelAfterSlim(timeout);
                try {
                    // await req.SendWebRequest().ToUniTask(progress: progress, cancellationToken: linkToken.Token);
                    await req.SendWebRequest();
                }
                catch (OperationCanceledException) {
                    if (!cancellationToken.IsCancellationRequested) {
                        throw new TimeoutException();
                    }
                }
                finally {
                    // stop CancelAfterSlim's loop
                    if (!linkToken.IsCancellationRequested) {
                        linkToken.Cancel();
                    }
                }

                Debug.Log($"{req.downloadHandler.text}");
                // Get response items first because UnityWebRequest is disposed in end of this method.
                // If you want to avoid allocating repsonse/header if no needed, think another way,
                return new ResponseContext(req.downloadHandler.data, req.responseCode, req.GetResponseHeaders());
            }
        }
    }
}