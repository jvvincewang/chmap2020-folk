using System;
using System.Collections.Generic;
using API.APIData;

namespace API {
    public class RequestContext {
        private int decoratorIdx = -1;
        private readonly IAsyncDecorator[] _decorators;
        private Dictionary<string, string> _headers = new Dictionary<string, string>();
        public string BasePath;
        public string Endpoint;
        public TimeSpan TimeOut;
        public object PostData;

        public RequestContext(string basePath, string endpoint, object postData, TimeSpan timeOut,
            IAsyncDecorator[] decorators) {
            _decorators = decorators;
            BasePath = basePath;
            Endpoint = endpoint;
            TimeOut = timeOut;
            PostData = postData;
        }

        public IAsyncDecorator GetNextDecorator => _decorators[++decoratorIdx];
        public Dictionary<string, string> GetHeaders => _headers;
    }
}