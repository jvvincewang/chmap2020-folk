﻿namespace Common {
    public class SingletonBase<T> where T : class, new() {
        private static T _instance;

        //インスタンスの取得
        public static T instance {
            get {
                if (_instance == null) {
                    _instance = new T();
                }

                return _instance;
            }
        }

        protected SingletonBase() { }
    }
}