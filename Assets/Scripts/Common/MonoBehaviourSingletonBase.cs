﻿using UnityEngine;

public class MonoBehaviourSingletonBase<T> : MonoBehaviour where T : MonoBehaviour
{
	private static object syncObj = new object();

	private static T _instance;

	public static T instance
	{
		get
		{
			if (_instance != null)
			{
				return _instance;
			}
			var t = typeof(T);
			_instance = (T) FindObjectOfType(t);
			if (_instance != null)
			{
				return _instance;
			}

			// Lock to avoid the same instance object being create
			lock(syncObj)
			{
				var obj = new GameObject(t.Name);
				_instance = obj.AddComponent<T>();
			}

			return _instance;
		}
	}

	public static bool exists => _instance != null;

	/// <summary>
	/// インスタンスの削除
	/// </summary>
	public static void DestroyInstance()
	{
		if (_instance == null)
		{
			return;
		}

		Destroy(_instance.gameObject);
		_instance = null;
	}

	protected virtual void AwakeSingleton() { }

	void Awake()
	{
		// Check if attached on other game object
		if (this != instance)
		{
			Destroy(gameObject);
			Debug.LogWarning(
				$"{typeof(T)} The Singleton of this behaviour type has attached on other game object :[{instance.gameObject.name}]");
			return;
		}
		DontDestroyOnLoad(gameObject);

		AwakeSingleton();
	}
}