using UnityEngine;
using System.IO;
using App;

namespace Common {
    public static class StorageUtil {
        public static string UserLocalRoot {
            get {
                var appPath = Application.persistentDataPath;
#if UNITY_EDITOR
                appPath = Application.dataPath;
#endif
                return Path.Combine(appPath, Config.StorageConfig.LocalFileRoot);
            }
        }

        public static string GetLocalFilePathByFileName(string filename) {
            return Path.Combine(UserLocalRoot, filename);
        }

        public static bool IsExist(string fileName) {
            var path = Path.Combine(UserLocalRoot, fileName);
            return File.Exists(path);
        }

        public static void SaveFileToBytes(string fileName, byte[] data) {
            var path = GetLocalFilePathByFileName(fileName);
            Debug.Log($"Save file to path: {path}");
            var dir = Path.GetDirectoryName(path);
            if (!string.IsNullOrEmpty(dir)) {
                Directory.CreateDirectory(dir);
            }
            File.WriteAllBytes(path, data);
        }

        public static byte[] LoadFileToByte(string fileName) {
            byte[] result = null;
            var path = Path.Combine(UserLocalRoot, fileName);
            Debug.Log($"Log File from path: {fileName}");
            if (IsExist(fileName)) {
                result = File.ReadAllBytes(path);
            } else {
                Debug.Log($"Load local file error: File not found [{fileName}]");
            }
            return result;
        }
    }
}