using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using API.Attributes;
using API.Interfaces;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

namespace AppEditor.AppApi {
    public static class AppApiClassGenerator {
        private const string DestGeneratedFileDir = "Assets/Scripts/API/Request/SosMap/Generated";

        private const string TemplateFileDir =
            "Assets/Scripts/Editor/AppEditor/AppApi/Templates/SosMapRequestTemplate.txt";

        private const string WEB_METHOD_CLASS_NAME_KEY = "{WEB_METHOD_CLASS_NAME}";
        private const string BASE_CLASS_NAME_KEY = "{BASE_CLASS_NAME}";
        private const string ENDPOINT_URL_KEY = "{ENDPOINT_URL}";
        private const string REQUEST_METHOD_TYPE_INTERFACE = "{REQUEST_METHOD_TYPE_INTERFACE}";

        [MenuItem("Project/Generate And Update API Class")]
        public static void GenerateRequestClass() {
            Debug.Log("<color=lime>[INFO] Started Generate API Class</color>");
            var files = GetRequestMainFilePath();
            ClearGeneratedFile();
            foreach (var s in files) {
                // We not generate the RequestBase class because that class use as an abstract parent
                // We not generate the CuuHoMienTrung request too since we not use it anymore, but since we still ref to
                // it so we have to keep, after refactoring code will remove it out.
                if (s.Name.Contains("RequestBase") || 
                    !string.IsNullOrEmpty(s.FullName) && s.FullName.Contains("CuuHoMienTrung")) {
                    continue;
                }
                Generate(s);
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            Debug.Log("<color=lime>[INFO] Ended Generate API Class</color>");
        }

        private static Type[] GetRequestMainFilePath() {
            var type = typeof(IApiRequest);
            var types = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && !p.IsInterface)
                .ToArray();
            return types;
        }

        private static void Generate(Type t) {
            var getMethod = t.GetCustomAttributes(typeof(MethodTypeAttribute), true);
            var endpointAtt = t.GetCustomAttributes(typeof(EndpointPathAttribute), true);
            if (endpointAtt.Length == 0) {
                Debug.LogError($"This API dont have endpoint path, please define endpoint path before generate: {t}");
                return;
            }
            var templateTxt = File.ReadAllText(TemplateFileDir);
            foreach (var webMethod in getMethod) {
                var method = ((MethodTypeAttribute) webMethod).MethodType;
                var methodName = method.ToLower();
                methodName = methodName.First().ToString().ToUpper() + methodName.Substring(1);
                
                var endpointUrl = ((EndpointPathAttribute) endpointAtt.First()).EndpointUrl;
                var className = $"{methodName}{t.Name}";
                var methodTypeInterface = GetRequestMethodTypeInterface(method);
                var txt = templateTxt
                    .Replace(WEB_METHOD_CLASS_NAME_KEY, className)
                    .Replace(BASE_CLASS_NAME_KEY, t.Name)
                    .Replace(ENDPOINT_URL_KEY, endpointUrl)
                    .Replace(REQUEST_METHOD_TYPE_INTERFACE, methodTypeInterface);
                var p = GetDestinationFileDir(className, method);
                File.WriteAllText(p, txt);
                Debug.Log($"[API Generator] Created Method API: {className}");
            }
        }

        private static string GetRequestMethodTypeInterface(string methodType) {
            switch (methodType) {
                case UnityWebRequest.kHttpVerbPOST:
                    return "IPostApiRequest";
                case UnityWebRequest.kHttpVerbPUT:
                    return "IPutApiRequest";
                case UnityWebRequest.kHttpVerbGET:
                    return "IGetApiRequest";
                case UnityWebRequest.kHttpVerbDELETE:
                    return "IDeleteApiRequest";
            }
            return string.Empty;
        }

        private static string GetDestinationFileDir(string className, string methodType) {
            var path = DestGeneratedFileDir;
            switch (methodType) {
                case UnityWebRequest.kHttpVerbPOST:
                    path = Path.Combine(path, "PostRequests");
                    break;
                case UnityWebRequest.kHttpVerbPUT:
                    path = Path.Combine(path, "PutRequests");
                    break;
                case UnityWebRequest.kHttpVerbGET:
                    path = Path.Combine(path, "GetRequests");
                    break;
                case UnityWebRequest.kHttpVerbDELETE:
                    path = Path.Combine(path, "DeleteRequests");
                    break;
            }

            path = Path.Combine(path, $"{className}.cs");
            return path;
        }

        private static void ClearGeneratedFile() {
            var clearDir = new List<string> {
                Path.Combine(DestGeneratedFileDir, "PostRequests"),
                Path.Combine(DestGeneratedFileDir, "PutRequests"),
                Path.Combine(DestGeneratedFileDir, "GetRequests"),
                Path.Combine(DestGeneratedFileDir, "DeleteRequests")
            };
            foreach (var f in clearDir
                .Select(dir => new DirectoryInfo(dir))
                .Select(di => di.GetFiles())
                .SelectMany(files => files)) {
                f.Delete();
            }
        }
    }
}