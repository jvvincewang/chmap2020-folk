﻿namespace Game.Build
{
    public static class NameBinaryDefine
    {
        public const string ADMIN_SERVER = "iVoRi360-server";
        public const string GAME_ROOM_SERVER = "iVoRi360-roomserver";
        public const string OCULUS_APP = "iVoRi360";
        public const string BRIDGER = "iVoRi360-bridge";

        public static string AdminServerExe = $"{ADMIN_SERVER}.exe";
        public static string GameRoomServerExe = $"{GAME_ROOM_SERVER}.exe";
        public static string OculusAppApk = $"{OCULUS_APP}.apk";
        public static string BridgerExe = $"{BRIDGER}.exe";
    }
}