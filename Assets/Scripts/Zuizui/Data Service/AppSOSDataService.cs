﻿using System;
using System.Collections;
using System.Collections.Generic;
using API;
using API.Request.SosMap;
using OfflineMap;
using UnityEngine;
using static API.Request.SosMap.DestinationRequest;
using static API.Request.SosMap.ProvinceRequest;
using Province = API.Request.SosMap.ProvinceRequest.Province;
using static API.Request.SosMap.SupportRequest;

public class AppSosDataService : DataService
{
	public override string DefaultEndpointResourcePath() => "EndpointConfig/SosMapNetEndpointConfig";
	public static DestinationPoint DestinationPoint = new DestinationPoint();
	public static SupportPoint SupportPoint = new SupportPoint();
	public static ProvincePoint ProvincePoint = new ProvincePoint();
	public static event Action<List<Destination>> OnRequestDestinationCompleted;
	public static event Action<List<Support>> OnRequestSupportCompleted;
	public static event Action<List<Province>> OnRequestProvinceCompleted;

	public static event Action<List<Destination>> OnLoadCacheDestinationCompleted;
	public static event Action<List<Support>> OnLoadCacheSupportCompleted;

	public override void Init()
	{
		AppAPIManager.instance.SetDefaultEndpointConfig(DefaultEndpointResourcePath());
	}

	public override void LoadDataFromCache()
	{
		OnLoadCacheDestinationCompleted?.Invoke(MarkerCache.Destinations);
		OnLoadCacheSupportCompleted?.Invoke(MarkerCache.Supports);
	}

	public override void RequestDataFromServer(Action onCompleted = null, Action onFailed = null)
	{
		DestinationPoint.RequestDataFromServer(data =>
		{
			OnRequestDestinationCompleted?.Invoke(data);
			MarkerCache.SaveMarkersDestination(data);
		});

		SupportPoint.RequestDataFromServer(data =>
		{
			OnRequestSupportCompleted?.Invoke(data);
			MarkerCache.SaveMarkersSupport(data);

		});

		ProvincePoint.RequestDataFromServer(data =>
		{
			OnRequestProvinceCompleted?.Invoke(data);
		});
	}

	public static List<DestinationMarkerData> ParseDestinationMarkerData(List<Destination> data)
	{
		List<DestinationMarkerData> markers = new List<DestinationMarkerData>();

		for (int i = 0; i < data.Count; i++)
		{
			DestinationMarkerData marker = new DestinationMarkerData();
			marker.name = data[i].name;
			marker.longitude = data[i].longtitude;
			marker.latitude = data[i].lat;
			marker.status = data[i].status;
			marker.phone = data[i].guide;
			marker.issue = data[i].issue;
			marker.level = data[i].level.ToString();
			marker.approach = data[i].approach;
			marker.description = data[i].description;
			marker.province = data[i].provinceId?.name;
			marker.updatedAt = data[i].updatedAt;
			marker.createdAt = data[i].createdAt;

			markers.Add(marker);
		}

		return markers;
	}

	public static List<SupportMarkerData> ParseSupportToMarkerData(List<Support> data)
	{
		List<SupportMarkerData> markers = new List<SupportMarkerData>();

		for (int i = 0; i < data.Count; i++)
		{
			SupportMarkerData marker = new SupportMarkerData();
			marker.name = data[i].name;
			marker.longitude = data[i].longtitude;
			marker.latitude = data[i].lat;
			marker.phone = data[i].phone;
			marker.province = data[i].area;
			marker.provide = data[i].provide;
			marker.vehicle = data[i].vehicle;

			marker.type = data[i].type;
			marker.status = data[i].status;
			marker.updatedAt = data[i].createdAt;

			markers.Add(marker);
		}

		return markers;
	}

	public static List<ProvinceMarkerData> ParseProvinceToMarkerData(List<Province> data)
	{
		List<ProvinceMarkerData> markers = new List<ProvinceMarkerData>();

		for (int i = 0; i < data.Count; i++)
		{
			ProvinceMarkerData marker = new ProvinceMarkerData();
			marker.name = data[i].name;
			marker.longitude = data[i].longtitude;
			marker.latitude = data[i].lat;
			marker.updatedAt = data[i].updatedAt;

			markers.Add(marker);
		}

		return markers;
	}

}