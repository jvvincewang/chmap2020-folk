﻿using System;
using System.Collections.Generic;
using API;
using API.Request.SosMap.Generated;
using static API.Request.SosMap.DestinationRequest;
using static API.Request.SosMap.SupportRequest;
using static API.Request.SosMap.ProvinceRequest;
using API.Request.SosMap;
using UnityEngine.Networking;

public class DestinationPoint : DataServicePoint<Destination>
{
	public override void RequestDataFromServer(Action<List<Destination>> onSuccess, string error = default)
	{
		var request = AppAPIManager.instance.CreateRequest<GetDestinationRequest>();
		request.Send(_ =>
		{
			Items = _.data;
			onSuccess?.Invoke(_.data);
		}, (req, errStr) =>
		{
			error = errStr;
		});
	}

	public void PostDataToServer(PostDestinationRequest.PostObject data)
	{
		var req = AppAPIManager.instance.CreateRequest<API.Request.SosMap.Generated.PostDestinationRequest>(UnityWebRequest.kHttpVerbPOST);

		req.SetPostData(data);
		req.Send(res =>
		{
			// AppSaveData.Data. = res.data.ID;
			// AppSaveData.Data.userInAppId = res.data._id;
			// AppSaveData.Save();

			// fetch user info to runtime info
			// Login(finishedCallback);
		}, (arg0, s) =>
		{
			// Debug.LogError($"<color=red>Error Signup guest user: {s}</color>");
		});
	}
}

public class SupportPoint : DataServicePoint<Support>
{
	public override void RequestDataFromServer(Action<List<Support>> onSuccess, string error = default)
	{
		var request = AppAPIManager.instance.CreateRequest<GetSupportRequest>();
		request.Send(_ =>
		{
			Items = _.data;
			onSuccess?.Invoke(_.data);
		}, (req, errStr) =>
		{
			error = errStr;
		});
	}
}

public class ProvincePoint : DataServicePoint<API.Request.SosMap.ProvinceRequest.Province>
{
	public override void RequestDataFromServer(Action<List<API.Request.SosMap.ProvinceRequest.Province>> onSuccess, string error = default)
	{
		var request = AppAPIManager.instance.CreateRequest<GetProvinceRequest>();
		request.Send(_ =>
		{
			Items = _.data;
			onSuccess?.Invoke(_.data);
		}, (req, errStr) =>
		{
			error = errStr;
		});
	}
}