﻿using API;
using UnityEngine;

[CreateAssetMenu(fileName = "MapConfig", menuName = "sosmap/MapConfig", order = 0)]
public class MapConfig : ScriptableObject
{
	public EndpointConfiguration server;
}