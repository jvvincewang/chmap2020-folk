﻿using System;
using System.Collections;
using System.Collections.Generic;
using App.Services;
using DG.Tweening;
using NaughtyAttributes;
using UnityEngine;

public class MapSmoothMoveable : MonoBehaviour
{
	Vector2 mapPos;
	Sequence sequence;

	float forceDuration = 0;

	public void SmoothMoveTo(double longitude, double latitude, float duration = 1, Vector2 offset = default)
	{
		duration = forceDuration;

		Location to = new Location(longitude + offset.x, latitude + offset.y);
		SmoothMoveTo(to, duration, offset);
	}

	public void SmoothMoveTo(Location location, float duration = 1, Vector2 offset = default)
	{
		duration = forceDuration;

		Location to = new Location(location.longitude + offset.x, location.latitude + offset.y);
		// Location to = location;

		Vector2 newLocation = new Vector2((float) location.longitude, (float) location.latitude);
		mapPos = OnlineMaps.instance.position;

		Tween tween = DOTween.To(() => mapPos, x => mapPos = x, newLocation, duration).OnUpdate(() =>
		{
			OnlineMaps.instance.SetPosition(mapPos.x, mapPos.y);
		}).OnComplete(null).SetEase(Ease.OutQuad);

		sequence = DOTween.Sequence();
		sequence.Append(tween);
	}

	public void Stop()
	{
		sequence.Pause();
	}
}