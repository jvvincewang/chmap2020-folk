﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MapSmoothZoomable : MonoBehaviour
{
	float from;
	Sequence sequence;

	float forceDuration = 0;
	public void ZoomIn()
	{
		from = OnlineMaps.instance.floatZoom;
		float to = from + 1;

		SmoothZoomTo(to, 0.25f);
	}

	public void ZoomOut()
	{
		from = OnlineMaps.instance.floatZoom;
		float to = from - 1;
		SmoothZoomTo(to, 0.25f);
	}

	public void SmoothZoomTo(float zoom, float duration = 1)
	{
		duration = forceDuration;

		from = OnlineMaps.instance.floatZoom;
		float to = zoom;

		var tween = DOTween.To(() => from, x => from = x, to, duration).OnUpdate(() =>
		{
			OnlineMaps.instance.floatZoom = from;
		}).OnComplete(null).SetEase(Ease.OutQuad);

		sequence = DOTween.Sequence();
		sequence.Append(tween);
	}

	public void Stop()
	{
		sequence.Pause();
	}
}