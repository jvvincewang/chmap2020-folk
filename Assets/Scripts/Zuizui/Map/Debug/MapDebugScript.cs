﻿using System.Collections;
using System.Collections.Generic;
using API;
using API.Request.SosMap;
// using API.Request.CuuHoMienTrung;
using UnityEngine;
using UnityEngine.EventSystems;

public class MapDebugScript : MonoBehaviour
{
	void Start()
	{
		AppAPIManager.instance.SetDefaultEndpointConfig("EndpointConfig/SosMapNetEndpointConfig");
		// var request = AppAPIManager.instance.CreateRequest<CuuhoRequest>();
		var request = AppAPIManager.instance.CreateRequest<ProvinceRequest>();
		// set data:
		var postData = new ProvinceRequest.PostObject();
		request.SetPostData(postData);
		request.Send(_ =>
		{
			Debug.Log("name:" + _.data[0].name);
			// Debug.Log("location:" + _.data[0].);
			// Debug.Log("note:" + _.data[0].note);
			Debug.Log("geo_latitude:" + _.data[0].lat);
			Debug.Log("geo_longtitude:" + _.data[0].longtitude);

		}, (req, errStr) => { });
	}

	void Update()
	{
		// OnDeviceOrientation();

		// DebugLocationOnClick();
	}

	//FIXME:
	void DebugLocationOnClick()
	{
		if (Input.GetMouseButtonDown(0) && !EventSystem.current.IsPointerOverGameObject())
		{
			double lon, lat;
			if (OnlineMapsTileSetControl.instance.GetCoords(Input.mousePosition, out lon, out lat))
			{
				// Vector2 coords = onlineMaps.GetCorners(Input.mousePosition);
				Debug.Log($"Coords: {lon},{lat}");
			}
		}
	}

	void OnDeviceOrientation()
	{
		if (Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
		{
			UseLandscapeLeftLayout();
		}
		else if (Input.deviceOrientation == DeviceOrientation.Portrait)
		{
			UsePortraitLayout();
		}
	}

	void UseLandscapeLeftLayout()
	{
		Debug.Log("UseLandscapeLeftLayout");
		RefreshMapLayout();
	}

	void UsePortraitLayout()
	{
		Debug.Log("UsePortraitLayout");
		RefreshMapLayout();
	}

	void RefreshMapLayout()
	{
		int size = Screen.height > Screen.width ? Screen.height : Screen.width;
		OnlineMapsTileSetControl.instance.Resize(size, size);
		// tileSet.sizeInScene = new Vector2(size, size);

		/// Use mapBoundary size !!!
		// int width = (int) mapBoundary.rect.height;
		// int height = (int) mapBoundary.rect.height;
		// tileSet.sizeInScene = new Vector2(width, height);
	}
}