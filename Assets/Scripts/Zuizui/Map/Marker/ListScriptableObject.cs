﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ListObject", menuName = "sosmap/ListObject", order = 0)]
public class ListScriptableObject : ScriptableObject
{
	[System.Serializable]
	public class ListScriptableObjectItem
	{
		public string name;
		public string description;
		public Color color = default(Color);
		public Sprite image;
	}

	[NaughtyAttributes.ReorderableList]
	public List<ListScriptableObjectItem> items = new List<ListScriptableObjectItem>();
}