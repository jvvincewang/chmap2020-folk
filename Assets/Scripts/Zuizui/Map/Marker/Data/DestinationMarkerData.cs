using System;

[Serializable]
public class DestinationMarkerData : MarkerDataBase
{
	public string phone;
	public string status;
	public string level;
	public string issue;
	public string approach;
	public string description;
	public string province;
	public string updatedAt;
	public string createdAt;
}