using System;

[Serializable]
public class SupportMarkerData : MarkerDataBase
{
	public string phone;
	public string province;
	public string provide;
	public string vehicle;
	public string type;
	public string status;
	public string updatedAt;
}