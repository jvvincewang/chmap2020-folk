﻿using System;
using System.Collections.Generic;
using API.Request.SosMap;
using UnityEngine;

public class MarkerController : MonoBehaviourSingletonBase<MarkerController>
{
	[Header("References")]
	public MarkerDataController markerDataController;

	protected override void AwakeSingleton()
	{
		AppSosDataService.OnRequestDestinationCompleted += OnRequestDestinationCompleted;
		AppSosDataService.OnRequestSupportCompleted += OnRequestSupportCompleted;
		// AppSosDataService.OnRequestProvinceCompleted += OnRequestProvinceCompleted;

		AppSosDataService.OnLoadCacheDestinationCompleted += OnRequestDestinationCompleted;
		AppSosDataService.OnLoadCacheSupportCompleted += OnRequestSupportCompleted;
	}

	void OnRequestProvinceCompleted(List<ProvinceRequest.Province> data)
	{
		var markerData = AppSosDataService.ParseProvinceToMarkerData(data);

		for (int i = 0; i < markerData.Count; i++)
		{
			AddMarker(markerData[i], MarkerType.Province);
		}
	}

	void OnRequestSupportCompleted(List<SupportRequest.Support> data)
	{
		var markerData = AppSosDataService.ParseSupportToMarkerData(data);

		for (int i = 0; i < markerData.Count; i++)
		{
			AddMarker(markerData[i], MarkerType.Support);
		}
	}

	void OnRequestDestinationCompleted(List<DestinationRequest.Destination> data)
	{
		var markerData = AppSosDataService.ParseDestinationMarkerData(data);

		for (int i = 0; i < markerData.Count; i++)
		{
			AddMarker(markerData[i], MarkerType.Destination);
		}
	}

	public void AddMarker(MarkerDataBase data, MarkerType type = MarkerType.None)
	{
		switch (type)
		{
			case MarkerType.None:
				AddMarkerToMap(double.Parse(data.longitude), double.Parse(data.latitude), null);
				break;
			case MarkerType.Destination:
				markerDataController.AddDestinationMarkerToMap(data as DestinationMarkerData);
				break;
			case MarkerType.Support:
				markerDataController.AddSupportMarkerToMap(data as SupportMarkerData);
				break;
			case MarkerType.Province:
				markerDataController.AddProvinceMarkerToMap(data as ProvinceMarkerData);
				break;
		}
	}

	public void ClearMarkersOnMap()
	{
		ClearMarkersOnMapAt(markerDataController.destinations);

		ClearMarkersOnMapAt(markerDataController.supports);

		ClearMarkersOnMapAt(markerDataController.province);

		ClearMarkersOnMapAt(markerDataController.defaultMakers);
	}

	public void SetActiveAllMarkersOnMap(bool enable)
	{
		SetActiveMarkersOnMapAt(enable);
	}

	public OnlineMapsMarker AddMarkerToMap(double longitude, double latitude, Action<OnlineMapsMarkerBase> OnClick = null)
	{
		return markerDataController.AddMarkerToMap(longitude, latitude, null, OnClick);
	}

	public OnlineMapsMarker AddMarkerToMap(double longitude, double latitude, Texture2D textureMarker, Action<OnlineMapsMarkerBase> OnClick = null)
	{
		return markerDataController.AddMarkerToMap(longitude, latitude, textureMarker, OnClick);
	}

	public void SetActiveMarkersOnMapAt(bool enable)
	{
		SetActiveMarkersOnMapAt(markerDataController.destinations, enable);

		SetActiveMarkersOnMapAt(markerDataController.supports, enable);

		SetActiveMarkersOnMapAt(markerDataController.province, enable);

		SetActiveMarkersOnMapAt(markerDataController.defaultMakers, enable);
	}

	void SetActiveMarkersOnMapAt(List<OnlineMapsMarker> markers, bool enable)
	{
		for (int i = 0; i < markers.Count; i++)
		{
			markers[i].enabled = enable;
		}
	}

	void ClearMarkersOnMapAt(List<OnlineMapsMarker> markers)
	{
		for (int i = 0; i < markers.Count; i++)
		{
			OnlineMapsMarkerManager.RemoveItem(markers[i]);
		}

		markers.Clear();
	}
}