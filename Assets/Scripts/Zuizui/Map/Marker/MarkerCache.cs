﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using API.Request.SosMap;
using Newtonsoft.Json;
using UnityEngine;
using static API.Request.SosMap.DestinationRequest;
using static API.Request.SosMap.SupportRequest;


namespace OfflineMap
{
    public static class MarkerCache
    {
        public static List<Support> Supports
        {
            get
            {
                if(!File.Exists(SupportMarkersCachePath)) return new List<Support>();
                
                var json = ReadAllText(SupportMarkersCachePath);
                return JsonConvert.DeserializeObject<SupportMarkers>(json).supports;
            }
        }

        public static List<Destination> Destinations
        {
            get
            {
                if(!File.Exists(DestinationMarkersCachePath)) return new List<Destination>();

                var json = ReadAllText(DestinationMarkersCachePath);
                return JsonConvert.DeserializeObject<DestinationMarkers>(json).destinations;
            }
        }

        private static string FolderPath
        {
            get
            {
                var folderPath = Path.Combine(Application.persistentDataPath, "MarkerCache");

                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }

                return folderPath;
            }
        }

        public static string SupportMarkersCachePath => Path.Combine(FolderPath, "SupportMarkers.json");
        public static string DestinationMarkersCachePath => Path.Combine(FolderPath, "DestinationMarkers.json");

        public static void SaveMarkersSupport(List<Support> supports)
        {
            var json = JsonConvert.SerializeObject(new SupportMarkers()
            {
                supports = supports
            });
            
            WriteAllText(SupportMarkersCachePath, json);
        }

        public static void SaveMarkersDestination(List<Destination> destinations)
        {
            var json = JsonConvert.SerializeObject(new DestinationMarkers()
            {
                destinations = destinations
            });
            
            WriteAllText(DestinationMarkersCachePath, json);
        }

        private static void WriteAllText(string path, string text)
        {
            File.WriteAllText(path, text);
        }

        private static string ReadAllText(string path)
        {
            return File.ReadAllText(path);
        }
    }

    [Serializable]
    public class SupportMarkers
    {
        public List<Support> supports = new List<Support>();
        
    }

    [Serializable]
    public class DestinationMarkers
    {
        public List<Destination> destinations = new List<Destination>();
    }
}