﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectionMarkerItem : MonoBehaviour
{
	public Text txtContent;
	public Image imgIcon;
	public Button btnClick;
}