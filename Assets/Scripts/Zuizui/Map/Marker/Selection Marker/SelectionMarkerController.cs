﻿using System;
using Database;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

public class SelectionMarkerController : MonoBehaviourSingletonBase<SelectionMarkerController>
{
	[SerializeField] GameObject panel;
	[Space]
	public Text txtTitle;
	public Transform labelContainer;
	public Transform buttonContainer;

	[Space]
	public SelectionMarkerItem labelPrefabs;
	public SelectionMarkerItem buttonPrefabs;

	[Space]
	ImageAssets imageAsset;
	ListScriptableObject supportType;
	ListScriptableObject destinationLevel;

	ObjectPool<SelectionMarkerItem> labelPools = new ObjectPool<SelectionMarkerItem>();
	ObjectPool<SelectionMarkerItem> buttonPools = new ObjectPool<SelectionMarkerItem>();

	protected override void AwakeSingleton()
	{
		imageAsset = Database.DataAssets.Image;
		destinationLevel = Database.DataAssets.DestinationLevel;
		supportType = Database.DataAssets.SupportType;

		labelPools.Init(labelPrefabs, labelContainer, 7);
		buttonPools.Init(buttonPrefabs, buttonContainer, 3);
	}

	void Start()
	{
		OnlineMapsControlBase.instance.OnMapClick += OnMapClick;
	}

	void OnMapClick()
	{
		Hide();
	}

	public void UpdateLabelDataFrom(string title, MarkerDataBase data)
	{
		txtTitle.text = title;

		Reset();

		CreateItemHasContent("", data.name, imageAsset.label.icon);

		switch (data)
		{
			case SupportMarkerData supportMarkerData:
				var supportData = (SupportMarkerData) data;

				CreateItemHasContent("", supportData.province, imageAsset.province.icon);
				CreateItemHasContent("", HandleSupportType(supportData.type), imageAsset.type.icon);
				CreateItemHasContent("", supportData.phone, imageAsset.phone.icon, () => { Debug.Log(supportData.phone); });
				CreateItemHasContent("", supportData.provide, imageAsset.provide.icon);
				CreateItemHasContent("", supportData.vehicle, imageAsset.vehicle.icon);
				CreateItemHasContent("", supportData.updatedAt.ToString(), imageAsset.updateTime.icon);
				break;
			case DestinationMarkerData destinationMarkerData:
				var desData = (DestinationMarkerData) data;

				CreateItemHasContent("", HandleDestinationTypeToString(desData.level), imageAsset.level.icon);
				CreateItemHasContent("", desData.province, imageAsset.province.icon);
				CreateItemHasContent("", desData.phone, imageAsset.phone.icon, () => { Debug.Log(desData.phone); });
				CreateItemHasContent("", desData.issue, imageAsset.issue.icon);
				CreateItemHasContent("", desData.approach, imageAsset.vehicle.icon);
				CreateItemHasContent("", desData.description, imageAsset.note.icon);
				CreateItemHasContent("", desData.updatedAt.ToString(), imageAsset.updateTime.icon);
				break;
			case ProvinceMarkerData provinceMarkerData:
				var provinceData = (ProvinceMarkerData) data;

				CreateItemHasContent("", provinceData.updatedAt.ToString(), imageAsset.updateTime.icon);
				break;
			default:
				break;
		}

	}

	public SelectionMarkerItem CreateItemHasContent(string label, string content, Sprite icon, Action onClick = null)
	{
		if (string.IsNullOrEmpty(content))
			return null;

		SelectionMarkerItem item = CreateItem(label, content, icon, onClick);
		return item;
	}

	//TODO: update label
	public SelectionMarkerItem CreateItem(string label, string content, Sprite icon, Action onClick = null)
	{
		SelectionMarkerItem item = labelPools.Get();
		item.transform.SetSiblingIndex(labelPools.Count - 1);
		item.gameObject.SetActive(true);

		item.txtContent.text = content;
		item.imgIcon.sprite = icon;

		item.btnClick.onClick.RemoveAllListeners();
		item.btnClick.onClick.AddListener(() =>
			onClick?.Invoke()
		);

		return item;
	}

	public SelectionMarkerItem CreateButton(string content, Action onClick)
	{
		SelectionMarkerItem item = CreateButton(content, null, onClick);
		return item;
	}

	public SelectionMarkerItem CreateButton(string content, Sprite icon, Action onClick)
	{
		SelectionMarkerItem item = buttonPools.Get();
		item.transform.SetSiblingIndex(buttonPools.Count - 1);
		item.gameObject.SetActive(true);

		item.txtContent.text = content;

		if (icon != null)
			item.imgIcon.sprite = icon;

		item.btnClick.onClick.RemoveAllListeners();
		item.btnClick.onClick.AddListener(() =>
			onClick?.Invoke()
		);

		return item;
	}

	public void Hide()
	{
		if (panel.activeSelf)
			panel.SetActive(false);
	}

	public void Show()
	{
		if (!panel.activeSelf)
			panel.SetActive(true);
	}

	[Button]
	public void Reset()
	{
		labelPools.ReturnAllObject();
		buttonPools.ReturnAllObject();
	}

	string HandleSupportType(string type)
	{
		if (type == null)
			return string.Empty;

		// Debug.Log($"type: {type}");
		return supportType.items[int.Parse(type)].name;
	}

	string HandleDestinationTypeToString(string level)
	{
		if (string.IsNullOrEmpty(level))
			return string.Empty;

		Debug.Log("level: " + level);

		return destinationLevel.items[int.Parse(level) - 1].name;
	}
}