using System;

[Serializable]
public class MarkerDataBase
{
	public string name;
	public string longitude;
	public string latitude;
}