﻿public enum MarkerType
{
	None,
	Destination,
	Support,
	Province
}