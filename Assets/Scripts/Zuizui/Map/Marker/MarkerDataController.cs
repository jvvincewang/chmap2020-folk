﻿using System;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;

public class MarkerDataController : MonoBehaviour
{
	public class MapMarker
	{
		public List<OnlineMapsMarker> markers = new List<OnlineMapsMarker>();

		public void Add()
		{

		}

		public void RemoveOnMap()
		{
			for (int i = 0; i < markers.Count; i++)
			{
				OnlineMapsMarkerManager.RemoveItem(markers[i]);
			}

			markers.Clear();
		}
	}

	[SerializeField] DirectionsPanel directionsPanel;
	[Header("Marker Config")]
	[SerializeField] Texture2D destinationMarker;
	[SerializeField] Texture2D provinceMarker;
	// public MapMarker destinations = new MapMarker();
	public List<OnlineMapsMarker> destinations = new List<OnlineMapsMarker>();
	public List<OnlineMapsMarker> supports = new List<OnlineMapsMarker>();
	public List<OnlineMapsMarker> province = new List<OnlineMapsMarker>();
	public List<OnlineMapsMarker> defaultMakers = new List<OnlineMapsMarker>();

	ListScriptableObject supportType;
	void Awake()
	{
		supportType = Database.DataAssets.SupportType;
	}

	public OnlineMapsMarker AddMarkerToMap(double longitude, double latitude, Texture2D textureMarker,
		Action<OnlineMapsMarkerBase> OnClick = null)
	{
		OnlineMapsMarker marker = OnlineMapsMarkerManager.CreateItem(longitude, latitude, textureMarker);

		if (OnClick != null)
			marker.OnClick += OnClick;

		defaultMakers.Add(marker);

		return marker;
	}

	public OnlineMapsMarker AddMarkerToMap(MarkerDataBase data, List<OnlineMapsMarker> markerGroup, Texture2D textureMarker,
		Action<OnlineMapsMarkerBase> OnClick = null)
	{
		OnlineMapsMarker marker = OnlineMapsMarkerManager.CreateItem(double.Parse(data.longitude),
			double.Parse(data.latitude), textureMarker);

		if (OnClick != null)
			marker.OnClick += OnClick;

		marker["data"] = data;

		markerGroup.Add(marker);

		return marker;
	}

	public void AddDestinationMarkerToMap(DestinationMarkerData data)
	{
		if (string.IsNullOrEmpty(data.longitude) || string.IsNullOrEmpty(data.latitude))
			return;

		AddMarkerToMap(data, destinations, destinationMarker, OnClick);
	}

	public void AddSupportMarkerToMap(SupportMarkerData data)
	{
		if (string.IsNullOrEmpty(data.longitude) || string.IsNullOrEmpty(data.latitude))
			return;

		AddMarkerToMap(data, supports, supportType.items[int.Parse(data.type)].image.texture, OnClick);
	}

	public void AddProvinceMarkerToMap(ProvinceMarkerData data)
	{
		if (string.IsNullOrEmpty(data.longitude) || string.IsNullOrEmpty(data.latitude))
			return;

		AddMarkerToMap(data, province, provinceMarker);
	}

	void OnClick(OnlineMapsMarkerBase marker)
	{
		var data = marker["data"] as MarkerDataBase;

		if (data == null) return;

		// Debug.Log($"Marker Location {data.longitude} {data.latitude}");
		var offset = new Vector2(0, -0.004f);

		MapController.instance.SmoothMoveAndZoom(double.Parse(data.longitude), double.Parse(data.latitude), 16, 1, offset);

		SelectionMarkerController.instance.UpdateLabelDataFrom("Chi Tiết", data);
		SelectionMarkerController.instance.CreateButton("Directions", Database.DataAssets.Image.directions.icon, () =>
		{
			SearchDirection(data);
		});

		SelectionMarkerController.instance.Show();
	}

	private void SearchDirection(MarkerDataBase data)
	{
		if (!Internet.IsAvailable() && !Internet.IsReachableViaCarrierDataNetwork())
		{
			Debug.LogError("No Internet connection");
			return;
		}

		if (!OnlineMapsKeyManager.hasGoogleMaps)
		{
			Debug.LogWarning("Please enter Map / Key Manager / Google Maps");
			return;
		}

		var longitude = float.Parse(data.longitude);
		var latitude = float.Parse(data.latitude);
		var origin = "";

		// Memo for use the search direction api class
		// origin order: lat, long
		// destination order: long, lat

#if !UNITY_EDITOR
		MyLocation.instance.GetCurrentLocation(location =>
		{
			var originLat = location.latitude.ToString(CultureInfo.InvariantCulture);
			var originLng = location.longitude.ToString(CultureInfo.InvariantCulture);
			origin = $"{originLat}, {originLng}";
			// directions.DrawRouteMap(origin, new Vector2(longitude, latitude));

			// var request =
			// 	new OnlineMapsGoogleDirections(OnlineMapsKeyManager.GoogleMaps(),
			// 		origin, new Vector2(longitude, latitude));
			// request.OnComplete += OnGoogleDirectionsComplete;
			// request.Send();
		});
#else
		// origin lat long
		origin = $"21.0277644, 105.8341598";
		directionsPanel.DrawRouteMap(origin, new Vector2(longitude, latitude));
		// var DebugRequest =
		// 	new OnlineMapsGoogleDirections(OnlineMapsKeyManager.GoogleMaps(),
		// 		origin, new Vector2(longitude, latitude));
		// DebugRequest.OnComplete += OnGoogleDirectionsComplete;
		// DebugRequest.Send();
#endif
	}

}