﻿using System.Linq;
using App.Services;
using App.UI;
using UnityEngine;

public class DirectionsPanel : BaseUIPanel
{
	static DirectionsPanel instance;
	public static DirectionsPanel Instance => instance;

	[Space]
	[NaughtyAttributes.ReorderableList]
	public GameObject[] anotherPanels;

	void Awake()
	{
		instance = this;
	}

	protected override void OnOpen()
	{
		ShowPanels(false);

		var myLocation = MyLocation.instance.lastLocation;

		if (myLocation != null)
			MapController.instance.SmoothMoveAndZoom(myLocation, 16, 0);

		MarkerController.instance.SetActiveAllMarkersOnMap(false);
	}

	protected override void OnClose()
	{
		ShowPanels(true);

		MarkerController.instance.SetActiveAllMarkersOnMap(true);
	}

	void ShowPanels(bool value)
	{
		foreach (var item in anotherPanels)
		{
			item.SetActive(value);
		}
	}

	public void DrawRouteMap(Location origin, Location destination)
	{
		DrawRouteMap(origin.ToVector2(), destination.ToVector2());
	}

	//TODO:
	public void DrawRouteMap(object origin, object destination)
	{
		var request =
			new OnlineMapsGoogleDirections(OnlineMapsKeyManager.GoogleMaps(),
				origin, destination);
		request.OnComplete += OnGoogleDirectionsComplete;
		request.Send();
	}

	void OnGoogleDirectionsComplete(string response)
	{
		Debug.Log(response);

		// Try load result
		var result = OnlineMapsGoogleDirections.GetResult(response);
		if (result == null || result.routes.Length == 0) return;

		// Get the first route
		OnlineMapsGoogleDirectionsResult.Route route = result.routes[0];

		// Draw route on the map
		OnlineMapsDrawingElementManager.AddItem(new OnlineMapsDrawingLine(route.overview_polyline, Color.red, 3));

		// Calculate the distance
		var distance = route.legs.Sum(l => l.distance.value); // meters

		// Calculate the duration
		var duration = route.legs.Sum(l => l.duration.value); // seconds

		// Log distance and duration
		Debug.Log("Distance: " + distance + " meters, or " + (distance / 1000f).ToString("F2") + " km");
		Debug.Log("Duration: " + duration + " sec, or " + (duration / 60f).ToString("F1") + " min, or " +
			(duration / 3600f).ToString("F1") + " hours");
	}

}