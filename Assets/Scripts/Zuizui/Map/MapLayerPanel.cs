﻿using System.Collections.Generic;
using App.UI;
using UnityEngine;
using UnityEngine.UI;

public class MapLayerPanel : BaseUIPanel
{
	[System.Serializable]
	public class MapType
	{
		public string provider;
		public string satelliteStyle;
		public string reliefStyle;
	}

	const string MAP_STYLE = "MapStyle";
	const string MAP_PROVIDER = "MapProvider";

	int mapStyleIndex;
	int mapProviderIndex;
	string mapSelected;
	[NaughtyAttributes.ReorderableList]
	[SerializeField] List<MapType> maptype = new List<MapType>();

	void LogTypeList()
	{
		// Gets all providers
		OnlineMapsProvider[] providers = OnlineMapsProvider.GetProviders();
		foreach (OnlineMapsProvider provider in providers)
		{
			Debug.Log(provider.id);
			foreach (OnlineMapsProvider.MapType type in provider.types)
			{
				Debug.Log(type);
			}
		}
	}

	void Start()
	{
		// Show full provider list
		// LogTypeList();
		mapProviderIndex = PlayerPrefs.GetInt(MAP_PROVIDER, 0); // google
		mapStyleIndex = PlayerPrefs.GetInt(MAP_STYLE, 1); // Relief

		RefreshMap();
	}

	public void ChangeMapProvider(int providerIndex)
	{
		mapProviderIndex = providerIndex;
		PlayerPrefs.SetInt(MAP_PROVIDER, mapProviderIndex);
		RefreshMap();
	}

	public void ChangeMapStyle(int styleIndex)
	{
		mapStyleIndex = styleIndex;
		PlayerPrefs.SetInt(MAP_STYLE, mapStyleIndex);
		RefreshMap();
	}

	public void RefreshMap()
	{
		string provider = maptype[mapProviderIndex].provider;
		string style = (mapStyleIndex == 0) ? maptype[mapProviderIndex].satelliteStyle : maptype[mapProviderIndex].reliefStyle;
		OnlineMaps.instance.mapType = $"{provider}.{style}";
		// Debug.Log($"{provider}.{style}");
	}

	protected override void OnOpen()
	{
		// throw new System.NotImplementedException();
	}

	protected override void OnClose()
	{
		// throw new System.NotImplementedException();
	}
}