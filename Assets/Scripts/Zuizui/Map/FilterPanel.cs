﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using App.UI;
using API.Request.SosMap;
using UnityEngine;
using UnityEngine.UI;

public class FilterPanel : BaseUIPanel
{
	ListScriptableObject lstSupportType;
	ListScriptableObject lstDestinationLevel;
	[Space]
	public Dropdown SupportTypeDropdown;
	public Dropdown DestinationLevelDropdown;

	void Awake()
	{
		lstDestinationLevel = Database.DataAssets.DestinationLevel;
		lstSupportType = Database.DataAssets.SupportType;

		PrepareDropdownList();

		AppSosDataService.OnRequestDestinationCompleted += OnRequestDestinationCompleted;
		AppSosDataService.OnRequestSupportCompleted += OnRequestSupportCompleted;
		// AppSosDataService.OnRequestProvinceCompleted += OnRequestProvinceCompleted;

		AppSosDataService.OnLoadCacheDestinationCompleted += OnRequestDestinationCompleted;
		AppSosDataService.OnLoadCacheSupportCompleted += OnRequestSupportCompleted;
	}

	void PrepareDropdownList()
	{
		SupportTypeDropdown.enabled = false;
		DestinationLevelDropdown.enabled = false;

		PrepareSupportTypeDropdownList();
		PrepareDestinationDropdownList();
	}

	void OnRequestSupportCompleted(List<SupportRequest.Support> obj)
	{
		SupportTypeDropdown.enabled = true;
		SupportTypeDropdown.SetValueWithoutNotify(0);
	}

	void OnRequestDestinationCompleted(List<DestinationRequest.Destination> obj)
	{
		DestinationLevelDropdown.enabled = true;
		DestinationLevelDropdown.SetValueWithoutNotify(0);
	}

	private void PrepareSupportTypeDropdownList()
	{
		var options = new List<Dropdown.OptionData>();

		var defaultOptionData = new Dropdown.OptionData("Tất cả");
		options.Add(defaultOptionData);

		for (var i = 0; i < lstSupportType.items.Count; i++)
		{
			var optionData = new Dropdown.OptionData(lstSupportType.items[i].name);
			options.Add(optionData);
		}

		SupportTypeDropdown.AddOptions(options);
		SupportTypeDropdown.onValueChanged.RemoveAllListeners();
		SupportTypeDropdown.onValueChanged.AddListener(OnSupportTypeValueChanged);
	}

	void OnSupportTypeValueChanged(int index)
	{
		var supports = MarkerController.instance.markerDataController.supports;
		if (index == 0)
		{
			supports.All(x => x.enabled = true);
		}
		else
		{
			ShowMarkerGroupsSupportTypeValue(supports, index - 1);
		}
	}

	void PrepareDestinationDropdownList()
	{
		var options = new List<Dropdown.OptionData>();

		var defaultOptionData = new Dropdown.OptionData("Tất cả");
		options.Add(defaultOptionData);

		for (var i = 0; i < lstDestinationLevel.items.Count; i++)
		{
			var optionData = new Dropdown.OptionData(lstDestinationLevel.items[i].name);
			options.Add(optionData);
		}

		DestinationLevelDropdown.AddOptions(options);
		DestinationLevelDropdown.onValueChanged.RemoveAllListeners();
		DestinationLevelDropdown.onValueChanged.AddListener(OnDestinationLevelValueChanged);
	}

	void OnDestinationLevelValueChanged(int index)
	{
		var destinations = MarkerController.instance.markerDataController.destinations;
		if (index == 0)
		{
			destinations.All(x => x.enabled = true);
		}
		else
		{
			ShowMarkerGroupsDestinationLevelValue(destinations, index - 1);
		}
	}

	void ShowMarkerGroupsSupportTypeValue(List<OnlineMapsMarker> lstValue, int value)
	{
		for (int i = 0; i < lstValue.Count; i++)
		{
			var data = lstValue[i]["data"] as SupportMarkerData;

			if (data != null)
			{
				if (data.type == value.ToString())
				{
					lstValue[i].enabled = true;
				}
				else
				{
					lstValue[i].enabled = false;
				}
			}
		}
	}

	void ShowMarkerGroupsDestinationLevelValue(List<OnlineMapsMarker> lstValue, int value)
	{
		for (int i = 0; i < lstValue.Count; i++)
		{
			var data = lstValue[i]["data"] as DestinationMarkerData;

			if (data != null)
			{
				if (data.level == value.ToString())
				{
					lstValue[i].enabled = true;
				}
				else
				{
					lstValue[i].enabled = false;
				}
			}
		}
	}

	protected override void OnOpen()
	{
		// throw new NotImplementedException();
	}

	protected override void OnClose()
	{
		// throw new NotImplementedException();
	}
}