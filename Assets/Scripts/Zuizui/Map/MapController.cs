﻿using System;
using App.Services;
using UnityEngine;
using UnityEngine.Events;

public class MapController : MonoBehaviourSingletonBase<MapController>
{
	[Header("References")]
	[SerializeField] MapSmoothZoomable mapSmoothZoomable;
	[SerializeField] MapSmoothMoveable mapSmoothMoveable;
	OnlineMapsControlBase onlineMapsControl;
	protected override void AwakeSingleton()
	{
		base.AwakeSingleton();
		onlineMapsControl = OnlineMapsControlBase.instance;
	}

	void Start()
	{
		onlineMapsControl.OnMapClick += OnStopInteracable;
		onlineMapsControl.OnMapClick += OnMapClick;
		onlineMapsControl.OnMapDoubleClick += OnMapDoubleClick;

		onlineMapsControl.OnMapDrag += OnStopInteracable;
	}

	void OnMapDoubleClick()
    {
		MyLocation.instance.OnFinishGetLocation(GetLocationAt(Input.mousePosition));
	}

	void OnMapClick()
	{
		Debug.Log(GetLocationAt(Input.mousePosition));
	}

	void OnStopInteracable()
	{
		mapSmoothZoomable.Stop();
		mapSmoothMoveable.Stop();
	}

	public Location GetLocationAt(Vector2 mousePos)
	{
		var coords = onlineMapsControl.GetCoords(mousePos);
		return new Location(coords.x, coords.y);
	}

	public void GetAddressByLocation(Vector2 coords, Action<string> callback)
	{
		// Try find location name by coordinates.
		OnlineMapsGoogleGeocoding request = new OnlineMapsGoogleGeocoding(coords, OnlineMapsKeyManager.GoogleMaps());
		request.Send();
		request.OnComplete += callback;
	}		

	public void UpdateLocationFromAddress(string address)
    {

		OnlineMapsGoogleGeocoding request = new OnlineMapsGoogleGeocoding(address, OnlineMapsKeyManager.GoogleMaps());
		request.Send();

		// Specifies that search results should be sent to OnFindLocationComplete.
		request.OnComplete += (response)=> {
			Vector2 position = OnlineMapsGoogleGeocoding.GetCoordinatesFromResult(response);
			if(position != Vector2.zero)
            {
				Debug.Log($"Update location from Address : longitude {position.x} - latitude {position.y}");
				Location location = new Location(position.x, position.y);
				MyLocation.instance.OnFinishGetLocation(location);
            }
		};
	}

	public void PlacesAutocomplete(string text, Action<string> callback)
    {
		OnlineMapsGooglePlacesAutocomplete.Find(text, OnlineMapsKeyManager.GoogleMaps()).OnComplete += callback;
	}

	public void SmoothMoveAndZoom(Location location, float zoom = 10, float duration = 2, Vector2 offset = default)
	{
		SmoothMoveAndZoom(location.longitude, location.latitude, zoom, duration, offset);
	}

	public void SmoothMoveAndZoom(double lng, double lat, float zoom = 10, float duration = 2, Vector2 offset = default)
	{
		SmoothMoveTo(lng, lat, duration, offset);
		SmoothZoomTo(zoom, duration);
	}

	public void SmoothMoveTo(double lng, double lat, float duration = 2, Vector2 offset = default)
	{
		mapSmoothMoveable.SmoothMoveTo(lng, lat, duration, offset);
	}

	public void SmoothZoomTo(float zoom, float duration = 1)
	{
		mapSmoothZoomable.SmoothZoomTo(zoom, duration);
	}
}