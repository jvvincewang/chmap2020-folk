using System.Collections.Generic;
using System.Globalization;
using API.Request.SosMap;
using UnityEngine;
using UnityEngine.UI;

namespace App.Map.Search
{
	public class SearchController : MonoBehaviour
	{
		[SerializeField]
		private SearchResultPanel _resultPanel;

		[SerializeField]
		private InputField _searchField;

		[SerializeField]
		private GameObject _closeSearchBtn;

		[SerializeField]
		private GameObject _clearSearchTxtBtn;

		[SerializeField]
		private int _defaultSearchZoom;

		private OnlineMapsMarker marker;
		private int _siblingIdx;
		private readonly List<SearchDetailObject> _searchableDestinationLst = new List<SearchDetailObject>();
		private readonly List<SearchDetailObject> _searchableSupportLst = new List<SearchDetailObject>();
		private bool _isParsing;
		private SearchDetailObject _currentSearch;

		private void Awake()
		{
			// Set the data preparation callback
			AppSosDataService.OnRequestDestinationCompleted += OnRequestDestinationCompleted;
			AppSosDataService.OnLoadCacheDestinationCompleted += OnRequestDestinationCompleted;
			AppSosDataService.OnRequestSupportCompleted += AppSOSDataOnOnRequestSupportCompleted;
			AppSosDataService.OnLoadCacheSupportCompleted += AppSOSDataOnOnRequestSupportCompleted;

			// Cache the panel sibling idx
			_siblingIdx = transform.GetSiblingIndex();
		}

		private void AppSOSDataOnOnRequestSupportCompleted(List<SupportRequest.Support> supports)
		{
			_searchableSupportLst.Clear();
			Debug.Log($"<color=cyan>Search panel: [Supporters]</color> request count: {supports.Count}");

			foreach (var sup in supports)
			{
				var detailObj = new SearchDetailObject
				{
					latitude = sup.lat,
						longtitude = sup.longtitude,
						name = sup.name,
						keyWordShowInSearch = $"{sup.name} {sup.area}",
						searchTargetType = SearchTargetType.Support,
						area = sup.area
				};
				_searchableSupportLst.Add(detailObj);
			}
		}

		private void OnRequestDestinationCompleted(List<DestinationRequest.Destination> destinations)
		{
			_searchableDestinationLst.Clear();
			Debug.Log($"<color=cyan>Search panel: [Destinations]</color> request count: {destinations.Count}");

			foreach (var des in destinations)
			{
				var detailObj = new SearchDetailObject
				{
					latitude = des.lat,
						longtitude = des.longtitude,
						name = des.name,
						keyWordShowInSearch = $"{des.name}",
						searchTargetType = SearchTargetType.Destination
				};
				_searchableDestinationLst.Add(detailObj);
			}
		}

		public void OnSelectingInputText()
		{
			_closeSearchBtn.SetActive(true);
			_resultPanel.OpenResult();
			// bring search panel to front of all other ui component
			transform.SetAsLastSibling();
		}

		public void OnEndEditingSearchText() { }

		/// <summary>
		/// Callback OnValue Change of the search input field
		/// </summary>
		public void OnSearchValueChange()
		{
			_clearSearchTxtBtn.SetActive(!string.IsNullOrEmpty(_searchField.text));
			SearchLocationFromCacheList();
		}

		/// <summary>
		/// OnClick callback of the close search button
		/// </summary>
		public void OnCloseSearchButtonClick()
		{
			_closeSearchBtn.SetActive(false);
			_resultPanel.CloseResult();

			// give the search panel back to it sibling idx on hierarchy
			transform.SetSiblingIndex(_siblingIdx);
		}

		/// <summary>
		/// Search the location from cache list
		/// </summary>
		private void SearchLocationFromCacheList()
		{
			if (string.IsNullOrEmpty(_searchField.text))
			{
				return;
			}

			// Only allow search from the available list 
			if (_searchableDestinationLst.Count == 0)
			{
				return;
			}

			var showList = new List<SearchDetailObject>();
			showList.AddRange(GetSearchList(_searchField.text, _searchableDestinationLst));
			showList.AddRange(GetSearchList(_searchField.text, _searchableSupportLst));
			_resultPanel.SetDisplayResult(new SearchResultPanel.SearchResultDisplayParams
			{
				resultDetail = showList
			});
		}

		private List<SearchDetailObject> GetSearchList(string searchStr, List<SearchDetailObject> targetLst)
		{
			var lst = new List<SearchDetailObject>();
			foreach (var result in targetLst)
			{
				var isContain = result.keyWordShowInSearch.Contains(searchStr);
				result.OnClickSearchDetailButton = null;
				if (!isContain)
				{
					continue;
				}

				result.OnClickSearchDetailButton = () =>
				{
					_searchField.text = result.name;
					if (!string.IsNullOrEmpty(result.longtitude) && !string.IsNullOrEmpty(result.latitude))
					{
						var x = double.Parse(result.longtitude);
						var y = double.Parse(result.latitude);
						OnlineMaps.instance.SetPosition(x, y);
						OnlineMaps.instance.zoom = _defaultSearchZoom;
					}
					else
					{
						SearchGoogleMapLocation();
					}

					CloseAllResultAndReset();
				};
				lst.Add(result);
			}

			return lst;
		}

		/// <summary>
		/// Fast clear search text
		/// </summary>
		public void OnClickClearText()
		{
			_searchField.text = string.Empty;
		}

		#region Search Google location
		private void SearchGoogleMapLocation()
		{
			if (!Internet.IsAvailable() && !Internet.IsReachableViaCarrierDataNetwork())
			{
				Debug.LogError("No Internet connection");
				return;
			}

			if (!OnlineMapsKeyManager.hasGoogleMaps)
			{
				Debug.LogWarning("Please enter Map / Key Manager / Google Maps");
				return;
			}

			if (_searchField.text.Length < 3)
			{
				return;
			}

			var locationName = _searchField.text;
			var request = new OnlineMapsGoogleGeocoding(locationName, OnlineMapsKeyManager.GoogleMaps());
			request.OnComplete += OnGeocodingComplete;
			request.OnFailed += OnFailedSearch;
			request.Send();
		}

		private void OnFailedSearch(OnlineMapsTextWebService obj)
		{
			Debug.LogError($"Search failed{obj.status}");
		}

		private void OnGeocodingComplete(string response)
		{
			Debug.Log($"search response: {response}");
			var results = OnlineMapsGoogleGeocoding.GetResults(response);
			if (results == null || results.Length == 0)
			{
				Debug.Log(response);
				return;
			}

			var r = results[0];
			OnlineMaps.instance.position = r.geometry_location;

			OnlineMapsUtils.GetCenterPointAndZoom(new [] { r.geometry_bounds_northeast, r.geometry_bounds_southwest },
				out _, out var zoom);
			OnlineMaps.instance.zoom = zoom;
		}
		#endregion

		/// <summary>
		/// Reset Search panel
		/// </summary>
		private void CloseAllResultAndReset()
		{
			_closeSearchBtn.SetActive(false);
			if (!string.IsNullOrEmpty(_searchField.text))
			{
				_resultPanel.CloseResult();
			}

			transform.SetSiblingIndex(_siblingIdx);
		}
	}
}