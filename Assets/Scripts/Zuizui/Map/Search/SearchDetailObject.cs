using System;
using UnityEngine.Events;

namespace App.Map.Search
{
	[Serializable]
	public class SearchDetailObject
	{
		public string keyWordShowInSearch;
		public string longtitude;
		public string latitude;
		// this is the address of the destination
		public string name;
		public SearchTargetType searchTargetType;
		public string area;

		/// <summary>
		/// The 
		/// </summary>
		public UnityAction OnClickSearchDetailButton;
	}
}