using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UIElements;

namespace App.Map.Search
{
	public class SearchResultPanel : MonoBehaviour
	{
		[SerializeField]
		private CanvasGroup _cvGrp;

		[SerializeField]
		private float _fadeDuration;

		[SerializeField]
		private GameObject _itemPrefab;

		[SerializeField]
		private GameObject _scrollviewContainer;

		private List<GameObject> _poolList = new List<GameObject>();

		private SearchResultDisplayParams _resultDisplayParams = new SearchResultDisplayParams();
		private Tween _fadeTween;

		[Serializable]
		public class SearchResultDisplayParams
		{
			public List<SearchDetailObject> resultDetail;
		}

		public void CloseResult()
		{
			_fadeTween?.Kill();
			_fadeTween = _cvGrp.DOFade(0f, _fadeDuration)
				.OnComplete(() =>
				{
					gameObject.SetActive(false);
					ResetPanel();
				});
		}

		public void OpenResult()
		{
			_fadeTween?.Kill();
			_fadeTween = _cvGrp.DOFade(1f, _fadeDuration)
				.OnStart(() =>
				{
					gameObject.SetActive(true);
				});
		}

		public void SetDisplayResult(SearchResultDisplayParams displayParams)
		{
			_resultDisplayParams = displayParams;
			UpdatePanelView();
		}

		private void UpdatePanelView()
		{
			ResetPanel();
			foreach (var display in _resultDisplayParams.resultDetail)
			{
				var reuseAble = _poolList.Find(x => !x.activeSelf);
				GameObject item;
				if (reuseAble != null)
				{
					item = reuseAble;
				}
				else
				{
					item = Instantiate(_itemPrefab, _scrollviewContainer.transform);
					_poolList.Add(item);
				}
				var itemComp = item.GetComponent<SearchDetailListItem>();
				itemComp.Init(display);
				item.SetActive(true);
			}
		}

		/// <summary>
		/// Reset the panel to default appearance
		/// </summary>
		private void ResetPanel()
		{
			foreach (Transform child in _scrollviewContainer.transform)
			{
				child.gameObject.SetActive(false);
			}
		}
	}
}