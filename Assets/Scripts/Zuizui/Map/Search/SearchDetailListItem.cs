using System;
using UnityEngine;
using UnityEngine.UI;

namespace App.Map.Search
{
	public class SearchDetailListItem : MonoBehaviour
	{
		[SerializeField]
		private Text _detail;

		[SerializeField]
		private Button _itemClickBtn;

		[SerializeField]
		private Image _label;

		[Header("Label Image")]
		[Space]
		[SerializeField]
		private Sprite _destinationSpr;
		[SerializeField]
		private Sprite _supporterSpr;

		private SearchDetailObject _detailObj;

		public void Init(SearchDetailObject param)
		{
			_detailObj = param;
			_detail.text = _detailObj.name;
			_itemClickBtn.onClick.RemoveAllListeners();
			if (_detailObj.OnClickSearchDetailButton != null)
			{
				_itemClickBtn.onClick.AddListener(_detailObj.OnClickSearchDetailButton);
			}
			SetUpLabelImage();
		}

		private void SetUpLabelImage()
		{
			switch (_detailObj.searchTargetType)
			{
				case SearchTargetType.Support:
					_label.sprite = _supporterSpr;
					break;
				case SearchTargetType.Destination:
					_label.sprite = _destinationSpr;
					break;
			}
		}
	}
}