﻿using UnityEngine;
using UnityEngine.UI;

public class RecommendItem : MonoBehaviour
{
	public Text txtTitle;
	public Image image;
	public Button button;
}