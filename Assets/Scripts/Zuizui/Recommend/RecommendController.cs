﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using static API.Request.SosMap.DestinationRequest;

public class RecommendController : MonoBehaviour
{
	[System.Serializable]
	public class CityInfo
	{
		public string id;
		public string name;
		public string lon;
		public string lat;

		public CityInfo() { }

		public CityInfo(string name, string lon, string lat)
		{
			this.name = name;
			this.lon = lon;
			this.lat = lat;
		}

		public CityInfo(string id, string name, string lon, string lat) : this(id, name, lon)
		{
			this.lat = lat;
		}
	}

	[SerializeField] Transform container;
	[SerializeField] RecommendItem itemPrefab;
	[Space]

	List<Province> cities = new List<Province>();
	List<RecommendItem> items = new List<RecommendItem>();

	void Awake()
	{
		AppSosDataService.OnRequestDestinationCompleted += OnRequestDestinationCompleted;
		AppSosDataService.OnLoadCacheDestinationCompleted += OnRequestDestinationCompleted;
	}

	void OnRequestDestinationCompleted(List<Destination> data)
	{
		ClearItems();
		cities.Clear();

		cities = GetCitiesAtDestination(data);

		CreateItems(cities);
	}

	List<Province> GetCitiesAtDestination(List<Destination> destinations)
	{
		List<Province> currentFilterCity = new List<Province>();

		for (int i = 0; i < destinations.Count; i++)
		{
			if (destinations[i].provinceId != null)
			{
				if (currentFilterCity.Any(x => x._id != destinations[i].provinceId._id))
				{
					currentFilterCity.Add(destinations[i].provinceId);
				}
			}
		}

		return currentFilterCity;
	}

	void CreateItems(List<Province> cities)
	{
		for (int i = 0; i < cities.Count; i++)
		{
			int index = i;

			RecommendItem item = Instantiate(itemPrefab, container, false);
			item.txtTitle.text = cities[i].name;

			item.button.onClick.AddListener(() =>
			{
				if (string.IsNullOrEmpty(cities[index].longtitude) || string.IsNullOrEmpty(cities[index].lat))
					return;

				SelectionMarkerController.instance.Hide();
				MapController.instance.SmoothMoveAndZoom(double.Parse(cities[index].longtitude), double.Parse(cities[index].lat), 10, 3);
			});

			items.Add(item);
		}
	}

	void ClearItems()
	{
		for (int i = 0; i < items.Count; i++)
		{
			Destroy(items[i].gameObject);
		}

		items.Clear();
	}

}