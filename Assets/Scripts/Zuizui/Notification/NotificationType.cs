﻿namespace App.Message
{
	public enum NotificationType
	{
		Normal,
		SOSAlert,
		SOSWarning,
	}
}