namespace App.Message
{
	public class NotificationManager
	{
		public static NotificationType GetNotificationType(string msg)
		{
			if (msg.Contains("SOS Alert"))
				return NotificationType.SOSAlert;
			if (msg.Contains("SOS Warning"))
				return NotificationType.SOSWarning;

			return NotificationType.Normal;
		}
	}
}