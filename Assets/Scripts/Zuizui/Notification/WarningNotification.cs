﻿using Newtonsoft.Json;

namespace App.Message
{
	public class WarningNotification : INotification
	{
		public WarningNotificationModel model;
		public WarningNotification(string msg)
		{
			model = JsonConvert.DeserializeObject<WarningNotificationModel>(msg);
		}
	}
}