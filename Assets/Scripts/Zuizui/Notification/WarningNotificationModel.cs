using API.Request.SosMap.Generated;
using static API.Request.SosMap.DestinationRequest;

namespace App.Message
{
	[System.Serializable]
	public class WarningNotificationModel : NotificationModelBase
	{
		public PostSosHelpRequest.ResponseObject data = new PostSosHelpRequest.ResponseObject();
	}
}