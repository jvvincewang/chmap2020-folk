﻿using App.UI;
using UnityEngine;
using UnityEngine.UI;

public class NeedSupportPopup : BaseUIPanel
{
	[Space]

	public Text txtTitle;
	public Text txtDes;
	public Text txtPhonenumber;
	[Space]
	public Button yesBtn;
	public Button noBtn;

	protected override void OnOpen() {}

	protected override void OnClose()
	{
		yesBtn.onClick.RemoveAllListeners();
		noBtn.onClick.RemoveAllListeners();
	}

	public void SetPhoneNumber(string phoneNumber)
	{
		if (!string.IsNullOrEmpty(phoneNumber))
		{
			txtPhonenumber.gameObject.SetActive(true);
			txtPhonenumber.text = phoneNumber;
		}
		else
			txtPhonenumber.gameObject.SetActive(false);
	}
}