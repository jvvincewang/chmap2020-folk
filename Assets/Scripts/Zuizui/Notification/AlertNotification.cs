using Newtonsoft.Json;

namespace App.Message
{
	public class AlertNotification : INotification
	{
		public AlertNotificationModel model;
		public AlertNotification(string msg)
		{
			model = JsonConvert.DeserializeObject<AlertNotificationModel>(msg);
		}
	}
}