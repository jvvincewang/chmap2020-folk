﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppBehaviors : MonoBehaviour
{
	public static event Action<bool> OnApplicationFocusChange;
	public static event Action<bool> OnApplicationPauseChange;

	void OnApplicationFocus(bool focusStatus)
	{
		OnApplicationFocusChange?.Invoke(focusStatus);
	}

	void OnApplicationPause(bool pauseStatus)
	{
		OnApplicationPauseChange?.Invoke(pauseStatus);

	}
}