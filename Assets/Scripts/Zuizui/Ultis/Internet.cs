﻿using UnityEngine;

public class Internet
{
	// flag for network status
	// bool isNetworkEnabled = false;

	// Check if the device can reach the internet
	public static bool IsAvailable()
	{
		if (Application.internetReachability != NetworkReachability.NotReachable)
			return true;
		return false;
	}

	// Check if the device can reach the internet via a carrier data network
	public static bool IsReachableViaCarrierDataNetwork()
	{
		if (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
			return true;
		return false;
	}

	// Check if the device can reach the internet via a LAN
	public static bool IsReachableViaLocalAreaNetwork()
	{
		if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork)
			return true;
		return false;
	}
}