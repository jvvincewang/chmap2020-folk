﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> where T : MonoBehaviour
{
	Transform transform;
	T prefab;
	List<T> freeList;
	List<T> usedList;

	public int Count { get => usedList.Count; }
	public T this[int index] => usedList[index];

	public void Init(T prefab, Transform transform, int prepareSpawnSize = 10)
	{
		freeList = new List<T>(prepareSpawnSize);
		usedList = new List<T>(prepareSpawnSize);

		this.prefab = prefab;
		this.transform = transform;
		for (var i = 0; i < prepareSpawnSize; i++)
		{
			CreateNewObjectPool(prefab, transform);
		}
	}

	public T Get()
	{
		var numFree = freeList.Count;
		if (numFree == 0)
		{
			CreateNewObjectPool(prefab, transform);
			return Get();
		}

		var pooledObject = freeList[numFree - 1];
		freeList.RemoveAt(numFree - 1);
		usedList.Add(pooledObject);
		return pooledObject;
	}

	public void ReturnObject(T pooledObject)
	{
		Debug.Assert(usedList.Contains(pooledObject));

		usedList.Remove(pooledObject);
		freeList.Add(pooledObject);

		var pooledObjectTransform = pooledObject.transform;
		pooledObjectTransform.parent = transform;
		pooledObjectTransform.localPosition = Vector3.zero;
		pooledObject.gameObject.SetActive(false);
	}

	public void ReturnAllObject()
	{
		for (int i = 0; i < usedList.Count; i++)
		{
			usedList[i].gameObject.SetActive(false);
			freeList.Add(usedList[i]);
		}

		usedList.Clear();
	}

	T CreateNewObjectPool(T prefab, Transform transform)
	{
		var pooledObject = GameObject.Instantiate(prefab, transform);
		pooledObject.gameObject.SetActive(false);
		freeList.Add(pooledObject);
		return pooledObject;
	}
}