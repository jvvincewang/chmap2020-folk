﻿using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

[InitializeOnLoadAttribute]
public static class HandleAddComponentEditor
{

    static HandleAddComponentEditor()
    {
        EditorApplication.hierarchyChanged -= MyHierarchyChangedCallback;
        EditorApplication.hierarchyChanged += MyHierarchyChangedCallback;
    }

    private static void MyHierarchyChangedCallback()
    {
        GameObject current = Selection.activeGameObject;

        if (current == null)
            return;
        // if (Selection.gameObjects.Length > 0)
        // {
        // GameObject current = Selection.gameObjects[0];

        MonoBehaviour[] monos = current.GetComponents<MonoBehaviour>();

        if (monos.Length > 0 && (current.name.Equals("GameObject") || current.name.Equals("GameObject (1)") || current.name.Equals("GameObject (2)")))
        {
            MonoBehaviour mono = monos[0];

            if (mono == null)
                return;

            string newName = FormatName(mono.GetType().Name);
            current.name = newName;
        }
        // }
    }

    static string FormatName(string name)
    {
        return Regex.Replace(name, @"((?<=\p{Ll})\p{Lu})|((?!\A)\p{Lu}(?>\p{Ll}))", " $0");
    }
}
