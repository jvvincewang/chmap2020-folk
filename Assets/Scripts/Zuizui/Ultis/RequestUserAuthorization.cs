﻿using System.Collections;
using UnityEngine;
using UnityEngine.Android;

public class RequestUserAuthorization : MonoBehaviour
{
	[System.Flags]
	public enum PermissionRequest
	{
		None,
		Camera = 1,
		Microphone = 2,
		FineLocation = 4,
		CoarseLocation = 8,
		ExternalStorageRead = 16,
		ExternalStorageWrite = 32,
	}

	[Header("Permission Request")]
	// public PermissionRequest permissionRequest = ~PermissionRequest.None;
	public PermissionRequest permissionRequest = PermissionRequest.None;

	IEnumerator Start()
	{
#if UNITY_ANDROID

		if (permissionRequest.HasFlag(PermissionRequest.Camera))
			if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
			{
				Permission.RequestUserPermission(Permission.Camera);
			}

		if (permissionRequest.HasFlag(PermissionRequest.Microphone))
			if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
			{
				Permission.RequestUserPermission(Permission.Microphone);
			}

		if (permissionRequest.HasFlag(PermissionRequest.FineLocation))
			if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
			{
				Permission.RequestUserPermission(Permission.FineLocation);
			}

		if (permissionRequest.HasFlag(PermissionRequest.CoarseLocation))
			if (!Permission.HasUserAuthorizedPermission(Permission.CoarseLocation))
			{
				Permission.RequestUserPermission(Permission.CoarseLocation);
			}

		if (permissionRequest.HasFlag(PermissionRequest.ExternalStorageRead))
			if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
			{
				Permission.RequestUserPermission(Permission.ExternalStorageRead);
			}

		if (permissionRequest.HasFlag(PermissionRequest.ExternalStorageWrite))
			if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
			{
				Permission.RequestUserPermission(Permission.ExternalStorageWrite);
			}

#elif UNITY_IOS
		if (permissionRequest.HasFlag(PermissionRequest.Camera))
			yield return IERequestUserAuthorization(UserAuthorization.WebCam);

		if (permissionRequest.HasFlag(PermissionRequest.Microphone))
			yield return IERequestUserAuthorization(UserAuthorization.Microphone);
#endif
		yield return null;
	}

	IEnumerator IERequestUserAuthorization(UserAuthorization userAuthorization)
	{
		yield return Application.RequestUserAuthorization(userAuthorization);

		if (Application.HasUserAuthorization(userAuthorization))
		{
			Debug.Log($"{userAuthorization} authorization is success!");
		}
		else
		{
			Debug.Log($"This {userAuthorization} Library can't work without the {userAuthorization} authorization");
			yield break;
		}
	}
}