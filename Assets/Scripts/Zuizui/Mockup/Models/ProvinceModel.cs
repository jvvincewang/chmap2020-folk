﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Mockup.Models
{
	[System.Serializable]
	public partial class ProvinceModel : ApiResultModel
	{
		[JsonProperty("results")]
		public Result[] Results { get; set; }

		[Serializable]
		public class Result
		{
			[JsonProperty("id")]
			public long Id { get; set; }

			[JsonProperty("name")]
			public string Name { get; set; }
		}
	}

	public partial class ProvinceModel
	{
		public static ProvinceModel FromJson(string json) => JsonConvert.DeserializeObject<ProvinceModel>(json);
	}

}