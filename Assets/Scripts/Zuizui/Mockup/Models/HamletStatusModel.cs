﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;

namespace Mockup.Models
{
	[System.Serializable]
	public class HamletStatusModel : ApiResultModel
	{
		[JsonProperty("results")]
		public Result[] Results { get; set; }

		[System.Serializable]
		public class Result
		{
			[JsonProperty("id")]
			public long Id { get; set; }

			[JsonProperty("name")]
			public string Name { get; set; }

			[JsonProperty("created_time")]
			public DateTimeOffset CreatedTime { get; set; }

			[JsonProperty("update_time")]
			public DateTimeOffset UpdateTime { get; set; }
		}
		public static HamletStatusModel FromJson(string json) => JsonConvert.DeserializeObject<HamletStatusModel>(json);
	}
}