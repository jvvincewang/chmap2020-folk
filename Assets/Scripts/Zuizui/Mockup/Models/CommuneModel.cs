﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Mockup.Models
{
	[System.Serializable]
	public partial class CommuneModel : ApiResultModel
	{
		[JsonProperty("results")]
		public Result[] Results { get; set; }

		[Serializable]
		public class Result
		{
			[JsonProperty("id")]
			public long Id { get; set; }

			[JsonProperty("name")]
			public string Name { get; set; }

			[JsonProperty("huyen")]
			public long? Huyen { get; set; }
		}
	}

	public partial class CommuneModel
	{
		public static CommuneModel FromJson(string json) => JsonConvert.DeserializeObject<CommuneModel>(json);
	}
}