﻿using System;
using Newtonsoft.Json;
namespace Mockup.Models
{
	[System.Serializable]
	public partial class VolunteersModel : ApiResultModel
	{
		[JsonProperty("results")]
		public Result[] Results { get; set; }

		[Serializable]
		public class Result
		{
			[JsonProperty("id")]
			public long Id { get; set; }

			[JsonProperty("tinh")]
			public long? Tinh { get; set; }

			[JsonProperty("tinh_display")]
			public string TinhDisplay { get; set; }

			[JsonProperty("huyen")]
			public long? Huyen { get; set; }

			[JsonProperty("huyen_display")]
			public string HuyenDisplay { get; set; }

			[JsonProperty("xa")]
			public long? Xa { get; set; }

			[JsonProperty("xa_display")]
			public string XaDisplay { get; set; }

			[JsonProperty("name")]
			public string Name { get; set; }

			[JsonProperty("status")]
			public long Status { get; set; }

			[JsonProperty("location")]
			public string Location { get; set; }

			[JsonProperty("phone")]
			public string Phone { get; set; }

			[JsonProperty("note")]
			public string Note { get; set; }

			[JsonProperty("thon")]
			public long? Thon { get; set; }
		}
	}

	public partial class VolunteersModel
	{
		public static VolunteersModel FromJson(string json) => JsonConvert.DeserializeObject<VolunteersModel>(json);
	}
}