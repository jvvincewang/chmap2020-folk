﻿using System;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Mockup.Models
{
	[System.Serializable]
	public partial class HamletModel : ApiResultModel
	{
		[JsonProperty("results")]
		public Result[] Results { get; set; }

		[Serializable]
		public class Result
		{
			[JsonProperty("id")]
			public long Id { get; set; }

			[JsonProperty("tinh")]
			public long Tinh { get; set; }

			[JsonProperty("tinh_display")]
			public string TinhDisplay { get; set; }

			[JsonProperty("huyen")]
			public long? Huyen { get; set; }

			[JsonProperty("huyen_display")]
			public string HuyenDisplay { get; set; }

			[JsonProperty("xa")]
			public long? Xa { get; set; }

			[JsonProperty("xa_display")]
			public string XaDisplay { get; set; }

			[JsonProperty("status")]
			public int Status { get; set; }

			[JsonProperty("status_display")]
			public string StatusDisplay { get; set; }

			[JsonProperty("geo_longtitude")]
			public object GeoLongtitude { get; set; }

			[JsonProperty("geo_latitude")]
			public object GeoLatitude { get; set; }

			[JsonProperty("update_time_timestamp")]
			public long UpdateTimeTimestamp { get; set; }

			[JsonProperty("update_time")]
			public string UpdateTime { get; set; }

			[JsonProperty("name")]
			public string Name { get; set; }

			[JsonProperty("location")]
			public string Location { get; set; }

			[JsonProperty("phone")]
			public string Phone { get; set; }

			[JsonProperty("note")]
			public string Note { get; set; }

			[JsonProperty("thon")]
			public string Thon { get; set; }

			[JsonProperty("volunteer")]
			public long? Volunteer { get; set; }
		}
	}

	public partial class HamletModel
	{
		public static HamletModel FromJson(string json) => JsonConvert.DeserializeObject<HamletModel>(json);
	}
}