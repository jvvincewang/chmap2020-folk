﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

namespace Mockup.Models
{
	public partial class DistrictModel : ApiResultModel
	{
		[JsonProperty("results")]
		public Result[] Results { get; set; }
	}

	public partial class Result
	{
		[JsonProperty("id")]
		public long Id { get; set; }

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("tinh")]
		public long? Tinh { get; set; }
	}

	public partial class DistrictModel
	{
		public static DistrictModel FromJson(string json) => JsonConvert.DeserializeObject<DistrictModel>(json);
	}
}