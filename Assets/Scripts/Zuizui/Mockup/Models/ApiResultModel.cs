﻿using Newtonsoft.Json;
using UnityEngine;

namespace Mockup.Models
{
	[System.Serializable]
	public class ApiResultModel
	{
		[JsonProperty("count")]
		public int count;
		[JsonProperty("next")]
		public string next;
		[JsonProperty("previous")]
		public string previous;
	}
}