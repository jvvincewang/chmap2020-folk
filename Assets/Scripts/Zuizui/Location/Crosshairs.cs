﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class State
{
	public string name;
	public Sprite sprite;
	public Color color = Color.black;

	// public abstract void EnterState();

	// public abstract void ExitState();
}

[System.Serializable] public class OnState : State { }

[System.Serializable] public class OffState : State { }

[System.Serializable] public class ScanningState : State { }

[System.Serializable] public class QuestionState : State { }

public class Crosshairs : MonoBehaviour
{

	public Image target;
	public GameObject waitingAnimation;
	[Space]
	public State currentState;
	[Space]

	[SerializeField] OnState onState;
	[SerializeField] OffState offState;
	[SerializeField] ScanningState scanningState;
	[SerializeField] QuestionState questionState;

	public void SetOnState()
	{
		TransitionToState(onState);
	}

	public void SetOffState()
	{
		TransitionToState(offState);
	}

	// public void OnAvailabe()
	// {
	// 	TransitionToState(scanningState);
	// }

	public void SetEnableScanningState()
	{
		TransitionToState(scanningState);

		target.gameObject.SetActive(false);
		waitingAnimation.SetActive(true);
	}

	public void SetDisableScanningState()
	{
		TransitionToState(onState);

		target.gameObject.SetActive(true);
		waitingAnimation.SetActive(false);
	}

	public void TransitionToState(State state)
	{
		// currentState.ExitState();
		currentState = state;
		// currentState.EnterState();
		ApplyRenderState();
	}

	void ApplyRenderState()
	{
		if (currentState.sprite != null)
			target.sprite = currentState.sprite;

		target.color = currentState.color;
	}
}