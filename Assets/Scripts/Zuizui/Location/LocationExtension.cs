﻿using System;
using App.Services;
using UnityEngine;

namespace App.Extension
{
	public static class LocationExt
	{

		public static int ExtractDegrees(this double value)
		{
			return (int) value;
		}

		public static int ExtractMinutes(this double value)
		{
			value = Math.Abs(value);
			return (int) (value - value.ExtractDegrees() * 60);
		}

		public static int ExtractSeconds(this double value)
		{
			value = Math.Abs(value);
			double minutes = (value - ExtractDegrees(value)) * 60;
			return (int) Math.Round(minutes - value.ExtractMinutes() * 60);
		}

		public static Vector2 ToVector2(this Location location)
		{
			return new Vector2((float) location.longitude, (float) location.latitude);
		}

		public static Location ToLocation(this DmsLocation dmsLocation)
		{
			if (dmsLocation == null)
			{
				return null;
			}

			return new Location
			{
				latitude = (double) CalculateDecimal(dmsLocation.latitude),
					longitude = (double) CalculateDecimal(dmsLocation.longitude)
			};
		}

		// Ex: 40.7600000,-73.9840000 => 40° 45' 36.000" N,73° 59' 2.400" W 
		public static DmsLocation ToDmsLocation(this Location location)
		{
			if (location == null)
			{
				return null;
			}

			return new DmsLocation
			{
				latitude = new DmsPoint
					{
						Degrees = location.latitude.ExtractDegrees(),
							Minutes = location.latitude.ExtractMinutes(),
							Seconds = location.latitude.ExtractSeconds(),
							Type = PointType.Lat
					},
					longitude = new DmsPoint
					{
						Degrees = location.longitude.ExtractDegrees(),
							Minutes = location.longitude.ExtractMinutes(),
							Seconds = location.longitude.ExtractSeconds(),
							Type = PointType.Lon
					}
			};
		}

		// return: Distance (meter)
		public static float GetDistance(Location p1, Location p2)
		{
			float distanceInKm = OnlineMapsUtils.DistanceBetweenPoints(p1.ToVector2(), p2.ToVector2()).magnitude;
			float distanceInMeter = distanceInKm * 1000f;
			return distanceInMeter;
		}

		static decimal CalculateDecimal(DmsPoint point)
		{
			if (point == null)
			{
				return default(decimal);
			}
			return point.Degrees + (decimal) point.Minutes / 60 + (decimal) point.Seconds / 3600;
		}
	}
}