﻿using UnityEngine;

namespace App.Services
{
	[System.Serializable]
	public class Location
	{
		public double longitude { get; set; }
		public double latitude { get; set; }

		public Location() { }

		public Location(double longitude, double latitude)
		{
			this.longitude = longitude;
			this.latitude = latitude;
		}

		public override string ToString()
		{
			return string.Format("{0:f5}, {1:f5}",
				longitude, latitude);
		}

		public Vector2 ToVector2()
		{
			return new Vector2((float) longitude, (float) latitude);
		}
	}
}