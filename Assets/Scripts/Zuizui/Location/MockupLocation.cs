﻿using App.Services;

namespace Mockup
{
	public class City
	{
		public string name;
		public double longitude;
		public double latitude;

		public City() { }

		public City(string name, double longitude, double latitude)
		{
			this.name = name;
			this.longitude = longitude;
			this.latitude = latitude;
		}

		public Location GetLocation()
		{
			return new Location(longitude, latitude);
		}
	}

	public class Locations
	{
		public static City[] cities = new City[10]
		{
			new City("San Francisco", -122.5076401, 37.7576793),
				new City("Da Nang", 108.205444, 16.062690),
				new City("Ha Noi", 105.84117, 21.0245),
				new City("Ho Chi Minh", 106.77209, 10.84863),
				new City("Beijing", 116.39723, 39.9075),
				new City("Havana", -82.38304, 23.13302),
				new City("Paris", 2.2770202, 48.8588377),
				new City("New York", -74.2598661, 40.6971494),
				new City("Havana", 23.050625, -82.4730881),
				new City("Argentina", -81.6356, -37.0555503)
		};
	}
}