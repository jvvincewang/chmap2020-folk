﻿using System;
using System.Collections;
using App.Extension;
using App.Services;
using UnityEngine;

public class GPS : MonoBehaviourSingletonBase<GPS>
{
	double _longitude;
	public double longitude => _longitude;

	double _latitude;
	public double latitude => _latitude;

	bool isProcessing;
	bool _isUnknown = true;

	public bool isUnknown => _isUnknown;
	public bool isEnabledByUser => Input.location.isEnabledByUser;
	Coroutine getLocationCoroutine = null;

	WaitForSeconds waitOneSecond = new WaitForSeconds(1f);

	public void GetLocation(Action<Location> onFinishGetLocation, Action onDisableByUser = null, Action onTimeout = null, int timeout = 20)
	{
		// isProcessing To Running
		if (!isProcessing)
		{
			getLocationCoroutine = StartCoroutine(IEGetLocation(onFinishGetLocation, onDisableByUser, onTimeout, timeout));
		}
		else
		{
			Debug.Log("GPS is processing");
		}
	}

	public IEnumerator IEGetLocation(Action<Location> onFinishGetLocation, Action onDisableByUser = null, Action onTimeout = null, int timeout = 20)
	{
		isProcessing = true;

		// First, check if user has location service enabled
		if (!Input.location.isEnabledByUser)
		{
			//Debug.Log("User has not enable GPS");
			onDisableByUser?.Invoke();
			isProcessing = false;
			yield break;
		}

		// Start service before querying location
		if (Input.location.status == LocationServiceStatus.Stopped)
			Input.location.Start();

		// Wait until service initializes
		while (Input.location.status == LocationServiceStatus.Initializing && timeout > 0)
		{
			yield return waitOneSecond;
			timeout--;
		}

		// Service didn't initialize in 10 seconds
		if (timeout < 1)
		{
			Debug.Log("Timed out");
			onTimeout?.Invoke();
			isProcessing = false;
			yield break;
		}

		// Connection has failed
		if (Input.location.status == LocationServiceStatus.Failed)
		{
			Debug.Log("Unable to determine device location");
			isProcessing = false;
			yield break;
		}
		else
		{
			// Access granted and location value could be retrieved
			Debug.Log("[GPS]: [lat] =" + Input.location.lastData.latitude + ", [lon] =" + Input.location.lastData.longitude + ", [altitude] =" + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + ", [timestamp] " + Input.location.lastData.timestamp);

			_longitude = Input.location.lastData.longitude;
			_latitude = Input.location.lastData.latitude;

			onFinishGetLocation?.Invoke(new Location(longitude, latitude));
			isProcessing = false;
			_isUnknown = false;
		}

		isProcessing = false;
	}

	public string ConvertToDmsLocation()
	{
		if (!isUnknown)
			return new Location(longitude, latitude).ToDmsLocation().ToString();

		return "null,null";
	}

	public void Start()
	{
		Input.location.Start();
	}

	public void Stop()
	{
		Input.location.Stop();
	}

}