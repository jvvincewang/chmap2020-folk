﻿using System;
using System.Collections;
using App;
using App.Services;
using API;
using API.Request.SosMap.Generated;
using UnityEngine;

public class MyLocation : MonoBehaviourSingletonBase<MyLocation>
{
	[Header("References")]
	public Crosshairs crosshairs;

	[Header("Config")]
	public bool isAutoLocationSync = false;
	public float timeInterval = 1f;
	[Space]
	public string label = "Me";
	[SerializeField] Texture2D markerTex;
	[HideInInspector] public Location lastLocation;
	OnlineMapsMarker marker = null;

	void Start()
	{
		CheckAvailabeStatus();

		if (isAutoLocationSync)
			StartCoroutine(IEAutoLocationSync());
	}

	IEnumerator IEAutoLocationSync()
	{
		while (true)
		{
			if (lastLocation != null)
			{
				//Debug.Log("IEGetLocation waiting");
				yield return GPS.instance.IEGetLocation((loc) =>
				{
					OnFinishGetLocation(loc);
				});

				//Debug.Log("IEGetLocation finish");
			}
			yield return new WaitForSeconds(timeInterval);
		}
	}

	void CheckAvailabeStatus()
	{
		if (GPS.instance.isEnabledByUser)
		{
			crosshairs.SetOnState();
			GPS.instance.IEGetLocation((loc) =>
			{
				OnFinishGetLocation(loc);
			});
		}
		else
		{
			crosshairs.SetOffState();
		}
	}

	public void OnClickShowCurrentLocation()
	{
#if UNITY_EDITOR
		string label = Mockup.Locations.cities[1].name;
		Location location = Mockup.Locations.cities[1].GetLocation();
		OnFinishGetLocation(location);
#else
		GetCurrentLocation(location =>
		{
			OnFinishGetLocation(location);
		});
#endif
	}

	public void OnFinishGetLocation(Location location)
	{
		SetPosition(location.longitude, location.latitude);

		MapController.instance.SmoothMoveAndZoom(location.longitude, location.latitude, 16);

		var postData = new PutLocationRequest.PostObject()
		{
			location = new LocationObject()
			{
				type = "Point",
					coordinates = new float[2]
					{
						(float) location.longitude, (float) location.latitude,
					}
			},
		};

		AppAPIManager.instance.PutLocationInfo(postData, () =>
		{
			Debug.Log("Success Update user info");
		});
	}

	public void GetCurrentLocation(Action<Location> onFinishGetLocation)
	{
		crosshairs.SetEnableScanningState();
		GPS.instance.GetLocation((location) =>
		{
			onFinishGetLocation?.Invoke(location);
			crosshairs.SetDisableScanningState();
		}, () =>
		{
			crosshairs.SetDisableScanningState();
			crosshairs.SetOffState();
		}, () =>
		{
			crosshairs.SetDisableScanningState();
		});
	}

	public void SetPosition(double longitude, double latitude)
	{
		lastLocation = new Location(longitude, latitude);

		if (marker == null)
		{
			marker = OnlineMapsMarkerManager.CreateItem(longitude, latitude, markerTex, label);
		}
		else
		{
			marker.SetPosition(longitude, latitude);
		}
	}
}