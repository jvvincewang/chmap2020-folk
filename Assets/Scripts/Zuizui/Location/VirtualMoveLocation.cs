﻿using System.Collections;
using System.Collections.Generic;
using App.Services;
using UnityEngine;

public class VirtualMoveLocation : MonoBehaviour
{
	public float value = 0.1f;
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			MoveLeft();
		}

		if (Input.GetKeyDown(KeyCode.RightArrow))
		{

			MoveRight();
		}

		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			MoveUp();
		}

		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			MoveDown();
		}
	}

	public void MoveUp()
	{
		MoveTo(new Vector2(0, value));
	}

	public void MoveDown()
	{
		MoveTo(new Vector2(0, -value));
	}

	public void MoveLeft()
	{
		MoveTo(new Vector2(-value, 0));
	}

	public void MoveRight()
	{
		MoveTo(new Vector2(value, 0));
	}

	public void MoveTo(Vector2 pos)
	{
		MyLocation.instance.SetPosition(MyLocation.instance.lastLocation.longitude + pos.x, MyLocation.instance.lastLocation.latitude + pos.y);
		MapController.instance.SmoothMoveAndZoom(MyLocation.instance.lastLocation.longitude, MyLocation.instance.lastLocation.latitude, 16);
	}
}