﻿using UnityEngine;

public class PhoneCall
{
	public string phoneNumber;

	public PhoneCall(string phoneNumber) { this.phoneNumber = phoneNumber; }

	public void CallTo(string phoneNumber)
	{
		Application.OpenURL("tel:" + phoneNumber);
	}

	public void Call()
	{
		if (string.IsNullOrEmpty(phoneNumber))
		{
			Debug.LogError("phoneNumber not installed");
			return;
		}

		CallTo(phoneNumber);
	}
}