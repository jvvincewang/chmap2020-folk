﻿using System.Collections;
using System.Collections.Generic;
using App.UI;
using UnityEngine;
using UnityEngine.UI;

public class ContactScreen : BaseUIPanel
{
	public Text txtUsername;
	public Text txtPhonenumber;

	[Space]
	[SerializeField] Button iconBtn;

	public void SetActiveIcon(bool active)
	{
		iconBtn?.gameObject.SetActive(active);
	}

	protected override void OnClose() {}

	protected override void OnOpen() {}

	public void SetPhoneNumber(string phoneNumber)
	{
		if (!string.IsNullOrEmpty(phoneNumber))
		{
			txtPhonenumber.gameObject.SetActive(true);
			txtPhonenumber.text = phoneNumber;
		}
		else
			txtPhonenumber.gameObject.SetActive(false);
	}
}