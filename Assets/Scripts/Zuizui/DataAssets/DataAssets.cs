﻿using UnityEngine;

namespace Database
{
	public static class DataAssets
	{
		public static ImageAssets Image => Resources.Load<ImageAssets>("DataAssets/ImageAssets");
		public static ListScriptableObject SupportType => Resources.Load<ListScriptableObject>("Configs/SupportType");
		public static ListScriptableObject DestinationLevel => Resources.Load<ListScriptableObject>("Configs/DestinationLevel");
	}
}