﻿using UnityEngine;

namespace Database
{
	[System.Serializable]
	public class ImageAssetsDefine
	{
		public string name;
		public string description;
		public Sprite icon;
	}

	[CreateAssetMenu(fileName = "ImageAssets", menuName = "Database/ImageAssets", order = 0)]
	public class ImageAssets : ScriptableObject
	{
		public ImageAssetsDefine label;
		public ImageAssetsDefine phone;
		public ImageAssetsDefine note;
		public ImageAssetsDefine status;
		public ImageAssetsDefine province;
		public ImageAssetsDefine address;
		public ImageAssetsDefine updateTime;
		public ImageAssetsDefine provide;
		public ImageAssetsDefine vehicle;
		public ImageAssetsDefine directions;
		public ImageAssetsDefine issue;
		public ImageAssetsDefine level;
		public ImageAssetsDefine type;
		public ImageAssetsDefine place;
		public ImageAssetsDefine destination;
	}
}