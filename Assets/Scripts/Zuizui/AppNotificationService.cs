﻿using App;
using App.Message;
using API;
using API.Request.SosMap.Generated;
using Firebase.Messaging;
using Gravitons.UI.Modal;
using NaughtyAttributes;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
#if UNITY_ANDROID
using Unity.Notifications.Android;
#endif
#if UNITY_IOS
using Unity.Notifications.iOS;
#endif

public class AppNotificationService : MonoBehaviour
{
	[SerializeField] SOSPanelV2 sosPanel;
	[SerializeField] HelperPanel helperPanel;
	[SerializeField] SosNotificationHandler sosMessageHandler;

	SosObject _sosData;
	bool _isModelOpened = false;

	void Awake()
	{
		AppFirebaseCloudMessManager.MessageReceived += OnMessageReceived;
		// AppFirebaseCloudMessManager.TokenReceived += OnTokenReceived;
	}

    private void Start()
    {
		//Check and show SOS hepler at launch
        if(AppFirebaseCloudMessManager.instance.ShowSOSAtLaunch)
        {
			_sosData = JsonConvert.DeserializeObject<SosObject>(AppFirebaseCloudMessManager.instance.SOSData);

			_isModelOpened = true;
			if(_sosData.CreatedUserId != AppSaveData.Data.userInfo.userInAppId)
            {
				ShowNotificationPopup();
			}				
			AppFirebaseCloudMessManager.instance.ShowSOSAtLaunch = false;
		}			
    }

    void OnMessageReceived(object sender, MessageReceivedEventArgs e)
	{
		string msg = "";
		var data = e.Message.Data;
		if (data.Count > 0)
		{
			foreach (var item in data)
			{
				msg += $"[{item.Key}]: " + item.Value + System.Environment.NewLine;
			}
		}

		Debug.Log("OnMessageReceived:" + System.Environment.NewLine + msg);
		Debug.Log("PN type:" + System.Environment.NewLine + e.Message.MessageType);
		Debug.Log("PN title:" + System.Environment.NewLine + e.Message.Notification.Title);
		Debug.Log("PN Body:" + System.Environment.NewLine + e.Message.Notification.Body);
		Debug.Log("PN Data:" + System.Environment.NewLine + e.Message.Data);

		// ModalManager.Show("FCM", msg);
		if (_isModelOpened)
        {
			//Block spam model
			return;
        }			
		if (data.ContainsKey("data"))
		{
			var sosJson = data["data"];

			_sosData = JsonConvert.DeserializeObject<SosObject>(sosJson);

            if (!string.IsNullOrEmpty(msg) && _sosData.CreatedUserId != AppSaveData.Data.userInfo.userInAppId)
            {
                var msgType = NotificationManager.GetNotificationType(msg);
				//TODO:
				//HandleNotificationType(msgType, msg);
				_isModelOpened = true;
				Debug.Log(msgType.ToString());
				switch (msgType)
                {
					case NotificationType.SOSWarning:
					case NotificationType.SOSAlert:
					case NotificationType.Normal: 
						{
							ShowNotificationPopup();
						} break;
                }					
            }				

        }
    }

	void ShowNotificationPopup()
    {
		ModalManager.Show(_sosData.Name,
								"Có người cần giúp đỡ!",
								"Giúp đỡ ngay", () => {
									sosPanel.Close();
									helperPanel.Open();
									helperPanel.AddListener(null, () => { _isModelOpened = false; sosPanel.Open(); });
									helperPanel.UpdateInfo(_sosData);
								},
								"Từ chối", null);
	}		
		
	void HandleNotificationType(NotificationType type, string msg)
	{
		switch (type)
		{
			case NotificationType.SOSAlert: // SOS //	title: 'SOS Alert', body: 'One supporter is going',
				sosMessageHandler.ReceivedAlertNotification(new AlertNotification(msg));
				break;
			case NotificationType.SOSWarning: // SOS //	title: 'SOS Warning', body: 'Please help me',
				sosMessageHandler.ReceivedNotificationFromDestination(new WarningNotification(msg));
				break;
			default:
				//TODO:
				break;
		}
	}

	void OnDestroy()
	{
		AppFirebaseCloudMessManager.MessageReceived -= OnMessageReceived;
		// AppFirebaseCloudMessManager.TokenReceived -= OnTokenReceived
	}

	[Space]
	[SerializeField] string chanelId;

	[Button]
	void CreateHelperSosChanel()
	{
		var chanel = SosChannel.CreateChannel<HelperSosChannel>(chanelId);
		chanel.sosData = new SosObject();
		chanel.sosData.Location = new LocationObject();
		chanel.sosData.Location.coordinates = new float[2] { 0, 0 };
	}

	[Button]
	void PostSosesMetRequest()
	{
		var req = AppAPIManager.instance.CreateRequest<PostSosesMetRequest>(UnityWebRequest.kHttpVerbPOST, (object) chanelId);
		req.Send(res =>
		{
			Debug.Log("PostSosesMetRequest Success");

		}, (arg0, err) =>
		{
			Debug.LogError($"<color=red>Error PostSosesMetRequest: {err}</color>");
		});
	}

	[Button]
	void ShowDemoMsg()
	{
		ModalManager.Show("Bạn đã nhận 1 tin nhắn", "Demo");
	}

}