﻿using System;
using System.Collections.Generic;
using App;
using App.AppData;
using API;
using BestHTTP.SocketIO;
using BestHTTP.SocketIO.Events;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Events;

public abstract class SosChannel : MonoBehaviour
{
	SocketManager Manager;
	Socket socket;
	public UnityAction onConnected;
	public UnityAction onDisconected;
	public UnityAction onError;
	public UnityAction onOtherDeviceConnected;
	public UnityAction onMeet;
	public UnityAction OnRefuseSOS;
	public bool IsOpen => socket.IsOpen;
	public string channelId;

	public static T CreateChannel<T>(string channelId) where T : SosChannel
	{
		var channelBehaviour = new GameObject(typeof(T).Name);
		var channel = channelBehaviour.AddComponent<T>();
		channel.channelId = channelId;
		return channel;
	}

	protected virtual void Start()
	{
		CreateConnection();
	}

	void CreateConnection()
	{
		string socketIoDomain = NetworkingConfig.SocketIoDomain;

		SocketOptions options = new SocketOptions();
		options.ConnectWith = BestHTTP.SocketIO.Transports.TransportTypes.WebSocket;
		options.AdditionalQueryParams = new PlatformSupport.Collections.ObjectModel.ObservableDictionary<string, string>();
		options.AdditionalQueryParams.Add("channelId", channelId);
		options.AdditionalQueryParams.Add("token", AppRuntimeData.Data.token);

		Manager = new SocketManager(new Uri(socketIoDomain), options);

		socket = Manager.Socket;

		Manager.Socket.On(SocketIOEventTypes.Connect, (s, p, a) =>
		{
			Debug.Log($"Connected to socket: {channelId}!");
			onConnected?.Invoke();
		});

		socket.On(SocketIOEventTypes.Disconnect, (s, p, a) =>
		{
			Debug.Log("Disconnected!");
			onDisconected?.Invoke();
		});

		// The argument will be an Error object.
		socket.On(SocketIOEventTypes.Error, (socket, packet, args) =>
		{
			Debug.LogError(string.Format("Error: {0}", args[0].ToString()));
		});

		OnOpen();
	}

	protected abstract void OnOpen();
	protected abstract void OnClose();

	public void Subscribe(string channelId, SocketIOCallback socketIOCallback)
	{
		this.channelId = channelId;
		socket.On(channelId, socketIOCallback);
	}

	public void Emit(object data)
	{
		socket.Emit(channelId, data);
	}

	public void Off()
	{
		socket.Off();
	}

	public void Off(string channelId)
	{
		socket.Off(channelId);
	}

	public void Close()
	{
		socket.Disconnect();
		Manager.Close();
		OnClose();
		Destroy(this.gameObject);
	}
}