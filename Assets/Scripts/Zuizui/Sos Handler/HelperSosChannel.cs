﻿using System.Collections;
using System.Collections.Generic;
using App.Extension;
using App.Services;
using API;
using API.Request.SosMap.Generated;
using BestHTTP.SocketIO;
using Gravitons.UI.Modal;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;

public class HelperSosChannel : SosChannel
{
	float intervalForUpdateInSecs = 1;
	public SosObject sosData;
	OnlineMapsMarker targetMarker = null;
	Texture2D markerTexture;
	API.APIData.CommonUserExtendInfo _user = new API.APIData.CommonUserExtendInfo();

	protected override void OnOpen()
	{
		markerTexture = Database.DataAssets.Image.place.icon.texture;
		Subscribe(channelId, OnReceivedMessage);
		SyncLocation();

		onConnected += () =>
		{
			//emit first message include user info
			EmitToChannel("Update", MyLocation.instance.lastLocation);
		};
	}

	protected override void OnClose() {}

	void OnReceivedMessage(Socket socket, Packet packet, object[] args)
	{
		var arg = args[0] as Dictionary<string, object>;
		var stringData = arg["message"] as string;

		var data = JsonConvert.DeserializeObject<SocketModel>(stringData);

		var title = data.title;
		var location = data.location;

		if(data.title == "Refuse")
        {
			OnRefuseSOS?.Invoke();
			Close();
		}			

		Debug.Log(string.Format("Received Message {0}: {1}", title, location));
	}

	void SyncLocation()
	{
		StartCoroutine(IESyncLocation(intervalForUpdateInSecs));
	}

	IEnumerator IESyncLocation(float timeInterval)
	{
		yield return new WaitUntil(() => IsOpen);

		while (IsOpen)
		{
			Location me = MyLocation.instance.lastLocation;
			Location target = new Location((double) sosData.Location.coordinates[0], (double) sosData.Location.coordinates[1]);

			if (me == null || target == null)
			{
				yield return new WaitForSeconds(timeInterval);
				Debug.LogError("Location cannot be null");
			}
			else
			{
				if (targetMarker == null)
				{
					targetMarker = OnlineMapsMarkerManager.CreateItem(target.longitude, target.latitude, markerTexture, "");
				}
				else
				{
					targetMarker.SetPosition(target.longitude, target.latitude);
				}

				//DirectionsPanel.Instance.DrawRouteMap(me, target);

				// Matching two location
				if (IsInsideVisibleRange(me, target, 100))
				{
					OnDisconnect(me);

					Close();

					yield break;
				}
				else
				{
					EmitToChannel("Moving", me);

					yield return new WaitForSeconds(timeInterval);
				}
			}
		}
	}

	void OnDisconnect(Location me)
	{
		SendAPIMet();

		EmitToChannel("Met", me);

		//DONE:  4.Hiện thông báo matching ok lên ở cả hai app, ấn ok thì về màn hình home.
		//ModalManager.Show("Thông báo", "Đã đến vị trí cần đến", "Ok", () =>
		//{
		//	DirectionsPanel.Instance.Close();
		//});
		onMeet?.Invoke();

		OnlineMapsMarkerManager.RemoveItem(targetMarker);

		//SosNotificationHandler.instance.contactScreen.SetActiveIcon(false);
	}

	void EmitToChannel(string title, Location me)
	{
		var socketData = new SocketModel()
		{
			title = title,
			location = me,
			user = _user
		};

		var data = JsonConvert.SerializeObject(socketData);

		Debug.Log($"Emit: {title} - {data}");

		Emit(data);
	}

	bool IsInsideVisibleRange(Location loc1, Location loc2, float visibleDistanceInMeter)
	{
		float dis = LocationExt.GetDistance(loc1, loc2);

		Debug.Log($"Distance: {dis}");

		if (dis <= visibleDistanceInMeter)
		{
			return true;
		}

		return false;
	}

	public void SendAPIMet()
	{
		//TODO: "soses/{0}/met"
		var req = AppAPIManager.instance.CreateRequest<PostSosesMetRequest>(UnityWebRequest.kHttpVerbPOST, (object) channelId);
		req.Send(res =>
		{
			Debug.Log("PostSosesMetRequest Success");
		}, (arg0, err) =>
		{
			Debug.LogError($"<color=red>Error PostSosesMetRequest: {err}</color>");
		});
	}

	public void InitInfo(string UserName, string PhoneNumber, string Note)
    {
		_user.fullName = UserName;
		_user.phone = PhoneNumber;
		_user.Note = Note;
		//EmitToChannel("UpdateInfo", MyLocation.instance.lastLocation);
	}		

	public void CancelSOS()
    {
		EmitToChannel("Refuse", MyLocation.instance.lastLocation);
		Close();
	}		
}