﻿using System.Collections.Generic;
using App.Services;
using BestHTTP.SocketIO;
using Gravitons.UI.Modal;
using Newtonsoft.Json;
using UnityEngine;

public class DestinationSosChannel : SosChannel
{
	OnlineMapsMarker targetMarker;
	OnlineMapsMarker currentMarker;
	Texture2D markerTexture;
	Texture2D currentMarkerTexture;

	public Location location;

	API.APIData.CommonUserExtendInfo _helperInfo;

	protected override void OnOpen()
	{
		markerTexture = Database.DataAssets.Image.place.icon.texture;
		currentMarkerTexture = Database.DataAssets.Image.destination.icon.texture;
		currentMarker = OnlineMapsMarkerManager.CreateItem(location.longitude, location.latitude, currentMarkerTexture);
		Subscribe(channelId, OnReceivedMessage);
	}

	protected override void OnClose()
	{

	}

	void OnReceivedMessage(Socket socket, Packet packet, object[] args)
	{
		var arg = args[0] as Dictionary<string, object>;
		var data = arg["message"] as string;
		var model = JsonConvert.DeserializeObject<SocketModel>(data);

		//Close connection if helper cancel SOS
		if (model.title == "Refuse")
		{
			OnRefuseSOS?.Invoke();
			Close();
			return;
		}
		_helperInfo = model.user;

		var title = model.title;
		Location target = model.location;
		Location me = location;
		
		// Location me = MyLocation.instance.lastLocation;

		if (me == null || target == null)
		{
			Debug.LogError("Location cannot be null");
			return;
		}
		onOtherDeviceConnected?.Invoke();
		//DirectionsPanel.Instance.DrawRouteMap(me, target);

		if (targetMarker == null)
		{
			targetMarker = OnlineMapsMarkerManager.CreateItem(target.longitude, target.latitude, markerTexture, "");
		}
		else
		{
			targetMarker.SetPosition(target.longitude, target.latitude);
		}

		if (model.title == "Met")
		{
			//ModalManager.Show("Thông báo", "Đội cứu trợ đã đến", "Ok", () =>
			//{
			//	DirectionsPanel.Instance.Close();
			//});

			onMeet?.Invoke();

			OnlineMapsMarkerManager.RemoveItem(targetMarker);
			OnlineMapsMarkerManager.RemoveItem(currentMarker);

			//SosNotificationHandler.instance.contactScreen.SetActiveIcon(false);
			Close();
		}
	}
	void EmitToChannel(string title, Location me)
	{
		var socketData = new SocketModel()
		{
			title = title,
			location = me,
		};

		var data = JsonConvert.SerializeObject(socketData);

		Debug.Log($"Emit: {title} - {data}");

		Emit(data);
	}
	public void Refuse()
    {
		EmitToChannel("Refuse", MyLocation.instance.lastLocation);
		Close();
	}		

	public API.APIData.CommonUserExtendInfo GetHelperInfo()
    {
		return _helperInfo;
    }		
}