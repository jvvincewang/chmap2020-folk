using App.Message;
using App.Services;
using API;
using API.Request.SosMap.Generated;
using Gravitons.UI.Modal;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

[System.Serializable]
public class SocketModel
{
	public string title;
	public Location location;
	public API.APIData.CommonUserExtendInfo user;
}

public class SosNotificationHandler : MonoBehaviourSingletonBase<SosNotificationHandler>
{
	[SerializeField] NeedSupportPopup needSupportPopup;

	[Space]
	public ContactScreen contactScreen;

	//NOTE: 'One supporter is going'
	public void ReceivedAlertNotification(AlertNotification alertMessage)
	{
		ModalManager.Show(alertMessage.model.title, alertMessage.model.body, "OK", () =>
		{
			// alertMessage.model.data.chanelId
			var channelId = alertMessage.model.data.Data.Channel.ChannelId;
			var channel = SosChannel.CreateChannel<HelperSosChannel>(channelId);
		});
	}

	//DONE:  1. Nhận thông tin kêu cứu được push từ server thì sẽ hiện notification
	//DONE:  2. Ấn vào notification thì hiện poup(đang bật app thì cứ thế mở popup) có thể ấn accept để nhận đi cứu trợ hoặc cancel
	//DONE:  3. Trường hợp ấn accept => show map và chỉ đường ở hai client app  của người book sos, và người nhận cứu trợ. 
	//NOTE: Cập nhật liên tục vị trí của cả hai cho đến khi hai người gần nhau ( trùng nhau ở trên map trong khoảng cách <=10m)

	//NOTE: 'Please help me'
	public void ReceivedNotificationFromDestination(WarningNotification warningMessage)
	{
		// ModalManager.Show(warningMessage.model.title,
		// 	warningMessage.model.body,
		// 	"Yes", () => { OnAcceptRequest(warningMessage.model.data); },
		// 	"No", null
		// );

		needSupportPopup.txtTitle.text = warningMessage.model.title;
		needSupportPopup.txtDes.text = warningMessage.model.body;
		// needSupportPopup.SetPhoneNumber(warningMessage.model.data.Data.Channel.);
		needSupportPopup.SetPhoneNumber("");
		//TODO:
		needSupportPopup.yesBtn.onClick.AddListener(() =>
		{
			OnAcceptRequest(warningMessage.model.data);
			needSupportPopup.Close();

			//TODO:
			SettingContactScreen();

		});

		needSupportPopup.noBtn.onClick.AddListener(() =>
		{
			needSupportPopup.Close();
		});

		needSupportPopup.Open();
	}

	void OnAcceptRequest(PostSosHelpRequest.ResponseObject notiData)
	{
		//TODO: "soses/{0}/help"
		var req = AppAPIManager.instance.CreateRequest<PostSosHelpRequest>(UnityWebRequest.kHttpVerbPOST, notiData.Data.Channel.ChannelId);
		req.Send(res =>
		{
			Debug.Log("PostSosHelpRequest Success");

			string channelId = res.Data.Channel.ChannelId;

			Debug.Log($"channel Id:  {channelId}");

			var channel = SosChannel.CreateChannel<HelperSosChannel>(channelId);
			channel.sosData = notiData.Data.Sos;

			SettingContactScreen();

		}, (arg0, err) =>
		{
			Debug.LogError($"<color=red>Error PostSosHelpRequest: {err}</color>");
		});
	}

	void SettingContactScreen()
	{
		contactScreen.txtUsername.text = "";
		contactScreen.SetPhoneNumber("");

		contactScreen.SetActiveIcon(true);
	}
}