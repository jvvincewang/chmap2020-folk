﻿using System;
using App;
using App.UI;
using API;
using API.Request.SosMap.Generated;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using static API.Request.SosMap.DestinationRequest;
using Gravitons.UI.Modal;
using JVLib.Scripts.PopupModule;

public class SosPanel : BaseUIPanel
{
	[SerializeField] Button btnConfirm;
	[SerializeField] InputField ipfMyPhoneNumber;
	[SerializeField] InputField ipfAnotherUserPhoneNumber;
	[SerializeField] InputField ipfTitle;
	[SerializeField] InputField ipfDes;
	[Space]
	[SerializeField] string selectionPhoneNumber;
	[Space]
	[SerializeField] DirectionsPanel directionsPanel;
	[Space]
	[NaughtyAttributes.ReorderableList]
	public GameObject[] anotherPanels;
	public Texture2D textureMarker;

	void Awake()
	{
		btnConfirm.onClick.AddListener(() =>
		{
			ShowConfirmCreateNewDestinationPopup();
		});

		Debug.Log("<color=blue>[AppRuntimeData.Data.token]</color> " + AppRuntimeData.Data.token);

		ipfMyPhoneNumber.text = GetMyPhoneNumber();

		// AppAPIManager.instance.PutNotificationSetting(() =>
		// {
		// 	Debug.Log("Put Ok");
		// });
	}

	void ShowConfirmCreateNewDestinationPopup()
	{
		ModalManager.Show("Xác nhận",
			"tạo Sos với vị trí đăng ký hiện tại!",
			"Đồng Ý", () => CreateNewSos(),
			"Hủy", null
		);
	}

	void CreateNewSos()
	{
		var currentLocation = MapController.instance.GetLocationAt(new Vector2(Screen.width / 2, Screen.height / 2));
		Debug.LogFormat("CreateNewSos: {0},{1}", selectionPhoneNumber, currentLocation);

		string title = string.IsNullOrEmpty(ipfTitle.text) ? "SOS" : ipfTitle.text;

		var data = new PostSosesRequest.PostObject
		{
			name = title,
				type = 1,
				description = ipfDes.text,
				location = new LocationObject()
				{
					type = "Point", coordinates = new float[2]
					{
						(float) currentLocation.longitude, (float) currentLocation.latitude
					}
				},
		};

		AppAPIManager.instance.PostSos(data, (res) =>
		{
			Debug.Log("PostSos.DONE");
			string chanelId = res.Data.Channel.ChannelId;
			var chanel = SosChannel.CreateChannel<DestinationSosChannel>(chanelId);
			chanel.location = currentLocation;

			ModalManager.Show("Thông báo",
				"Đã tạo điểm cứu trợ thành công"
			);
		});

		Close();
	}

	string GetMyPhoneNumber()
	{
		Debug.Log("GetMyPhoneNumber(): " + AppRuntimeData.Data.userPhone);
		return AppRuntimeData.Data.userPhone;
	}

	protected override void OnOpen()
	{
		ShowPanels(false);

		var myLocation = MyLocation.instance.lastLocation;

		// if (myLocation != null)
		// MapController.instance.SmoothMoveAndZoom(myLocation, 16, 0);

		MarkerController.instance.SetActiveAllMarkersOnMap(false);
	}

	protected override void OnClose()
	{
		ShowPanels(true);

		MarkerController.instance.SetActiveAllMarkersOnMap(true);
	}

	void ShowPanels(bool value)
	{
		foreach (var item in anotherPanels)
		{
			item.SetActive(value);
		}
	}

	public void SetPhoneNumber(InputField ipf)
	{
		selectionPhoneNumber = ipf.text;
	}
}