﻿using NaughtyAttributes;
using UnityEngine;

namespace App.UI
{
	public abstract class BaseUIPanel : MonoBehaviour
	{
		[SerializeField] GameObject mainPanel;

		[Button]
		public void Open()
		{
			OnOpen();
			mainPanel.SetActive(true);
		}

		[Button]
		public void Close()
		{
			OnClose();
			mainPanel.SetActive(false);
		}

		protected abstract void OnOpen();
		protected abstract void OnClose();
	}
}