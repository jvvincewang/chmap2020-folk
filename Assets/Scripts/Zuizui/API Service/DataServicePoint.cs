﻿using System;
using System.Collections.Generic;

public abstract class DataServicePoint<T>
{
	public List<T> Items { get; set; }
	public abstract void RequestDataFromServer(Action<List<T>> onSuccess, string error = default);
	// public abstract void PostDataToServer(T postObject, Action<List<T>> onSuccess, string error = default);
}