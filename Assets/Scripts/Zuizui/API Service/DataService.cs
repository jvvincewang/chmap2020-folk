﻿using System;
using UnityEngine;

public abstract class DataService
{
	public abstract string DefaultEndpointResourcePath();
	public abstract void Init();
	public abstract void LoadDataFromCache();
	public abstract void RequestDataFromServer(Action onSuccess = null, Action onFailed = null);
}