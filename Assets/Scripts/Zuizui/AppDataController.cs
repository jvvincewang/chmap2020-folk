﻿using System;
using UnityEngine;

public class AppDataController : MonoBehaviourSingletonBase<AppDataController>
{
	[Space]
	public DataService dataService = new AppSosDataService();
	DataService apiService;

	protected override void AwakeSingleton()
	{

	}

	void Start()
	{
		apiService = dataService;

		apiService.Init();

		if (!Internet.IsAvailable())
		{
			LoadDataFromCache();

			apiService.LoadDataFromCache();
		}
		else
		{
			RefreshDataFromServer();
		}
	}

	public void OnClickRefreshData()
	{
		RefreshDataFromServer();
	}

	public void RefreshDataFromServer()
	{
		MarkerController.instance.ClearMarkersOnMap();
		apiService.RequestDataFromServer();
	}

	void LoadDataFromCache()
	{
		Debug.Log("LoadDataFromCache");
		MarkerController.instance.ClearMarkersOnMap();
		apiService.LoadDataFromCache();
	}

	public void RequestData()
	{
		apiService.RequestDataFromServer();
	}
}