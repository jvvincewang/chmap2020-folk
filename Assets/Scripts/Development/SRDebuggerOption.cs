// ReSharper disable once CheckNamespace

using App;
using App.AppData;
using BayatGames.SaveGameFree;

public partial class SROptions {
    [DisplayName("Remove user info data")]
    public void RemoveUserInfoCache() {
        AppSaveData.Data.ClearUserMainInfo();
        AppSaveData.Save();
    }
}