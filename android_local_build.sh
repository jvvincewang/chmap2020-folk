#!/usr/bin/env sh

set -x

export UNITY_EXECUTABLE=${UNITY_EXECUTABLE:-"/Applications/Unity/Hub/Editor/2019.4.12f1/Unity.app/Contents/MacOS/Unity"}
export BUILD_NAME=${BUILD_NAME:-"SOSMap"}

chmod +x ./ci/build.sh
BUILD_TARGET=Android ./ci/build.sh
