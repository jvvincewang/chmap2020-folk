#!/usr/bin/env bash
cp -a ./Builds/fastlane ./Builds/iOS/$BUILD_NAME
cp ./Builds/Gemfile ./Builds/iOS/$BUILD_NAME
cp ./Builds/Gemfile.lock ./Builds/iOS/$BUILD_NAME
    
cd ./Builds/iOS/$BUILD_NAME

bundle exec fastlane ios beta
